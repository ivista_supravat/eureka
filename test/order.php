<?php

# Create New admin User programmatically.
require_once('../app/Mage.php'); // Relative path to Mage.php
umask(0);
Mage::app();

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

try {
    // header('Content-Type: text/xml');
    $dta = Mage::getSingleton('sap/info')->getData(179);

    /*

      //Zend_Debug::dump($data['SalesOrderInfo']);
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
      array_to_xml($dta, $xml_data);

      //DOMDocument to format code output
      $dom = new DOMDocument('1.0');
      $dom->preserveWhiteSpace = false;
      $dom->formatOutput = true;
      $dom->loadXML($xml_data->asXML());


      echo $dom->saveXML();

      $result = $xml_data->asXML('order.xml');

     * 
     */
    $wsdlparams = [
        'Client' => Mage::getSingleton('sap/config')->getSoapClient(),
        'UserName' => Mage::getSingleton('sap/config')->getSoapUsername(),
        'Password' => Mage::getSingleton('sap/config')->getSoapPassword(),
        'exceptions' => true,
        'trace' => true
    ];
    $options = array(
        "soap_version" => SOAP_1_2,
        "cache_wsdl" => WSDL_CACHE_NONE,
        "exceptions" => false
    );

    echo Mage::getSingleton('sap/config')->getSoapUrl();
    Zend_Debug::dump($wsdlparams);

    $client = new SoapClient(Mage::getSingleton('sap/config')->getSoapUrl());

    Zend_Debug::dump($client->__getFunctions());

    $response = $client->ZEcTransactionCreate(['ZEcTransactionCreate' => $dta]);

    //$ds = file_get_contents(Mage::getSingleton('sap/config')->getSoapUrl());
    $res = json_decode(json_encode($response->ZEcTransactionCreateResponse), true);

    Zend_Debug::dump($res);
} catch (SoapFault $exception) {


    Zend_Debug::dump($exception->getMessage());
}

function array_to_xml($data, &$xml_data) {
    foreach ($data as $key => $value) {
        if (is_array($value)) {
            if (is_numeric($key)) {
                $key = 'item' . $key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key", htmlspecialchars("$value"));
        }
    }
}
