<?php

# Create New admin User programmatically.
require_once('../app/Mage.php'); // Relative path to Mage.php
umask(0);
Mage::app();

$translate = Mage::getSingleton('core/translate');
$translate->setTranslateInline(false);

$mailTemplate = Mage::getModel('core/email_template');


echo '<pre>';
var_dump($mailTemplate);
$emailTemplateVariables = array();
$emailTemplateVariables['var1'] = 'var1 value';
$emailTemplateVariables['var2'] = 'var 2 value';
$emailTemplateVariables['var3'] = 'var 3 value';
$emailTemplate->getProcessedTemplate($emailTemplateVariables);
$emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name'));
$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));

try {
    var_dump($emailTemplate->send('supravat.com@gmail.com', 'bla bla', $emailTemplateVariables));
} catch (Exception $e) {
    echo $e->getMessage();
}