<?php

# Create New admin User programmatically.
require_once('../app/Mage.php'); // Relative path to Mage.php
umask(0);
Mage::app();

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

try {
    $dta = Mage::getSingleton('sap/info')->getData(179);
    $wsdlparams = [
        'Client' => Mage::getSingleton('sap/config')->getSoapClient(),
        'UserName' => Mage::getSingleton('sap/config')->getSoapUsername(),
        'Password' => Mage::getSingleton('sap/config')->getSoapPassword(),
        'exceptions' => true,
        'trace' => true
    ];
    $options = array(
        //"soap_version" => SOAP_1_2,
        "cache_wsdl" => WSDL_CACHE_NONE,
        "exceptions" => false
    );

    $client = new SoapClient(NULL, array("location" => "http://10.98.0.117:8005/sap/bc/srt/wsdl/bndg_549BA955BDCE0DB0E10080000A620075/wsdl11/allinone/standard/document?sap-client=900?WSDL",
        "uri" => "",
        "style" => SOAP_RPC,
        "use" => SOAP_ENCODED
    ));

    $soap = $client->__call('ZEcTransactionCreate', $dta);

    $res = json_decode(json_encode($soap->ZEcTransactionCreateResponse), true);


    Zend_Debug::dump($res);
} catch (SoapFault $exception) {
    Zend_Debug::dump($exception);
}
