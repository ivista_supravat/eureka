<?php
require_once('../app/Mage.php'); // Relative path to Mage.php
umask(0);
Mage::app();

error_reporting(E_ALL ^ E_NOTICE); 

$serializer_options = array ( 

    'addDecl' => TRUE, 

    'encoding' => 'ISO-8859-1', 

    'indent' => '  ', 

    'rootName' => 'palette', 

    'defaultTagName' => 'color', 

);