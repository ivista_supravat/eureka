<?php
# Create New admin User programmatically.
require_once('app/Mage.php'); // Relative path to Mage.php
umask(0);
Mage::app();

try {
    $user = Mage::getModel('admin/user')
	->setData(array(
	'username'  => 'newadmin',
	'firstname' => 'New',
	'lastname'    => 'Admin',
	'email'     => 'youremail@domain.com',
	'password'  =>'password',
	'is_active' => 1
))->save();

} catch (Exception $e) {
	echo $e->getMessage();
	exit;
}

//Assign Role Id
try {
	$user->setRoleIds(array(9))  //Administrator role id is 1 ,Here you can assign other roles ids
	->setRoleUserId($user->getUserId())
	->saveRelations();
} catch (Exception $e) {
	echo $e->getMessage();
	exit;
}

echo 'User created successfully';

?>
