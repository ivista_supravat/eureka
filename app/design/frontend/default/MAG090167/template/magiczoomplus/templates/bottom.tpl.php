<?php
if(!empty($magicscroll) && !empty($magicscrollOptions)) {
    $magicscrollOptions = " data-options=\"{$magicscrollOptions}\"";
}
?>
<!-- Begin magiczoomplus -->
<div class="MagicToolboxContainer selectorsBottom minWidth">
    <?php echo $main; ?>
<?php

$thumbs = array_unique($thumbs);

if(count($thumbs) > 1) {
    ?>
    <div class="MagicToolboxSelectorsContainer">
        <div id="MagicToolboxSelectors<?php echo $pid ?>" class="<?php echo $magicscroll ?>"<?php echo $magicscrollOptions ?>>
        <?php echo join("\n\t", $thumbs); ?>
            <?php 
                  $videoUrl = Mage::getModel('catalog/product')->load($pid)->getResource()->getAttribute('videourl'); 
                  if ($videoUrl && $videoUrl->getId() && !empty($productmodel->getResource()->getAttribute('videourl')->getFrontend()->getValue($productmodel))) {  
            ?>
           <button type="button" class="modalButton" data-toggle="modal" data-src="<?php echo $productmodel->getResource()->getAttribute('videourl')->getFrontend()->getValue($productmodel);?>"  data-height="800" data-target="#videopopup" data-video-fullscreen=""><img src="<?php echo $videoimage;?>"></button>
            <?php } ?>
        </div>
        
    </div>
    <?php
}
?>
    

</div>
<!-- End magiczoomplus -->
<?php 
        $videoUrl = Mage::getModel('catalog/product')->load($pid)->getResource()->getAttribute('videourl'); 
        if ($videoUrl && $videoUrl->getId() && !empty($productmodel->getResource()->getAttribute('videourl')->getFrontend()->getValue($productmodel))) {  
  ?>
<div class="modal fade" id="videopopup">
		<div class="modal-dialog" style="width:90%">
                    <div class="modal-content" style="width:100%">
                        <a href="javascript:void(0)" data-dismiss="modal" style="float: right!important"><i class="fa fa-times"></i></a>
                        <div style="clear:both"></div>
				<div class="modal-body" style="width:100%; padding: 0;">
          
          <div class="embed-responsive embed-responsive-16by9">
					            <iframe class="embed-responsive-item"  frameborder="0"></iframe>
          </div>
				</div>

			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
<script>
( function($) {
function iframeModalOpen(){
        $('.modalButton').on('click', function(e) {
                var src = $(this).attr('data-src');
                var width = $(this).attr('data-width') || 640; 
                var height = $(this).attr('data-height') || 360; 

                var allowfullscreen = $(this).attr('data-video-fullscreen');
                $("#videopopup iframe").attr({
                        'src': src,
                        'height': height,
                        'allowfullscreen':''
                });
        });
        $('#videopopup').on('hidden.bs.modal', function(){
                $(this).find('iframe').html("");
                $(this).find('iframe').attr("src", "");
        });
	}
  $(document).ready(function(){
		iframeModalOpen();
  });
  
  } ) ( jQuery );
</script>
<style>
.modalButton {
  border: 1px solid #E2E2E2;
}
  .modal-content {
    box-shadow: none;
    background-color: transparent;
    border: 0;
  }  
    iframe {
      display: block;
      margin: 0 auto;
      width: 100%
    }
</style>
 <?php } ?>