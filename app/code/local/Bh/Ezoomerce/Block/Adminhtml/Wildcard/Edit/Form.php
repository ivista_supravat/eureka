<?php
/**
 * @package    Bh_Ezoomerce_Block
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Block_Adminhtml_Wildcard_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
     
        $this->setId('ezoomerce_wildcard_form');
        $this->setTitle($this->__('Wildcard Information'));
    }  
     
    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {  
        $model = Mage::registry('ezoomerce');
     
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method'    => 'post'
        ));
     
        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => Mage::helper('checkout')->__('Wildcard Information'),
            'class'     => 'fieldset-wide',
        ));
     
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }  
     	$fieldset->addField('title', 'select', array(
            'name'      => 'title',
            'label'     => 'Title',
            'title'     => 'Title',
			'options' => Mage::getModel('ezoomerce/wildcard')->toOptionArray(),
            'required'  => true,
        ));
        $fieldset->addField('symbol', 'text', array(
            'name'      => 'symbol',
            'label'     => 'Symbol',
            'title'     => 'Symbol',
            'required'  => true,
        ));
     
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
     
        return parent::_prepareForm();
    }  
}
?>