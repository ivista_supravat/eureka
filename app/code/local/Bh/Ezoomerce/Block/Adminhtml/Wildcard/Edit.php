<?php
/**
 * @package    Bh_Ezoomerce_Block
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Block_Adminhtml_Wildcard_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {  
        $this->_blockGroup = 'ezoomerce';
        $this->_controller = 'adminhtml_wildcard';
     
        parent::__construct();
     
        $this->_updateButton('save', 'label', $this->__('Save Wildcard'));
        $this->_updateButton('delete', 'label', $this->__('Delete Wildcard'));
    }  
     
    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {  
        if (Mage::registry('ezoomerce')->getId()) {
            return $this->__('Edit Wildcard');
        }  
        else {
            return $this->__('New Wildcard');
        }  
    }  
}
?>