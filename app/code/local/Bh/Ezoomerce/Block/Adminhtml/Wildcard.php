<?php
/**
 * @package    Bh_Ezoomerce_Block
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Block_Adminhtml_Wildcard extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'ezoomerce';
        $this->_controller = 'adminhtml_wildcard';
        $this->_headerText = $this->__('Wildcard');
         
        parent::__construct();
    }
}
?>