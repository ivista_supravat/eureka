<?php

/**
 */
class Bh_Ezoomerce_Adminhtml_WildcardController extends Mage_Adminhtml_Controller_Action 
{
	
	/**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('ezoomerce/ezoomerce_wildcard')
            ->_title($this->__('Ezoomerce'))->_title($this->__('Wildcard'))
            ->_addBreadcrumb($this->__('Ezoomerce'), $this->__('Ezoomerce'))
            ->_addBreadcrumb($this->__('Wildcard'), $this->__('Wildcard'));
         
        return $this;
    }
	
    public function indexAction()
    {  
        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('ezoomerce/adminhtml_wildcard'));
        $this->renderLayout();
    }  
     
    public function newAction()
    {  
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
    }  
     
    public function editAction()
    {  
        $this->_initAction();
     
        // Get id if available
        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('ezoomerce/wildcard');
     
        if ($id) {
            // Load record
            $model->load($id);
     
            // Check if record is loaded
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This wildcard no longer exists.'));
                $this->_redirect('*/*/');
     
                return;
            }  
        }  
     
        $this->_title($model->getId() ? $model->getName() : $this->__('New Wildcard'));
     
        $data = Mage::getSingleton('adminhtml/session')->getBazData(true);
        if (!empty($data)) {
            $model->setData($data);
        }  
     
        Mage::register('ezoomerce', $model);
     
        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit Wildcard') : $this->__('New Wildcard'), $id ? $this->__('Edit Wildcard') : $this->__('New Wildcard'))
            ->_addContent($this->getLayout()->createBlock('ezoomerce/adminhtml_wildcard_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }
     
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('ezoomerce/wildcard');
            $model->setData($postData);
 
            try {
                $model->save();
 
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The wildcard has been saved.'));
                $this->_redirect('*/*/');
 
                return;
            }  
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this wildcard.'));
            }
 
            Mage::getSingleton('adminhtml/session')->setWildcardData($postData);
            $this->_redirectReferer();
        }
    }
     
    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('ezoomerce/wildcard');
                $model->load($id);
                $model->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The wildcard has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while deleting the Wildcard.'));
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        $this->_getSession()->addError($this->__('Unable to find a Wildcard to delete.'));
        $this->_redirect('*/*/');
    }
     
    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ezoomerce/ezoomerce_wildcard');
    }
	
}
