<?php

/**
 * @package    Ezoomerce SMS
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Model_Client extends Varien_Object {

    protected $_sms_uri;
    protected $_sms_api_key;
    protected $_sms_api_password;
    protected $_sms_api_senderid;
    protected static $_httpClient;

    public function __construct() {
        $this->_sms_uri = Mage::getSingleton('ezoomerce/config')->getSmsUrl();
        $this->_sms_api_key = Mage::getSingleton('ezoomerce/config')->getKey();
        $this->_sms_api_password = Mage::getSingleton('ezoomerce/config')->getPassword();
        $this->_sms_api_senderid = Mage::getSingleton('ezoomerce/config')->getSenderid();
    }

    private static function _getHttpClient() {
        if (!self::$_httpClient instanceof Zend_Http_Client) {
            self::$_httpClient = new Zend_Http_Client();
        }
        return self::$_httpClient;
    }

    private function _prepareParams($params) {
        foreach ($params as $key => &$val) {
            if (!is_array($val))
                continue;
            $val = Zend_Json::encode($val);
        }
        $params['pass'] = $this->_sms_api_password;
        return $params;
    }

    public function call($args = array()) {
        $param = $this->_prepareParams($args);
        foreach ($param as $key => $val) {
            $request.= $key . "=" . urlencode($val);
            $request.= "&";
        }
        $request = substr($request, 0, strlen($request) - 1);
        $url = Mage::getStoreConfig('bhsms/api/url') . 'uname=' . $this->_sms_api_key . '&pass=' . $this->_sms_api_password . '&send=' . $this->_sms_api_senderid . '&' . $request;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        /* $client = self::_getHttpClient()
          ->setUri($this->_sms_uri)
          ->setParameterGet($params);

          try {
          $response = $client->request();
          } catch(Exception $e) {
          throw new Mage_Core_Exception('Service unavaliable');
          }

          if (!$response->isSuccessful()) {
          throw new Mage_Core_Exception('Service unavaliable');
          }

          $result_raw = $response->getBody();
          $result = explode(',', $result_raw);

          return $result; */
    }

}

?>