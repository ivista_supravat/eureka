<?php

/**
 * @package    Ezoomerce SMS
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Model_Customer extends Mage_Customer_Model_Customer {

    public function sendNewAccountSMS($model = null, $sms_template = null) {
        try {
            if (!empty($model)) {
                $wildcard = Mage::getModel('ezoomerce/wildcard');
                $mobile = '918553007539';
                $param = array(
                    'customer' => $model,
                    'order' => '',
                    'invoice' => '',
                    'shipment' => '',
                    'credit_memo' => '',
                );
                $sms = $wildcard->getSmsFormat($param, $sms_template);
                if (!empty($sms)) {
                    $message = $sms;
                } else {
                    $message = 'Dear ' . $name . ', welcome to ' . Mage::app()->getStore()->getFrontendName() . '. To log in when visiting our site just click Login or My Account at the top of every page, and then enter your e-mail address and password. Use the following values when prompted to log in: E-mail: ' . $email . ', Password: ' . $password;
                }
                $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
                return true;
            }
            return true;
        } catch (Exception $e) {
            
        }
    }

    /**
     * Send corresponding email template
     *
     * @param string $emailTemplate configuration path of email template
     * @param string $emailSender configuration path of email identity
     * @param array $templateParams
     * @param int|null $storeId
     * @return Mage_Customer_Model_Customer
     */
    protected function _sendEmailTemplate($template, $sender, $templateParams = array(), $storeId = null) {
        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getEmail(), $this->getName());
        $mailer->addEmailInfo($emailInfo);

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig($sender, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId(Mage::getStoreConfig($template, $storeId));
        //$templateParams['cname'] = $this->getShippingAddress()->getFirstname().' '.$this->getShippingAddress()->getLastname();
        //$templateParams['mno'] = $this->getShippingAddress()->getTelephone();
        $mailer->setTemplateParams($templateParams);

        // Custom code added by Rajesh for sending mail
        //send sms code added by Rajesh
        $sms_status = Mage::getSingleton('ezoomerce/config')->getRegistrationMode();
        if ($sms_status) {
            $sms_template = Mage::getSingleton('ezoomerce/config')->getRegistrationTemplate();
            $this->sendNewAccountSMS($templateParams['customer'], $sms_template);
        }
        //send email code added by Rajesh
        $isActive = Mage::getSingleton('ezoomerce/config')->getRegistrationActive();
        if ($isActive) {
            $groupId = Mage::getSingleton('ezoomerce/config')->getRegistrationGroupId();
            $msg_id = Mage::getSingleton('ezoomerce/config')->getRegistrationMessId();
            $email = $templateParams['customer']->getEmail();
            $firstName = $templateParams['customer']->getFirstname();
            $user = $firstName;
            $password = $templateParams['customer']->getPassword();
            if (!empty($groupId) && is_null($groupId)) {
                $groupId = "1800029625";
            }
            if (!empty($msg_id) && is_null($msg_id)) {
                $msg_id = "1600205586";
            }
            $params = array('email' => $email, 'firstName' => $firstName, 'user' => $user, 'password' => $password, 'groupId' => $groupId, 'msg_id' => $msg_id);
            $mailer->sending($params); //custom
        } else {
            $mailer->send(); // default
        }

        return $this;
    }

    /**
     * Send email with new account related information
     *
     * @param string $type
     * @param string $backUrl
     * @param string $storeId
     * @throws Mage_Core_Exception
     * @return Mage_Customer_Model_Customer
     */
    public function sendNewAccountEmail($type = 'registered', $backUrl = '', $storeId = '0') {
        $types = array(
            'registered' => self::XML_PATH_REGISTER_EMAIL_TEMPLATE, // welcome email, when confirmation is disabled
            'confirmed' => self::XML_PATH_CONFIRMED_EMAIL_TEMPLATE, // welcome email, when confirmation is enabled
            'confirmation' => self::XML_PATH_CONFIRM_EMAIL_TEMPLATE, // email with confirmation link
        );
        if (!isset($types[$type])) {
            Mage::throwException(Mage::helper('customer')->__('Wrong transactional account email type'));
        }

        if (!$storeId) {
            $storeId = $this->_getWebsiteStoreId($this->getSendemailStoreId());
        }

        $this->_sendEmailTemplate($types[$type], self::XML_PATH_REGISTER_EMAIL_IDENTITY, array('customer' => $this, 'back_url' => $backUrl), $storeId);

        return $this;
    }

    /**
     * Send email with new customer password
     *
     * @return Mage_Customer_Model_Customer
     */
    public function sendPasswordReminderEmail() {
        $storeId = $this->getStoreId();
        if (!$storeId) {
            $storeId = $this->_getWebsiteStoreId();
        }

        $this->_sendEmailTemplate(self::XML_PATH_REMIND_EMAIL_TEMPLATE, self::XML_PATH_FORGOT_EMAIL_IDENTITY, array('customer' => $this), $storeId);

        return $this;
    }

    /**
     * Send email with reset password confirmation link
     *
     * @return Mage_Customer_Model_Customer
     */
    public function sendPasswordResetConfirmationEmail() {
        $storeId = $this->getStoreId();
        if (!$storeId) {
            $storeId = $this->_getWebsiteStoreId();
        }

        $this->_sendEmailTemplate(self::XML_PATH_FORGOT_EMAIL_TEMPLATE, self::XML_PATH_FORGOT_EMAIL_IDENTITY, array('customer' => $this), $storeId);

        return $this;
    }

}
