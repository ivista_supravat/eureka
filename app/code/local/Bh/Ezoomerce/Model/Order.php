<?php

/**
 * @package    Ezoomerce SMS
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Model_Order extends Mage_Sales_Model_Order {

    
    

    const XML_PATH_FEEDBACK_TEMPLATE = 'ezoomerce/email_cms/delivery_close_feedback_template';

    // for gest checkout sms
    public function sendAsGuestSms($model = null, $sms_template = null) {

        try {
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($model->getCustomerId());
            if (!empty($model)) {
                $wildcard = Mage::getModel('ezoomerce/wildcard');

                $param = array(
                    'customer' => $customer,
                    'order' => $model,
                    'invoice' => '',
                    'shipment' => '',
                    'credit_memo' => '',
                );
                $sms = $wildcard->getSmsFormat($param, $sms_template);
                if (!empty($sms)) {
                    $message = $sms;
                    $mobile = '91'.$order->getBillingAddress()->getTelephone();
                    $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
                    return true;
                }
            }

            if ($order->getCustomerIsGuest()) {
                $pass = $customer->generatePassword();
                $firstName = $order->getShippingAddress()->getFirstname();
                $user = $firstName;
                $store = Mage::app()->getStore()->getFrontendName();
                $mobile = $order->getBillingAddress()->getTelephone();
                $email = $order->getCustomerEmail();
                $isActive = Mage::getSingleton('ezoomerce/config')->getGuestcheckoutActive();
                $groupId = Mage::getSingleton('ezoomerce/config')->getGuestcheckoutGroupId();
                $msg_id = Mage::getSingleton('ezoomerce/config')->getGuestcheckoutMessId();
                if ($isActive) {
                    if (!empty($groupId) && is_null($groupId)) {
                        $groupId = "1800029625";
                    }
                    if (!empty($msg_id) && is_null($msg_id)) {
                        $msg_id = "1600205586";
                    }
                    try {
                        $Soapclient = Mage::getModel('ezoomerce/Soapclient');
                        $Soapclient->updateMembertoSend($email, $firstName, $user, $pass, $groupId, $msg_id);
                        return;
                    } catch (Exception $e) {
                        
                    }
                }
            }
            return;
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function sendAsOrderConfirmationSms($model = null, $sms_template = null) {

        try {
            // add custom code by Rajesh
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($model->getCustomerId());
            if (!empty($model)) {
                $wildcard = Mage::getModel('ezoomerce/wildcard');

                $param = array(
                    'customer' => $customer,
                    'order' => $model,
                    'invoice' => '',
                    'shipment' => '',
                    'credit_memo' => '',
                );
                $sms = $wildcard->getSmsFormat($param, $sms_template);
                if (!empty($sms)) {
                    $message = $sms;
                    $mobile = '91'.$model->getBillingAddress()->getTelephone();
                    $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
                    return true;
                }
                return true;
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Send email with order data
     *
     * @return Mage_Sales_Model_Order
     */
    public function sendNewOrderEmail() {
        $storeId = $this->getStore()->getId();

        //****************kunal start code to get the category id of last order ************************
        $lastOrderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $_order = Mage::getModel('sales/order')->load($lastOrderId);
        //kunal end code to get the category id of last order ************************
        if (!Mage::helper('sales')->canSendNewOrderEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                    ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
            // for guest checkout sms added by Rajesh
            /*$sms_status = Mage::getSingleton('ezoomerce/config')->getGuestCheckoutMode();
            if ($sms_status) {
                $sms_template = Mage::getSingleton('ezoomerce/config')->getGuestCheckoutTemplate();
                $this->sendAsGuestSms($this, $sms_template);
            }*/
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }
        
        //Change the banners on the emails - Kushal
        $items = $this->getAllItems();
        foreach ($items as $item) {
            $order = $item->getOrder();
            $product_id = $item['product_id'];
            $_product = Mage::getModel('catalog/product')->load($product_id);
            $pimage = Mage::getModel('catalog/product_media_config')->getMediaUrl($_product->getThumbnail());
            $pname = $_product->getName();
            $pqtyorder = $item->getQtyOrdered()*1;
            $pprice = $item->getRowTotal();
            $html .= '<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width:50%;"><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$pimage.'" alt="'.$pname.'" title="'.$pname.'" width="59" height="65"/></td><td style="color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;font-size: 12px">'.$pname.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">1</td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600; text-align:center;font-size: 12px">RS. '.$pprice.'/</td></tr></table></td></tr></table>' ;
        }
        $carturl = Mage::getBaseUrl().'/checkout/cart';
        $ordersubtotal = intval($order->getSubtotal());
        $discount = intval($order->getDiscountAmount());
        $grandtotal = intval($order->getGrandTotal());
        $paymentmode = $paymentBlock->getMethod()->getTitle();
        $html .= '<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width:50%;"><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:20px;"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; font-weight:100; color: #868686;">Free Gift : <span style=" color:#4f4f4f; font-weight:600">Gift Voucher</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:100; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:100; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; font-weight:100; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">'.$paymentmode.'</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:100; line-height:2px;">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:100; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td  style=" width:100px;font-size:8px;font-family: \'Open Sans\', sans-serif; color:#757575; font-weight:500;width:60%"><a href="'.$carturl.'" ><input type="button" value="TRACK / CANCEL ORDER" style="width:118px;font-size:8px;padding:10px; border-radius:4px;  color:#2762a8; font-weight:bold; margin-top:10px; background:#fff601;-webkit-box-shadow: 0px 3px 1px 0px #ded800; -moz-box-shadow:    0px 3px 1px 0px #ded800;box-shadow:0px 3px 1px 0px #ded800; border:#ded800"/></a></td><td style=" padding:8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
        // End - Banner change on emails - Kushal		
        // Set all required params and send emails
        $fullname = $this->getShippingAddress()->getFirstname().' '.$this->getShippingAddress()->getLastname();
        $streetaddress=$this->getShippingAddress()->getStreet();
        $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
        //$zipcode = Mage::getModel('wms/zipcode')->load($this->getShippingAddress()->getPostcode(),'zipcode');
        //$i = $zipcode->getDeliveryTime();
        $deliverydate = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false). ' + 7 day'));
        $shippingaddress = '<span style=" color:#4f4f4f; font-weight:bold; line-height:20px;">'.$fullname.'<br>'.$streetaddress[0].',<br> '.$this->getShippingAddress()->getCity().'<br>'.$this->getShippingAddress()->getRegion().' - '.$this->getShippingAddress()->getPostcode().'</span>';
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
            'order' => $this,
            'billing' => $this->getBillingAddress(),
            'payment_html' => $paymentBlockHtml,
            'mobile_no' => $this->getBillingAddress()->getTelephone(),
            'billing_address' => $shippingaddress,
            'customer_id' => $this->getCustomerId(),
            'name' => $fullname,
            'html' => $html,
            'order_time' => $ordertime,
            'delivery_date' => $deliverydate
                )
        );

        // Custom code added by Rajesh for sending custom sms & email
        //send confirm sms
        /*$sms_status = Mage::getSingleton('ezoomerce/config')->getNewOrderMode();
        if ($sms_status) {
            $sms_template = Mage::getSingleton('ezoomerce/config')->getNewOrderTemplate();
            $this->sendAsOrderConfirmationSms($this, $sms_template); // custom code for sending order confirmation sms
        }*/
        //send confirm email
        $isActive = Mage::getSingleton('ezoomerce/config')->getNeworderActive();
        $groupId = Mage::getSingleton('ezoomerce/config')->getNeworderGroupId();
        $msg_id = Mage::getSingleton('ezoomerce/config')->getNeworderMessId();
        if ($isActive) {
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($this->getCustomerId());
            $email = $this->getCustomerEmail();
            $firstName = $this->getShippingAddress()->getFirstname();
            $user = $firstName;
            $password = $customer->generatePassword();
            $store = Mage::app()->getStore()->getFrontendName();
            $mobile = $this->getBillingAddress()->getTelephone();
            if (!empty($groupId) && is_null($groupId)) {
                $groupId = "1800029625";
            }
            if (!empty($msg_id) && is_null($msg_id)) {
                $msg_id = "1600205586";
            }
            $params = array('email' => $email, 'firstName' => $firstName, 'user' => $user, 'password' => $password, 'groupId' => $groupId, 'msg_id' => $msg_id);
            $mailer->sending($params); //custom
            //$this->setEmailSent(true);
            $this->_getResource()->saveAttribute($this, 'email_sent');
        } else {
            $mailer->send(); // default
            //$this->setEmailSent(true);
            $this->_getResource()->saveAttribute($this, 'email_sent');
        }
        // End custom code
        return $this;
    }

    /**
     * Send email with order update information
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order
     */
    public function sendOrderUpdateEmail($notifyCustomer = true, $comment = '') {
        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendOrderCommentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_UPDATE_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_FEEDBACK_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_FEEDBACK_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        /* echo "<pre>";
          print_r($templateId);
          echo "</pre>";
          die(); */
        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($this->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is
        // 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
            'order' => $this,
            'comment' => $comment,
            'billing' => $this->getBillingAddress()
                )
        );
        // Custom code added by Rajesh for sending mail
        if (($this->getStatus() == "delivered" || $this->getStatus() == "closed") && $this->hasInvoices()) {
            $isActive = Mage::getSingleton('ezoomerce/config')->getFeedbackActive();
            $groupId = Mage::getSingleton('ezoomerce/config')->getFeedbackGroupId();
            $msg_id = Mage::getSingleton('ezoomerce/config')->getFeedbackMessId();
        } else {
            $isActive = Mage::getSingleton('ezoomerce/config')->getDelayinvoiceActive();
            $groupId = Mage::getSingleton('ezoomerce/config')->getDelayinvoiceGroupId();
            $msg_id = Mage::getSingleton('ezoomerce/config')->getDelayinvoiceMessId();
        }

        if (false) {
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($this->getCustomerId());
            $email = $this->getCustomerEmail();
            $firstName = $this->getShippingAddress()->getFirstname();
            $user = $firstName;
            $password = $customer->generatePassword();
            $store = Mage::app()->getStore()->getFrontendName();
            $mobile = $this->getBillingAddress()->getTelephone();
            if (!empty($groupId) && is_null($groupId)) {
                $groupId = "1800029625";
            }
            if (!empty($msg_id) && is_null($msg_id)) {
                $msg_id = "1600205586";
            }
            $params = array('email' => $email, 'firstName' => $firstName, 'user' => $user, 'password' => $password, 'groupId' => $groupId, 'msg_id' => $msg_id);
            $mailer->sending($params); //custom
        } else {
            $mailer->send(); // default
        }
        // End custom code
        return $this;
    }

}
