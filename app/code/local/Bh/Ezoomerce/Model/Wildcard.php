<?php

/*
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Bh_Ezoomerce_Model_Wildcard extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('ezoomerce/wildcard');
    }

    public function toOptionArray() {
        return array(
            '' => Mage::helper('ezoomerce')->__('-----Please select title must-----'),
            'customer_name' => Mage::helper('ezoomerce')->__('Customer Name'),
            'customer_password' => Mage::helper('ezoomerce')->__('Customer Password'),
            'customer_contact' => Mage::helper('ezoomerce')->__('Customer Contact Number'),
            'store_name' => Mage::helper('ezoomerce')->__('Store Name'),
            'order_no' => Mage::helper('ezoomerce')->__('Order Number'),
            'order_grand_total' => Mage::helper('ezoomerce')->__('Order Grand Total'),
            'order_status' => Mage::helper('ezoomerce')->__('Order Status'),
            'order_date' => Mage::helper('ezoomerce')->__('Order Date'),
            'invoice_no' => Mage::helper('ezoomerce')->__('Invoice Number'),
            'invoice_date' => Mage::helper('ezoomerce')->__('Invoice Date'),
            'shipment_no' => Mage::helper('ezoomerce')->__('Shipment Number'),
            'shipment_date' => Mage::helper('ezoomerce')->__('Shipment Date'),
            'tracking_info' => Mage::helper('ezoomerce')->__('Tracking Info'),
            'credit_memo_no' => Mage::helper('ezoomerce')->__('Credit Memo Number'),
            'credit_memo_date' => Mage::helper('ezoomerce')->__('Credit Memo Date'),
            'currency_code' => Mage::helper('ezoomerce')->__('Currency Code'),
                #'new_tag' => 'Tagged a Product',
                #'new_review' => 'Reviewed a Product',
        );
    }

    public function getSmsFormat($param, $sms_template = null) {
        // customer details variable

        $cust_name='';
        $mobile='';
        $email='';
        $password='';
        // order details variable
        $order_number='';
        $order_date='';
        $grand_total='';
        $currency_code='';
        $status;
        // invoice details variable
        $invoice_number='';
        $invoice_date='';
        $invoice_grand_total='';
        // shipment details variable
        $shipment_number='';
        $shipment_date='';
        $shipment_grand_total='';
        $track_info = "";
        // creditmemo details variable
        $credit_memo_number='';
        $credit_memo_date='';
        $credit_memo_grand_total='';

        $store_name = Mage::app()->getStore()->getFrontendName();
        //load model for check wild card has or not
        $wildcards = Mage::getModel('ezoomerce/wildcard')->getCollection();

        $template = $sms_template;

        // if has customer model assign variable value
        if (!empty($param['customer'])) {
            $cust_name = $param['customer']->getName();
            $mobile = '8436871830';
            $email = $param['customer']->getEmail();
            if ($param['customer']->getCustomerIsGuest()) {
                $password = $param['customer']->getPassword();
            } else {
                //$password = $param['customer']->generatePassword();
            }
        }
        // if has order model assign variable value
        if (!empty($param['order'])) {
            $cust_name = $param['order']->getShippingAddress()->getFirstname();
            $mobile = $param['order']->getBillingAddress()->getTelephone();
            $email = $param['order']->getCustomerEmail();
            $order_number = $param['order']->getIncrementId();
            $order_date = $param['order']->getCreatedAtFormated('long');
            $status = $param['order']->getStatusLabel();
            $grand_total = number_format($param['order']->getGrandTotal(), 0);
            $mobile = $param['order']->getBillingAddress()->getTelephone();
            $currency_code = $param['order']->getStoreCurrencyCode();
        }
        // if has invoice model assign variable value
        if (!empty($param['invoice'])) {
            $invoice_number = $param['invoice']->getIncrementId();
            $invoice_date = $param['invoice']->getCreatedAtFormated('long');
            $invoice_grand_total = number_format($param['invoice']->getGrandTotal(), 0);
        }
        // if has shipment model assign variable value
        if (!empty($param['shipment'])) {
            $shipment_number = $param['shipment']->getIncrementId();
            $shipment_date = $param['shipment']->getCreatedAtFormated('long');
            $shipment_grand_total = number_format($param['shipment']->getGrandTotal(), 0);

            foreach ($param['shipment']->getAllTracks() as $_item) {
                $track_info .= 'Carrier Title:' . $_item->getTitle() . ' Track No:' . $_item->getNumber();
            }
        }
        // if has credit_memo model assign variable value
        if (!empty($param['credit_memo'])) {
            $credit_memo_number = $param['credit_memo']->getIncrementId();
            $credit_memo_date = $param['credit_memo']->getCreatedAtFormated('long');
            $credit_memo_grand_total = number_format($param['credit_memo']->getGrandTotal(), 0);
        }
        foreach ($wildcards as $wildcard) {

            switch ($wildcard['title']) {
                case 'customer_name':
                    $template = str_replace($wildcard['symbol'], $cust_name, $template);
                    break;
                case 'customer_password':
                    $template = str_replace($wildcard['symbol'], $password, $template);
                    break;
                case 'customer_contact':
                    $template = str_replace($wildcard['symbol'], $mobile, $template);
                    break;
                case 'store_name':
                    $template = str_replace($wildcard['symbol'], $store_name, $template);
                    break;
                case 'order_no':
                    $template = str_replace($wildcard['symbol'], $order_number, $template);
                    break;
                case 'order_grand_total':
                    $template = str_replace($wildcard['symbol'], $grand_total, $template);
                    break;
                case 'order_status':
                    $template = str_replace($wildcard['symbol'], $status, $template);
                    break;
                case 'order_date':
                    $template = str_replace($wildcard['symbol'], $order_date, $template);
                    break;
                case 'invoice_no':
                    $template = str_replace($wildcard['symbol'], $invoice_number, $template);
                    break;
                case 'invoice_date':
                    $template = str_replace($wildcard['symbol'], $invoice_date, $template);
                    break;
                case 'shipment_no':
                    $template = str_replace($wildcard['symbol'], $shipment_number, $template);
                    break;
                case 'shipment_date':
                    $template = str_replace($wildcard['symbol'], $shipment_date, $template);
                    break;
                case 'tracking_info':
                    $template = str_replace($wildcard['symbol'], $track_info, $template);
                    break;
                case 'credit_memo_no':
                    $template = str_replace($wildcard['symbol'], $credit_memo_number, $template);
                    break;
                case 'credit_memo_date':
                    $template = str_replace($wildcard['symbol'], $credit_memo_date, $template);
                    break;
                case 'currency_code':
                    $template = str_replace($wildcard['symbol'], $currency_code, $template);
                    break;
                    break;
            }
        }
        return $template;
    }

}
