<?php

class Bh_Ezoomerce_Model_Observer {

    public function backinstock() {
        try {

            /* $to = "sk.rajesh@bluehorse.in";
              $subject = "Back in stock alert";
              $message = "Hello! product  Cron.";
              $from = "rajeshsk14@gmail.com";
              $headers = "From:" . $from;
              mail($to,$subject,$message,$headers); */
            Mage::getModel('productalert/observer')->process();
        } catch (Exception $e) {
            
        }
    }
    
    public function checkCrossSellInvoice() {
        try {
            //if ($sms_status) {die;
                $orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status', 'delivery_complete');
                foreach ($orders as $order) {
                        $order = Mage::getModel('sales/order')->load($order->getId());
                        $product = Mage::getModel('catalog/product')->load($order->getId());
                        $categoryIds = $product->getCategoryIds();
                        $favcat = $categoryIds[0];
                        switch ($favcat) {
                            case "3":
                                $urlhtml = Mage::getBaseUrl().'vaccum-cleaners.html'.'<br>'.Mage::getBaseUrl().'air-purifier.html'.'<br>'.Mage::getBaseUrl().'lifestyle-solutions.html'.'<br>';
                                break;
                            case "4":
                               $urlhtml = Mage::getBaseUrl().'vaccum-cleaners.html'.'<br>'.Mage::getBaseUrl().'air-purifier.html'.'<br>'.Mage::getBaseUrl().'lifestyle-solutions.html'.'<br>';
                                break;
                            case "5":
                                $urlhtml = Mage::getBaseUrl().'vaccum-cleaners.html'.'<br>'.Mage::getBaseUrl().'air-purifier.html'.'<br>'.Mage::getBaseUrl().'lifestyle-solutions.html'.'<br>';
                                break;
                            case "6":
                                $urlhtml = Mage::getBaseUrl().'vaccum-cleaners.html'.'<br>'.Mage::getBaseUrl().'air-purifier.html'.'<br>'.Mage::getBaseUrl().'lifestyle-solutions.html'.'<br>';
                                break;
                            default:
                               $urlhtml = Mage::getBaseUrl();
                        }
                        $useremail = $order->getCustomerEmail(); 
                        $Username = $order->getCustomerName();
                        $emailTemp = Mage::getModel('core/email_template')->load(4);
                        $emailTempVariables = array();
                        $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                        $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                        $adminUsername = 'Admin';
                        $emailTempVariables['customername'] = $order->getCustomerName();
                        $emailTempVariables['content']='Thank you for shopping with us. While you have made your first purchase, you could bring home the best of other ranges on '.$urlhtml.'';
                        $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
                        $emailTemp->setSenderName($adminUsername);
                        $emailTemp->setSenderEmail($adminEmail);
                        $emailTemp->send($useremail,$Username,$emailTempVariables);
                        
                        
                        $message = 'Thank you for shopping with us. While you have made your first purchase, you could bring home the best of other ranges on http://www.eurekaforbes.com.';
                        $mobile = '91'.$order->getBillingAddress()->getTelephone();
                        $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
                }
            //}
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
    
    public function checkDelayInvoice() {
        try {
            $sms_status = Mage::getSingleton('ezoomerce/config')->getDelayInvoiceMode();
            //if ($sms_status) {die;
                $orders = Mage::getModel('sales/order')->getCollection();
                foreach ($orders as $order) {
                    if (!$order->hasInvoices() ) {
                        $order = Mage::getModel('sales/order')->load($order->getId());
                        $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                                ->load($order->getCustomerId());
                        $current_date = date("Y-m-d");
                        $order_date = $order->getCreatedAt();
                        $new_date = strtotime('+3 day', strtotime($order_date));
                        $compare_date = date('Y-m-d', $new_date);

                        if ($compare_date < $current_date) {
                            
                             $order_array = array();
                              $order_array['customer_name'] = $order->getCustomerName();
                              $order_array['order_number'] = $order->getIncrementId();
                              $order_array['date'] = $order->getCreatedAtFormated('long');
                              $mobile = $order->getBillingAddress()->getTelephone();
                              $email = $order->getCustomerEmail();
                              $message_new = Mage::getSingleton('ezoomerce/config')->getDelayinvoiceTemplate();

                              if (!empty($message_new)) {
                              $message_new = str_replace('[name]', $order_array['customer_name'], $message_new);
                              $message_new = str_replace('[store]', Mage::app()->getStore()->getFrontendName(), $message_new);
                              $message_new = str_replace('[order]', $order_array['order_number'], $message_new);
                              $message_new = str_replace('[date]', $order_array['date'], $message_new);
                              $message = $message_new;
                              } else {

                              $message = 'Hello '.$order_array['customer_name'].' , Your order from '.Mage::app()->getStore()->getFrontendName().' , order#'.$order_array['order_number'].'  which placed on '.$order_array['date'].' are delay in shipping. For details, please call us on 7676 10 4444.';
                              }
                              $mobile = '91'.$order->getBillingAddress()->getTelephone();
                              $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
                            $comment = 'We regret to inform about the delay in shipping your order. For details, please call us on 7676 10 4444.';  
                            $order->sendOrderUpdateEmail(true, $comment );
                            $order->setFlag(1);
                            $order->save();
                        }
                    }
                }
            //}
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function checkDelivery() {
        try {
            $sms_status = Mage::getSingleton('ezoomerce/config')->getDelayDeliveryMode();
            if ($sms_status) {
                $invoice_collection = Mage::getModel("sales/order_invoice")->getCollection();
                foreach ($invoice_collection as $inv) {
                    $inv_incid = $inv->getData('increment_id');
                    $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($inv_incid);
                    $orderId = $invoice->getData('order_id');
                    $order = Mage::getModel('sales/order')->load($orderId);
                    $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                            ->load($order->getCustomerId());
                    //$payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
                    if ($order->hasInvoices() && $inv_incid >= 100000008 && $inv->getFlag() != 1 && $order->canShip() != 0) {
                        $current_date = date("Y-m-d");
                        $inv_date = $inv->getCreatedAt();
                        $new_date = strtotime('+3 day', strtotime($inv_date));
                        $compare_date = date('Y-m-d', $new_date);

                        if ($compare_date < $current_date && $order->getStatus() != "closed") {
                            if (!empty($order)) {
                                $wildcard = Mage::getModel('ezoomerce/wildcard');
                                $sms_template = Mage::getSingleton('ezoomerce/config')->getDelayDeliveryTemplate();
                                $param = array(
                                    'customer' => $customer,
                                    'order' => $order,
                                    'invoice' => $invoice,
                                    'shipment' => '',
                                    'credit_memo' => '',
                                );
                                $sms = $wildcard->getSmsFormat($param, $sms_template);
                                if (!empty($sms)) {
                                    $message = $sms;
                                    $mobile = $order->getBillingAddress()->getTelephone();
                                    $this->sendSms($message, $mobile);
                                }
                            }
                            /* $order_array = array();
                              $order_array['customer_name'] = $order->getCustomerName();
                              $order_array['order_number'] = $order->getIncrementId();
                              $order_array['date'] = $order->getCreatedAtFormated('long');
                              $store = Mage::app()->getStore()->getFrontendName();
                              $mobile = $order->getBillingAddress()->getTelephone();
                              $email = $order->getCustomerEmail();
                              $message_new = Mage::getSingleton('ezoomerce/config')->getDelayDeliveryTemplate();

                              if (!empty($message_new)) {
                              $message_new = str_replace('[name]', $order_array['customer_name'], $message_new);
                              $message_new = str_replace('[store]', $store, $message_new);
                              $message_new = str_replace('[order]', $order_array['order_number'], $message_new);
                              $message_new = str_replace('[date]', $order_array['date'], $message_new);
                              $message = $message_new;
                              } else {

                              $message = 'Hello '.$order_array['customer_name'].' , Your order from '.Mage::app()->getStore()->getFrontendName().' , order#'.$order_array['order_number'].'  which placed on '.$order_array['date'].' has not delivered';
                              }
                              $result = $this->sendSms($message,$mobile); */
                            $invoice->sendUpdateEmail(true, $comment = '');
                            $invoice->setFlag(1);
                            $invoice->save();
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function checkStatus() {
        try {
            $sms_status = Mage::getSingleton('ezoomerce/config')->getDeliveryCloseFeedbackMode();
            if ($sms_status) {
                $orders = Mage::getModel('sales/order')->getCollection()
                        ->addFieldToFilter('status', array('in' => array('delivered', 'closed')))
                        ->addFieldToFilter('flag', array('eq' => 0));
                foreach ($orders as $order) {
                    if (($order->getStatus() == "delivered" || $order->getStatus() == "closed") && $order->getId() >= 25 && $order->getFlag() != 1) {
                        $order = Mage::getModel('sales/order')->load($order->getId());
                        $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                                ->load($order->getCustomerId());
                        if (!empty($order)) {
                            $wildcard = Mage::getModel('ezoomerce/wildcard');
                            $sms_template = Mage::getSingleton('ezoomerce/config')->getDeliveryCloseFeedbackTemplate();
                            $param = array(
                                'customer' => $customer,
                                'order' => $order,
                                'invoice' => '',
                                'shipment' => '',
                                'credit_memo' => '',
                            );
                            $sms = $wildcard->getSmsFormat($param, $sms_template);
                            if (!empty($sms)) {
                                $message = $sms;
                                $mobile = $order->getBillingAddress()->getTelephone();
                                $this->sendSms($message, $mobile);
                            }
                        }
                        /* $order_array = array();
                          $order_array['customer_name'] = $order->getCustomerName();
                          $order_array['order_number'] = $order->getIncrementId();
                          $order_array['date'] = $order->getCreatedAtFormated('long');
                          $store = Mage::app()->getStore()->getFrontendName();
                          $mobile = $order->getBillingAddress()->getTelephone();
                          $email = $order->getCustomerEmail();
                          $status = $order->getStatus();
                          $message_new = Mage::getSingleton('ezoomerce/config')->getDeliveryCloseFeedbackTemplate();

                          if (!empty($message_new)) {
                          $message_new = str_replace('[name]', $order_array['customer_name'], $message_new);
                          $message_new = str_replace('[store]', $store, $message_new);
                          $message_new = str_replace('[order]', $order_array['order_number'], $message_new);
                          $message_new = str_replace('[date]', $order_array['date'], $message_new);
                          $message_new = str_replace('[status]', $status, $message_new);
                          $message = $message_new;
                          } else {
                          $message = 'Hello '.$order_array['customer_name'].' , Your order from '.$store.' , order#'.$order_array['order_number'].'  which placed on '.$order_array['date'].' Your order status:'.$status;
                          }
                          $this->sendSms($message,$mobile); */
                        $order->sendOrderUpdateEmail(true, $comment = '');
                        $order->setFlag(1);
                        $order->save();
                    }
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function sendSms($message = '', $mobile = '') {
        try {

            $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));

            return $sms_status;
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function sendStatusChangeSms($observer) {
        $order = $observer->getOrder();
        $incrementId = $order->getIncrementId();
        $mobile = '91'.$observer->getOrder()->getBillingAddress()->getTelephone();
        $ordertime = date('d.m.Y', strtotime($order->getCreatedAt()));
        $message='';
        if($order->getId()){
            foreach ($order->getAllVisibleItems() as $item)
            {
                $ProdustName .= $item->getName() . ' + ';
            }        
        }
        $productName = rtrim($ProdustName,"+ ");
        $productName =  substr($productName, 0, 25);
        if($order->getStatus()=='cancel_request'){
        $message = 'We have received your cancellation request for '.$productName.'. Our team will reach out to you within 24-48 hours. For queries, call 080-46636363.';    
        }
        if($order->getStatus()=='cancel_request_reject'){
        $message = 'Your cancellation request for '.$productName.' has not been processed, as the issue has been reported to be resolved. For queries, call 080-46636363.';    
        }
        if($order->getStatus()=='cancel_request_approve'){
        $message = 'Your order for '.$productName.' is cancelled. If you have already paid, refund will be processed within 4 business days from our end. For queries, call 080-46636363.';    
        }
        if($order->getStatus()=='redytoship'){
        $message = 'Your order for '.$productName.' is ready to ship. To track/cancel your order visit My Account @ eurekaforbes.com. For queries, call 080-46636363.';    
        }
        if($order->getStatus()=='despatch'){
            $shipmentCollection = Mage::getResourceModel('sales/order_shipment_collection')
            ->setOrderFilter($order)
        ->load();
        foreach ($shipmentCollection as $shipment){
            // This will give me the shipment IncrementId, but not the actual tracking information.
            foreach($shipment->getAllTracks() as $tracknum)
            {
                $tracknums[]=$tracknum->getNumber();
                $trackname[]=$tracknum->getTitle();
            }

        }
        //$zipcode = Mage::getModel('wms/zipcode')->load($observer->getOrder()->getBillingAddress()->getPostcode(),'zipcode');
        //$i = $zipcode->getDeliveryTime();
        $deliverydate = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false). ' + 7 day'));
        $message = 'Your package with '.$productName.' has been dispatched via '.$trackname[0].' with tracking no. '.$tracknums[0].'. Expect delivery by '.$deliverydate.'. For queries, call 080-4663636.';    
        }
        if($order->getStatus()=='delivery_complete'){
        $message = 'Your package with '.$productName.' was successfully delivered. Please check your email for further details or call 080-4663636.';    
        }
        if($order->getStatus()=='exchange_request'){
        $message = 'We have received your replacement request for '.$productName.'. Our team will reach out to you within 24-48 hours. For queries call 080-46636363.';    
        }
        if($order->getStatus()=='exchange_request_reject'){
        $message = 'Your replacement request for '.$productName.' has not been processed, as the issue has been reported to be resolved. For queries, call 080-46636363.';    
        }
        if($order->getStatus()=='exchange_request_approve'){
        $message = 'Your replacement request for '.$productName.' is approved. The new product will be delivered within 7 business days. Please keep your existing product packed at the time of delivery. For queries, call 080-46636363.';    
        }
        if($order->getStatus()=='return_request'){
        $message = 'We have received your return request for '.$productName.'. Our team will reach out to you within 24-48 hours. For queries, call 080-46636363 .';    
        }
        if($order->getStatus()=='return_request_reject'){
        $message = 'Your return request for '.$productName.' has not been processed, as the issue has been reported to be resolved. For queries, call 080-46636363.';    
        }
        if($order->getStatus()=='return_request_approve'){
        $message = 'Your return request for '.$productName.' has been approved. Refund will be processed within 4 business days from our end. For queries, call 080-46636363.';    
        }
        if($order->getStatus()=='refund_initiated'){
        $message = 'Your refund of Rs. '.  intval($order->getGrandTotal()).' for '.$productName.' has been processed from our end. Please check your email for further details. For queries, call 080-46636363.';    
        }
        if($order->getStatus() =='pending' || $order->getStatus() =='processing'){
        //$zipcode = Mage::getModel('wms/zipcode')->load($observer->getOrder()->getBillingAddress()->getPostcode(),'zipcode');
        //$i = $zipcode->getDeliveryTime();
        $deliverydate = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false). ' + 7 day'));   
        $message = 'Your Order for '.$productName.' is confirmed and will be delivered by '.$deliverydate.'. To track/cancel your order visit My Account at eurekaforbes.com. For queries, call 080-46636363.';  
        }
        if($order->getStatus() !=''){
        try {
        $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
        return $sms_status;
        } catch (Exception $e) {
            Mage::logException($e);
        }
        }
    }

    public function controllerActionPostdispatchContactsIndexPost(Varien_Event_Observer $observer) {
        $name = $_POST['name '];
        $mobile = $_POST['telephone'];
        $message = 'Hello '.$name.', thank you for you Query. Our Team Will contact you within 24 hours';
        try {
        $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
        return $sms_status;
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
    public function salesQuoteSaveAfter($observer) {
        $quote = $observer->getEvent()->getQuote();
        if($quote->getItemsCount() == 0) {
            Mage::getSingleton('core/session')->unsQuoteItemExpiration();
            Mage::getSingleton('core/session')->unsQuoteItemExpiration1();
        } /*else {
            Mage::getSingleton('core/session')->unsQuoteItemExpiration();
            Mage::getSingleton('core/session')->unsQuoteItemExpiration1();
            foreach ($observer->getQuote()->getAllItems() as $item) {
                $itemExpiration[$item->getSku()] = (time() + (60));
                Mage::getSingleton('core/session')->setQuoteItemExpiration1($itemExpiration1);
                $itemExpiration1[$item->getSku()] = (time() + (2 * 60));
                Mage::getSingleton('core/session')->setQuoteItemExpiration1($itemExpiration1);
            }
        }*/
        
    }
    /* observes sales_quote_add_item */
    public function addItemExpiration(Varien_Event_Observer $event)
    {
        $item = $event->getEvent()->getQuoteItem();
        $itemExpiration = Mage::getSingleton('core/session')->getQuoteItemExpiration();
        if (!$itemExpiration)$itemExpiration = array();
        $itemExpiration[$item->getSku()] = (time() + (30 * 60));
        Mage::getSingleton('core/session')->setQuoteItemExpiration($itemExpiration);
        $itemExpiration1 = Mage::getSingleton('core/session')->getQuoteItemExpiration1();
        if (!$itemExpiration1)$itemExpiration1 = array();
        $itemExpiration1[$item->getSku()] = (time() + (24 * 60 * 60));
        Mage::getSingleton('core/session')->setQuoteItemExpiration1($itemExpiration1);
    }
    /* observes sales_quote_load_after */
    public function removeExpiredItems(Varien_Event_Observer $event)
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
        $itemExpiration = Mage::getSingleton('core/session')->getQuoteItemExpiration();   //echo '<pre>';print_r($itemExpiration);
        foreach ($event->getQuote()->getAllItems() as $item) {
           if (isset($itemExpiration[$item->getSku()]) && $itemExpiration[$item->getSku()] < time()) { 
                $_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$item->getSku());
                $productName = $_product->getName();
                $productPrice = intval($_product->getPrice());
                $productQty = '1';
                $productimageurl = Mage::helper('catalog/image')->init($_product, 'thumbnail');
                $html .= '<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width:50%;"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width:96%;"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;width:100%;"><tr><td><img src="'.$productimageurl.'" alt="'.$productName.'" title="'.$productName.'" width="59" height="65"/></td><td style="color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;font-size:14px;">'.$productName.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">1</td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center;font-size:14px">RS.'.$productPrice.'/</td></tr></table></td></tr></table>';
                $customer = Mage::getSingleton('customer/session')->getCustomer(); 
                $Username = $customer->getName();
                $useremail =$customer->getEmail();
                $emailTemp = Mage::getModel('core/email_template')->load(16);
                $emailTempVariables = array();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $adminUsername = 'Admin';
                $emailTempVariables['customername'] = $customer->getName();
		$emailTempVariables['html']= $html;
                $emailTempVariables['buylink']= Mage::getBaseUrl().'checkout/cart/';
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);//print_r($processedTemplate);die;
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
                $emailTemp->send($useremail,$Username,$emailTempVariables);
                Mage::getSingleton('core/session')->unsQuoteItemExpiration();
             }
           }
        }
        
        
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
        $itemExpiration1 = Mage::getSingleton('core/session')->getQuoteItemExpiration1();  //echo '<pre>';print_r($itemExpiration1);
        foreach ($event->getQuote()->getAllItems() as $item) {
           if (isset($itemExpiration1[$item->getSku()]) && $itemExpiration1[$item->getSku()] < time()) {
                $_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$item->getSku());
                $productName = $_product->getName();
                $productPrice = intval($_product->getPrice());
                $productQty = '1';
                $productimageurl = Mage::helper('catalog/image')->init($_product, 'thumbnail');
                $html1 .= '<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width:50%;"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width:97%;"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$productimageurl.'" alt="'.$productName.'" title="'.$productName.'" width="59" height="75"/></td><td style="font-size:14px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$productName.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">1</td><td style=" font-size:14px;border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$productPrice.'/</td></tr></table></td></tr></table>';
                $customer = Mage::getSingleton('customer/session')->getCustomer(); 
                $Username = $customer->getName();
                $useremail =$customer->getEmail();
                $emailTemp = Mage::getModel('core/email_template')->load(17);
                $emailTempVariables = array();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $adminUsername = 'Admin';
                $emailTempVariables['customername'] = $customer->getName();
		$emailTempVariables['html']= $html1;
                $emailTempVariables['buylink']= Mage::getBaseUrl().'checkout/cart/';
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);//print_r($processedTemplate);die;
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
                $emailTemp->send($useremail,$Username,$emailTempVariables);
                Mage::getSingleton('core/session')->unsQuoteItemExpiration1();
             }
           }
        }
        
    }

}
