<?php

/**
 * SMS config model
 *
 * @package    Ezoomerce SMS
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Model_Config extends Varien_Object {

    // for email configuration
    const XML_PATH_SOAP_URL = 'ezoomerce/email_conf_cms/soap_url';
    const XML_PATH_REALM = 'ezoomerce/email_conf_cms/realm';
    const XML_PATH_REALM_USER = 'ezoomerce/email_conf_cms/user';
    const XML_PATH_REALM_PASS = 'ezoomerce/email_conf_cms/password';

    // for email configuration method
    public function getSoapUrl($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_SOAP_URL, $storeId));
    }

    public function getRealm($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REALM, $storeId));
    }

    public function getRealmUser($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REALM_USER, $storeId));
    }

    public function getRealmPass($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REALM_PASS, $storeId));
    }

    ////////////////////////////////////////////////////////////////////////////////
    //get email group id & message id
    const XML_PATH_REGISTRATION_ACTIVE = 'ezoomerce/email_cms/registration_active';
    const XML_PATH_REGISTRATION_GROUPID = 'ezoomerce/email_cms/registration_groupid';
    const XML_PATH_REGISTRATION_MESSAGEID = 'ezoomerce/email_cms/registration_messid';

    public function getRegistrationActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REGISTRATION_ACTIVE, $storeId));
    }

    public function getRegistrationGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REGISTRATION_GROUPID, $storeId));
    }

    public function getRegistrationMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REGISTRATION_MESSAGEID, $storeId));
    }

    const XML_PATH_GUESTCHECKOUT_ACTIVE = 'ezoomerce/email_cms/guestcheckout_active';
    const XML_PATH_GUESTCHECKOUT_GROUPID = 'ezoomerce/email_cms/guestcheckout_groupid';
    const XML_PATH_GUESTCHECKOUT_MESSAGEID = 'ezoomerce/email_cms/guestcheckout_messid';

    public function getGuestcheckoutActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_GUESTCHECKOUT_ACTIVE, $storeId));
    }

    public function getGuestcheckoutGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_GUESTCHECKOUT_GROUPID, $storeId));
    }

    public function getGuestcheckoutMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_GUESTCHECKOUT_MESSAGEID, $storeId));
    }

    const XML_PATH_NEWORDER_ACTIVE = 'ezoomerce/email_cms/neworder_active';
    const XML_PATH_NEWORDER_GROUPID = 'ezoomerce/email_cms/neworder_groupid';
    const XML_PATH_NEWORDER_MESSAGEID = 'ezoomerce/email_cms/neworder_messid';

    public function getNeworderActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_NEWORDER_ACTIVE, $storeId));
    }

    public function getNeworderGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_NEWORDER_GROUPID, $storeId));
    }

    public function getNeworderMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_NEWORDER_MESSAGEID, $storeId));
    }

    const XML_PATH_CANCEL_ACTIVE = 'ezoomerce/email_cms/cancel_active';
    const XML_PATH_CANCEL_GROUPID = 'ezoomerce/email_cms/cancel_groupid';
    const XML_PATH_CANCEL_MESSAGEID = 'ezoomerce/email_cms/cancel_messid';

    public function getCancelActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_CANCEL_ACTIVE, $storeId));
    }

    public function getCancelGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_CANCEL_GROUPID, $storeId));
    }

    public function getCancelMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_CANCEL_MESSAGEID, $storeId));
    }

    const XML_PATH_DELAYINVOICE_ACTIVE = 'ezoomerce/email_cms/delayinvoice_active';
    const XML_PATH_DELAYINVOICE_GROUPID = 'ezoomerce/email_cms/delayinvoice_groupid';
    const XML_PATH_DELAYINVOICE_MESSAGEID = 'ezoomerce/email_cms/delayinvoice_messid';

    public function getDelayinvoiceActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYINVOICE_ACTIVE, $storeId));
    }

    public function getDelayinvoiceGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYINVOICE_GROUPID, $storeId));
    }

    public function getDelayinvoiceMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYINVOICE_MESSAGEID, $storeId));
    }

    const XML_PATH_DELAYDELIVERY_ACTIVE = 'ezoomerce/email_cms/delaydelivery_active';
    const XML_PATH_DELAYDELIVERY_GROUPID = 'ezoomerce/email_cms/delaydelivery_groupid';
    const XML_PATH_DELAYDELIVERY_MESSAGEID = 'ezoomerce/email_cms/delaydelivery_messid';

    public function getDelaydeliveryActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYDELIVERY_ACTIVE, $storeId));
    }

    public function getDelaydeliveryGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYDELIVERY_GROUPID, $storeId));
    }

    public function getDelaydeliveryMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYDELIVERY_MESSAGEID, $storeId));
    }

    const XML_PATH_INVOICE_ACTIVE = 'ezoomerce/email_cms/invoice_active';
    const XML_PATH_INVOICE_GROUPID = 'ezoomerce/email_cms/invoice_groupid';
    const XML_PATH_INVOICE_MESSAGEID = 'ezoomerce/email_cms/invoice_messid';

    public function getInvoiceActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_INVOICE_ACTIVE, $storeId));
    }

    public function getInvoiceGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_INVOICE_GROUPID, $storeId));
    }

    public function getInvoiceMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_INVOICE_MESSAGEID, $storeId));
    }

    const XML_PATH_SHIPMENT_ACTIVE = 'ezoomerce/email_cms/shipment_active';
    const XML_PATH_SHIPMENT_GROUPID = 'ezoomerce/email_cms/shipment_groupid';
    const XML_PATH_SHIPMENT_MESSAGEID = 'ezoomerce/email_cms/shipment_messid';

    public function getShipmentActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_SHIPMENT_ACTIVE, $storeId));
    }

    public function getShipmentGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_SHIPMENT_GROUPID, $storeId));
    }

    public function getShipmentMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_SHIPMENT_MESSAGEID, $storeId));
    }

    const XML_PATH_FEEDBACK_ACTIVE = 'ezoomerce/email_cms/delivery_close_feedback_active';
    const XML_PATH_FEEDBACK_GROUPID = 'ezoomerce/email_cms/delivery_close_feedback_groupid';
    const XML_PATH_FEEDBACK_MESSAGEID = 'ezoomerce/email_cms/delivery_close_feedback_messid';

    public function getFeedbackActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_FEEDBACK_ACTIVE, $storeId));
    }

    public function getFeedbackGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_FEEDBACK_GROUPID, $storeId));
    }

    public function getFeedbackMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_FEEDBACK_MESSAGEID, $storeId));
    }

    const XML_PATH_REFUND_ACTIVE = 'ezoomerce/email_cms/refund_active';
    const XML_PATH_REFUND_GROUPID = 'ezoomerce/email_cms/refund_groupid';
    const XML_PATH_REFUND_MESSAGEID = 'ezoomerce/email_cms/refund_messid';

    public function getRefundActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REFUND_ACTIVE, $storeId));
    }

    public function getRefundGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REFUND_GROUPID, $storeId));
    }

    public function getRefundMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REFUND_MESSAGEID, $storeId));
    }

    const XML_PATH_BACKINSTOCK_ACTIVE = 'ezoomerce/email_cms/backinstock_active';
    const XML_PATH_BACKINSTOCK_GROUPID = 'ezoomerce/email_cms/backinstock_groupid';
    const XML_PATH_BACKINSTOCK_MESSAGEID = 'ezoomerce/email_cms/backinstock_messid';

    public function getBackinstockActive($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_BACKINSTOCK_ACTIVE, $storeId));
    }

    public function getBackinstockGroupId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_BACKINSTOCK_GROUPID, $storeId));
    }

    public function getBackinstockMessId($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_BACKINSTOCK_MESSAGEID, $storeId));
    }

    ////////////////////////////////////////////////////////////////////////////////
    // for sms configuration
    const XML_PATH_KEY = 'ezoomerce/sms_conf_cms/key';
    const XML_PATH_SMS_URL = 'ezoomerce/sms_conf_cms/sms_url';
    const XML_PATH_PASSWORD = 'ezoomerce/sms_conf_cms/password';
    const XML_PATH_SENDERID = 'ezoomerce/sms_conf_cms/senderid';

    // for sms configuration method
    public function getKey($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_KEY, $storeId));
    }

    public function getSmsUrl($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_SMS_URL, $storeId));
    }

    public function getPassword($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_PASSWORD, $storeId));
    }

    public function getSenderid($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_SENDERID, $storeId));
    }

    // Get SMS  Mode & Template
    // customer new registration
    const XML_PATH_REGISTRATION_MODE = 'ezoomerce/sms_cms/registration_mode';
    const XML_PATH_REGISTRATION_TEMPLATE = 'ezoomerce/sms_cms/registration_template';

    public function getRegistrationMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REGISTRATION_MODE, $storeId));
    }

    public function getRegistrationTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REGISTRATION_TEMPLATE, $storeId));
    }

    // guest checkout
    const XML_PATH_GUESTCHECKOUT_MODE = 'ezoomerce/sms_cms/gest_mode';
    const XML_PATH_GUESTCHECKOUT_TEMPLATE = 'ezoomerce/sms_cms/guestcheckout_template';

    public function getGuestCheckoutMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_GUESTCHECKOUT_MODE, $storeId));
    }

    public function getGuestCheckoutTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_GUESTCHECKOUT_TEMPLATE, $storeId));
    }

    // new order confirmation
    const XML_PATH_NEWORDER_MODE = 'ezoomerce/sms_cms/neworder_mode';
    const XML_PATH_NEWORDER_TEMPLATE = 'ezoomerce/sms_cms/neworder_template';

    public function getNewOrderMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_NEWORDER_MODE, $storeId));
    }

    public function getNewOrderTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_NEWORDER_TEMPLATE, $storeId));
    }

    // order canceletion request
    const XML_PATH_CANCELREQUEST_MODE = 'ezoomerce/sms_cms/cancelrequest_mode';
    const XML_PATH_CANCELREQUEST_TEMPLATE = 'ezoomerce/sms_cms/cancelrequest_template';

    public function getCancelRequestMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_CANCELREQUEST_MODE, $storeId));
    }

    public function getCancelRequestTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_CANCELREQUEST_TEMPLATE, $storeId));
    }

    // order canceletion confirm
    const XML_PATH_CANCEL_MODE = 'ezoomerce/sms_cms/cancel_mode';
    const XML_PATH_CANCELATION_TEMPLATE = 'ezoomerce/sms_cms/cancel_template';

    public function getCancelMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_CANCEL_MODE, $storeId));
    }

    public function getCancelTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_CANCELATION_TEMPLATE, $storeId));
    }

    // invoice creation
    const XML_PATH_INVOICE_MODE = 'ezoomerce/sms_cms/invoice_mode';
    const XML_PATH_INVOICE_TEMPLATE = 'ezoomerce/sms_cms/invoice_template';

    public function getInvoiceMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_INVOICE_MODE, $storeId));
    }

    public function getInvoiceTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_INVOICE_TEMPLATE, $storeId));
    }

    // delay invoice creation
    const XML_PATH_DELAYINVOICE_MODE = 'ezoomerce/sms_cms/delayinvoice_mode';
    const XML_PATH_DELAYINVOICE_TEMPLATE = 'ezoomerce/sms_cms/delayinvoice_template';

    public function getDelayInvoiceMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYINVOICE_MODE, $storeId));
    }

    public function getDelayinvoiceTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYINVOICE_TEMPLATE, $storeId));
    }

    // delay delivery creation
    const XML_PATH_DELAYDELIVERY_MODE = 'ezoomerce/sms_cms/delaydelivery_mode';
    const XML_PATH_DELAYDELIVERY_TEMPLATE = 'ezoomerce/sms_cms/delaydelivery_template';

    public function getDelayDeliveryMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYDELIVERY_MODE, $storeId));
    }

    public function getDelayDeliveryTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELAYDELIVERY_TEMPLATE, $storeId));
    }

    // ready to ship creation
    const XML_PATH_SHIPMENTCREATE_MODE = 'ezoomerce/sms_cms/shipmentcreate_mode';
    const XML_PATH_SHIPMENT_TEMPLATE = 'ezoomerce/sms_cms/shipment_template';

    public function getShipmentcreateMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_SHIPMENTCREATE_MODE, $storeId));
    }

    public function getShipmentTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_SHIPMENT_TEMPLATE, $storeId));
    }

    // delivery / close feedback
    const XML_PATH_DELIVERYCLOSEFEEDBACK_MODE = 'ezoomerce/sms_cms/delivery_close_feedback_mode';
    const XML_PATH_DELIVERYCLOSEFEEDBACK_TEMPLATE = 'ezoomerce/sms_cms/delivery_close_feedback_template';

    public function getDeliveryCloseFeedbackMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELIVERYCLOSEFEEDBACK_MODE, $storeId));
    }

    public function getDeliveryCloseFeedbackTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_DELIVERYCLOSEFEEDBACK_TEMPLATE, $storeId));
    }

    // order refund
    const XML_PATH_REFUND_MODE = 'ezoomerce/sms_cms/refund_mode';
    const XML_PATH_REFUND_TEMPLATE = 'ezoomerce/sms_cms/refund_template';

    public function getRefundMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REFUND_TEMPLATE, $storeId));
    }

    public function getRefundTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_REFUND_TEMPLATE, $storeId));
    }

    // back in stock
    const XML_PATH_BACKINSTOCK_MODE = 'ezoomerce/sms_cms/backinstock_mode';
    const XML_PATH_BACKINSTOCK_TEMPLATE = 'ezoomerce/sms_cms/backinstock_template';

    public function getBackinStockMode($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_BACKINSTOCK_MODE, $storeId));
    }

    public function getBackinStockTemplate($storeId = null) {
        return trim(Mage::getStoreConfig(self::XML_PATH_BACKINSTOCK_TEMPLATE, $storeId));
    }

    ////////////////////////////////////////////////////////////////////////////////
}

?>