<?php

/**
 * @package    Ezoomerce SMS
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Model_Invoice extends Mage_Sales_Model_Order_Invoice {

    public function sendSms($model = null, $sms_template = null) {
        try {
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($model->getCustomerId());
            if (!empty($model)) {
                $wildcard = Mage::getModel('ezoomerce/wildcard');

                $param = array(
                    'customer' => $customer,
                    'order' => $model,
                    'invoice' => $this,
                    'shipment' => '',
                    'credit_memo' => '',
                );
                $sms = $wildcard->getSmsFormat($param, $sms_template);
                if (!empty($sms)) {
                    $message = $sms;
                    $mobile = '91'.$model->getBillingAddress()->getTelephone();
                    $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
                    return true;
                }
                return true;
            }
            return true;
        } catch (Exception $e) {
            
        }
    }

    /**
     * Send email with invoice data
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order_Invoice
     */
    public function sendEmail($notifyCustomer = true, $comment = '') {
        $order = $this->getOrder();
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewInvoiceEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                    ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        //Change the banners on the emails - Kushal
        $items = $order->getAllItems();
        foreach ($items as $item) {
            $product_id = $item['product_id'];
            $_product = Mage::getModel('catalog/product')->load($product_id);

            $cat = $_product->getCategoryIds();
            $cat_name[0] = "";
            $cat_name[1] = "";
            $cat_name[2] = "";
            $j = 0;
            for ($i = 0; $i < count($cat); $i++) {
                $topCategory = Mage::getResourceModel('catalog/category_collection')
                        ->addIdFilter($cat[$i])
                        ->setOrder('level', 'ASC')
                        ->setPage(1, 1)
                        ->getFirstItem();
                $cats = explode("/", $topCategory->getPath());
                if (!isset($cats[3]))
                    continue;
                if ($cats[3] == "46") {
                    $email_banner = "transaction-mailer-banner-REF.jpg";
                    break 2;
                }
                if ($cats[3] == "39") {
                    $email_banner = "transaction-mailer-banner-TL-WM.jpg";
                    break 2;
                }
                if ($cats[3] == "40") {
                    $email_banner = "transaction-mailer-banner-TL-WM.jpg";
                    break 2;
                }
                if ($cats[3] == "41") {
                    $email_banner = "transaction-mailer-banner-CD.jpg";
                    break 2;
                }
                if ($cats[3] == "75") {
                    $email_banner = "transaction-mailer-banner-AC.jpg";
                    break 2;
                }
            }
        }

        // End - Banner change on emails - Kushal
        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
            'order' => $order,
            'invoice' => $this,
            'comment' => $comment,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $paymentBlockHtml,
            'email_banner' => $email_banner,
                )
        );

        // Custom code added by Rajesh for sending mail
        //send confirm sms
        $sms_status = Mage::getSingleton('ezoomerce/config')->getInvoiceMode();
        if ($sms_status) {
            $sms_template = Mage::getSingleton('ezoomerce/config')->getInvoiceTemplate();
            $this->sendSms($this->getOrder(), $sms_template); // custom code for sending order confirmation sms
        }

        //send confirm email
        $isActive = Mage::getSingleton('ezoomerce/config')->getInvoiceActive();
        $groupId = Mage::getSingleton('ezoomerce/config')->getInvoiceGroupId();
        $msg_id = Mage::getSingleton('ezoomerce/config')->getInvoiceMessId();
        if ($isActive) {
            $orderId = $this->getData('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($order->getCustomerId());
            $email = $order->getCustomerEmail();
            $firstName = $order->getShippingAddress()->getFirstname();
            $user = $firstName;
            $password = $customer->generatePassword();
            $store = Mage::app()->getStore()->getFrontendName();
            $mobile = $order->getBillingAddress()->getTelephone();
            if (!empty($groupId) && is_null($groupId)) {
                $groupId = "1800029625";
            }
            if (!empty($msg_id) && is_null($msg_id)) {
                $msg_id = "1600205586";
            }
            $params = array('email' => $email, 'firstName' => $firstName, 'user' => $user, 'password' => $password, 'groupId' => $groupId, 'msg_id' => $msg_id);
            $mailer->sending($params); //custom
            //$this->setEmailSent(true);
            $this->_getResource()->saveAttribute($this, 'email_sent');
        } else {
            $mailer->send(); // default
            //$this->setEmailSent(true);
            $this->_getResource()->saveAttribute($this, 'email_sent');
        }
        // End custom code
        return $this;
    }

    /**
     * Send email with invoice update information
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order_Invoice
     */
    public function sendUpdateEmail($notifyCustomer = true, $comment = '') {
        $order = $this->getOrder();
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendInvoiceCommentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_UPDATE_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
            'order' => $order,
            'invoice' => $this,
            'comment' => $comment,
            'billing' => $order->getBillingAddress()
                )
        );
        // Custom code added by Rajesh for sending mail
        $isActive = Mage::getSingleton('ezoomerce/config')->getDelaydeliveryActive();
        $groupId = Mage::getSingleton('ezoomerce/config')->getDelaydeliveryGroupId();
        $msg_id = Mage::getSingleton('ezoomerce/config')->getDelaydeliveryMessId();
        if ($isActive) {
            $orderId = $this->getData('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($order->getCustomerId());
            $email = $order->getCustomerEmail();
            $firstName = $order->getShippingAddress()->getFirstname();
            $user = $firstName;
            $password = $customer->generatePassword();
            $store = Mage::app()->getStore()->getFrontendName();
            $mobile = $order->getBillingAddress()->getTelephone();
            if (!empty($groupId) && is_null($groupId)) {
                $groupId = "1800029625";
            }
            if (!empty($msg_id) && is_null($msg_id)) {
                $msg_id = "1600205586";
            }
            $params = array('email' => $email, 'firstName' => $firstName, 'user' => $user, 'password' => $password, 'groupId' => $groupId, 'msg_id' => $msg_id);
            $mailer->sending($params); //custom
        } else {
            $mailer->send(); // default
        }
        // End custom code

        return $this;
    }

}
