<?php

/**
 * @package    Ezoomerce SMS
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Model_Soapclient extends Varien_Object {
	public static $client;
	public static $realm;
	public static $email_user;
	public static $email_password;
	//public static $jsession;
    protected function _construct() {
		self::$client = new soapclient(Mage::getSingleton('ezoomerce/config')->getSoapUrl());
		self::$realm = Mage::getSingleton('ezoomerce/config')->getRealm();
		self::$email_user = Mage::getSingleton('ezoomerce/config')->getRealmUser();
		self::$email_password = Mage::getSingleton('ezoomerce/config')->getRealmPass();
    }
	/**
	 * Static soapClient object
	 * @param $wsdl Soap Wsdl string link
	 */

	public function updateMembertoSend($email=null,$firstName=null,$user=null,$pass=null,$groupId=null,$msg_id=null)
	{
        ini_set('display_errors',1);
		error_reporting(E_ALL ^ E_NOTICE); 
		set_time_limit(0);
		
		$sendMessage=0; // Keep it always 0 to prevent sending system email
		$retry_count=1;
		$max_retry=3;
		/*$realm = 'https://secure.ecircle-ag.com/e_ifbappliances/';
		$email_user = 'rahul_pramanik@ifbglobal.com';
		$email_password = 'kenscio';*/

		$logonparams = array('realm' => self::$realm,'user' => self::$email_user,'passwd' => self::$email_password);						
			try	{	$result = self::$client->logon($logonparams);	}
			catch(Exception $e)	{
				//die("Invalid Login Details : ".$e->getMessage());
				Mage::getSingleton('core/session')->addError("Invalid Login Details : ".$e->getMessage());	
				}
				
			$sessionid = $result->logonReturn;
			$blacklist=array('session' => $sessionid,'email' =>  $email);	
			$retry_count=1;
			unset($result);
		
			while(!$result && $retry_count <= $max_retry)
			{
				try	{$result=self::$client->isEmailOnSystemBlacklist($blacklist);	}
				catch(Exception $e)	{
					//echo "Message: Exception Occurred : ".$e->getMessage();
					Mage::getSingleton('core/session')->addError("Message: Exception Occurred : ".$e->getMessage());
									}
				$retry_count++;
				sleep(1);
			}
			if($retry_count>$max_retry && !$result)
			{
				die('Blacklist check failed after 3 attempts'); 
			}
			
			$blacklist_check=$result->isEmailOnSystemBlacklistReturn;
				
			if($blacklist_check==1)
			{	
				//echo "User is blacklisted";
				Mage::getSingleton('core/session')->addError('User is blacklisted');
				//exit;
			}
			else
			{
				$member='<?xml version="1.0" ?>
						<member xmlns="http://webservices.ecircle-ag.com/ecm">
						<email>'.$email.'</email>
						<FirstName>'.$firstName.'</FirstName>
						<namedattr name="Username">'.$user.'</namedattr>
						<namedattr name="Password">'.$pass.'</namedattr>
						</member>';
					
				$params = array('session' => $sessionid,
								'memberXml' => $member,
								'groupId' => $groupId,
								'sendMessage' => 0);
		
				$retry_count=1;
				unset($result);
			
				while(!$result && $retry_count <= $max_retry)
				{
					try
					{
						$result = self::$client->createOrUpdateUserMemberByEmail($params);
					}
					catch(Exception $e)
					{
						//echo "<br/>Unable to create user <br/>" .$e->getMessage();
						Mage::getSingleton('core/session')->addError("<br/>Unable to create user <br/>" .$e->getMessage());
					}
					$retry_count++;
					sleep(1);
				}
				
				if($retry_count>$max_retry && !$result)
				{
					//die('CreateOrUpdateMember failed after 3 attempts'); 
					Mage::getSingleton('core/session')->addError("CreateOrUpdateMember failed after 3 attempts");
				}
				//echo "Member added to group successfully...";
				
				$userId=$result->createOrUpdateUserMemberByEmailReturn;
		
				$retry_count=1;
				unset($result);
				
				while(!$result && $retry_count <= $max_retry)
				{
					$sysparams = array('session' => $sessionid,
					'sendMessage' => 0,
					'singleMessageId'=>$msg_id,
					'userId'=>$userId);			
				
					try
					{
						$result= self::$client->sendSingleMessageToUser($sysparams);
					}
					catch(Exception $e)
					{
						Mage::getSingleton('core/session')->addError("Unable to send message".$e->getMessage());
						//echo "Unable to send message".$e->getMessage();
						//exit;
					}
					$retry_count++;
					sleep(1);
				}
				if($retry_count>$max_retry && !$result)
				{
					//die('sendSingleMessageToUser failed after 3 attempts. Unable to send mes');
					Mage::getSingleton('core/session')->addError('sendSingleMessageToUser failed after 3 attempts. Unable to send mes');
				}
				//echo "Email sent to ".$email;
				//Mage::getSingleton('core/session')->addSuccess("Email sent to ".$email);
				return;
			}
			return;	
	}
    public function changeStatus($event) {

        $order = $event->getEvent()->getOrder();
        $email = 'sk.rajesh@bluehorse.in';
		$firstName = 'Sk';
		$user = $firstName;
		$pass = "password";
		$autoresponders = Mage::getModel('fidelitas/autoresponders')
							->getCollection()->addFieldToFilter('event', 'order_status');
		foreach ($autoresponders as $autoresponder) {
				$groupId = $autoresponder['group_id'];
				$msg_id = $autoresponder['message_id'];
				$this->updateMembertoSend($email,$firstName,$user,$pass,$groupId,$msg_id);
			}
			//http://124.7.51.142/sendsms.aspx?uname=ifb&pwd=ifb@123&senderid=IFBCRM&to=xxxxxxxxxx&msg=Test
		return;
		/*		
		[autoresponder_id] => 2
            [event] => order_status
            [send_moment] => after
            [after_hours] => 0
            [after_days] => 0
            [send_once] => 1
            [group_id] => 1800029625
            [message_id] => 18004409
            [from_date] => 2014-01-01
            [to_date] => 2014-02-28
            [active] => 1
            [message] => Testing message for customer registration	*/	
    }
	public function guestCheckoutSuccess($event)
    { 
		$customer=$event->getEvent()->getCustomer();
    	$email = 'sk.rajesh@bluehorse.in';
		$firstName = 'Sk';
		$user = $firstName;
		$pass = "password";
		$autoresponders = Mage::getModel('fidelitas/autoresponders')
							->getCollection()->addFieldToFilter('event', 'order_status');
		foreach ($autoresponders as $autoresponder) {
				$groupId = $autoresponder['group_id'];
				$msg_id = $autoresponder['message_id'];
				$this->updateMembertoSend($email,$firstName,$user,$pass,$groupId,$msg_id);
			}
			//http://124.7.51.142/sendsms.aspx?uname=ifb&pwd=ifb@123&senderid=IFBCRM&to=xxxxxxxxxx&msg=Test
		return;
    }
}
