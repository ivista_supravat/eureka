<?php
/*
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Bh_Ezoomerce_Model_Template extends Mage_Core_Model_Email_Template
{
///////////////////////////////////////////////////////////////////////
// Custom code for get email body html
    public function sendEzmail($email, $name = null, array $variables = array(), $Params = array())
    {
        try {
            $template_html = $this->getProcessedTemplate($variables, true);
   			$split_templates = str_split($template_html, 2000);
			//echo $total_attributes = count($split_templates);
			//echo "<br>";
			//echo "-------------";
			//echo "<br>";
			//foreach($split_templates as $attribute)
			//{
			//	var_dump($attribute);
			//	echo "<br>";
			//	echo "-------------";
			//	echo "<br>";
			//}
			//die();
			$Soapclient = Mage::getModel('ezoomerce/Soapclient');
			$Soapclient->updateMembertoSend($Params['email'],$Params['firstName'],
											$Params['user'],$Params['password'],
											$Params['groupId'],$Params['msg_id']);
			return true;
			// here is sending code
			
        }
        catch (Exception $e) {
            Mage::logException($e);
            return false;
        }

        return true;
    }
	
public function sendEzoomerceMail($templateId, $sender, $email, $name, $bccemail, $vars=array(), $storeId=null, $Params = array())
    {
        $this->setSentSuccess(false);
        if (($storeId === null) && $this->getDesignConfig()->getStore()) {
            $storeId = $this->getDesignConfig()->getStore();
        }

        if (is_numeric($templateId)) {
            $this->load($templateId);
        } else {
            $localeCode = Mage::getStoreConfig('general/locale/code', $storeId);
            $this->loadDefault($templateId, $localeCode);
        }

        if (!$this->getId()) {
            throw Mage::exception('Mage_Core', Mage::helper('core')->__('Invalid transactional email code: ' . $templateId));
        }
		
        if (!isset($vars['store'])) {
            $vars['store'] = Mage::app()->getStore($storeId);
        }
        //$this->setSentSuccess($this->sendEzmail($email, $name, $vars));
		$this->sendEzmail($email, $name, $vars, $Params);
        return;
    }
///////////////////////////////////////////////////////////////////////
}
