<?php

/**
 * @package    Ezoomerce SMS
 * @author     Rajesh, sk.rajesh@bluehorse.in
 */
class Bh_Ezoomerce_Model_Shipment extends Mage_Sales_Model_Order_Shipment {

    public function sendSms($model = null, $sms_template = null) {
        try {
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($model->getCustomerId());
            if (!empty($model)) {
                $wildcard = Mage::getModel('ezoomerce/wildcard');

                $param = array(
                    'customer' => $customer,
                    'order' => $model,
                    'invoice' => '',
                    'shipment' => $this,
                    'credit_memo' => '',
                );
                $sms = $wildcard->getSmsFormat($param, $sms_template);
                if (!empty($sms)) {
                    $message = $sms;
                    $mobile = '91'.$model->getBillingAddress()->getTelephone();
                    $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
                    return true;
                }
                return true;
            }
            return true;
        } catch (Exception $e) {
            
        }
    }

    /**
     * Send email with shipment data
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order_Shipment
     */
    public function sendEmail($notifyCustomer = true, $comment = '') {
        $order = $this->getOrder();
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewShipmentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                    ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
            'order' => $order,
            'shipment' => $this,
            'comment' => $comment,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $paymentBlockHtml
                )
        );
        // Custom code added by Rajesh for sending mail
        //send confirm sms
        $sms_status = Mage::getSingleton('ezoomerce/config')->getShipmentcreateMode();
        if ($sms_status) {
            $sms_template = Mage::getSingleton('ezoomerce/config')->getShipmentTemplate();
            $this->sendSms($this->getOrder(), $sms_template); // custom code for sending order confirmation sms
        }
        //send confirm email
        $isActive = Mage::getSingleton('ezoomerce/config')->getShipmentActive();
        $groupId = Mage::getSingleton('ezoomerce/config')->getShipmentGroupId();
        $msg_id = Mage::getSingleton('ezoomerce/config')->getShipmentMessId();
        if ($isActive) {
            $orderId = $this->getData('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->load($order->getCustomerId());
            $email = $order->getCustomerEmail();
            $firstName = $order->getShippingAddress()->getFirstname();
            $user = $firstName;
            $password = $customer->generatePassword();
            $store = Mage::app()->getStore()->getFrontendName();
            $mobile = $order->getBillingAddress()->getTelephone();
            if (!empty($groupId) && is_null($groupId)) {
                $groupId = "1800029625";
            }
            if (!empty($msg_id) && is_null($msg_id)) {
                $msg_id = "1600205586";
            }
            $params = array('email' => $email, 'firstName' => $firstName, 'user' => $user, 'password' => $password, 'groupId' => $groupId, 'msg_id' => $msg_id);
            $mailer->sending($params); //custom
        } else {
            $mailer->send(); // default
        }
        // End custom code
        return $this;
    }

    /**
     * Send email with shipment update information
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order_Shipment
     */
    public function sendUpdateEmail($notifyCustomer = true, $comment = '') {
        $order = $this->getOrder();
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendShipmentCommentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_UPDATE_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
            'order' => $order,
            'shipment' => $this,
            'comment' => $comment,
            'billing' => $order->getBillingAddress()
                )
        );
        $mailer->send();

        return $this;
    }

}
