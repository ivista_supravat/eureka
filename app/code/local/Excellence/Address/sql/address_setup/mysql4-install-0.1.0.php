<?php

$installer = $this;

$installer->startSetup();

$this->addAttribute('customer_address', 'house_no', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'House NO#',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'visible_on_front' => 1
));
$this->addAttribute('customer_address', 'landmark', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'Landmark',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'visible_on_front' => 1
));

if (version_compare(Mage::getVersion(), '1.6.0', '<=')) {

    $customer = Mage::getModel('customer/address');
    $attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
    $this->addAttributeToSet('customer_address', $attrSetId, 'General', 'house_no');
    $this->addAttributeToSet('customer_address', $attrSetId, 'General', 'landmark');
}

if (version_compare(Mage::getVersion(), '1.4.2', '>=')) {

    Mage::getSingleton('eav/config')
            ->getAttribute('customer_address', 'house_no')
            ->setData('used_in_forms', array('customer_register_address', 'customer_address_edit', 'adminhtml_customer_address'))
            ->save();

    Mage::getSingleton('eav/config')
            ->getAttribute('customer_address', 'landmark')
            ->setData('used_in_forms', array('customer_register_address', 'customer_address_edit', 'adminhtml_customer_address'))
            ->save();
}

$tablequote = $this->getTable('sales/quote_address');
$installer->run("
ALTER TABLE  $tablequote ADD  `house_no` varchar(255) NOT NULL AFTER `street`
");

$tablequote = $this->getTable('sales/quote_address');
$installer->run("
ALTER TABLE  $tablequote ADD  `landmark` varchar(255) NOT NULL AFTER `street`
");

$tablequote = $this->getTable('sales/order_address');
$installer->run("
ALTER TABLE  $tablequote ADD  `house_no` varchar(255) NOT NULL AFTER `street`
");

$tablequote = $this->getTable('sales/order_address');
$installer->run("
ALTER TABLE  $tablequote ADD  `landmark` varchar(255) NOT NULL AFTER `street`
");

$installer->endSetup();
