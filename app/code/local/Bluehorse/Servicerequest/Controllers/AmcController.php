<?php
 
class Bluehorse_Servicerequest_AmcController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());/*redirect to home page*/
    }
 
    public function step1Action()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function step2Action()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function step3Action()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function thankyouAction()
    {
        $Username = 'Keshab Karmakar';
        $useremail = 'keshab.karmakar@bluehorse.in';
        $emailTemp = Mage::getModel('core/email_template')->load(4);
        $emailTempVariables = array();
        $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
        $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
        $adminUsername = 'Admin';
        $emailTempVariables['customername'] = 'Keshab Karmakar';
        $emailTempVariables['content']=' Thank you! for purchasing this amc';
        $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        $message = 'Thank you! We have received your service request and your service ticket number is xxxxxx.';
        $mobile = '918553007539';
        $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
        $this->loadLayout();
        $this->renderLayout();
    }
    public function myamclistAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function mylogserviceslistAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function buyamcinformAction()
    {
        $Username = 'Keshab Karmakar';
        $useremail = 'keshab.karmakar@bluehorse.in';
        $emailTemp = Mage::getModel('core/email_template')->load(4);
        $emailTempVariables = array();
        $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
        $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
        $adminUsername = 'Admin';
        $emailTempVariables['customername'] = 'Keshab Karmakar';
        $emailTempVariables['content']=' your have 30 days left to purchase AMC for your product';
        $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        $message = 'Thank you! We have received your service request and your service ticket number is xxxxxx.';
        $mobile = '918553007539';
        $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
        Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());
    }
}
?>