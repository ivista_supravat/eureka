<?php
 
class Bluehorse_Servicerequest_RequestController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());
    }
 
    public function logrequestAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function escalarateAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function servicerequestAction()
    {
        $Username = 'Keshab Karmakar';
        $useremail = 'keshab.karmakar@bluehorse.in';
        $emailTemp = Mage::getModel('core/email_template')->load(4);
        $emailTempVariables = array();
        $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
        $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
        $adminUsername = 'Admin';
        $emailTempVariables['customername'] = 'Keshab Karmakar';
        $emailTempVariables['content']=' Thank you! We have received your service request and your service ticket number is xxxxxx.    We will reach out to you within 24-48 hours regarding the same. In case you have further queries, you can feel free to reach out to us on 123456.';
        $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        $message = 'Thank you! We have received your service request and your service ticket number is xxxxxx.';
        $mobile = '918553007539';
        $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
        Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());
    }
}
?>