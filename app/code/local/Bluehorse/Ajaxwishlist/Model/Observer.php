<?php

class Bluehorse_Ajaxwishlist_Model_Observer {

    public function __contruct() {
        
    }

    public function shortlisttowishlist(Varien_Event_Observer $observer) {
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customerId, true);
        $product_ids = Mage::getModel('core/cookie')->get('productid');
        if (empty($product_ids))
            return;
        $explodeproductids = explode(',', $product_ids);
        foreach ($explodeproductids as $productid) {
            $product = Mage::getModel('catalog/product')->load($productid);
            $buyRequest = new Varien_Object(array());
            $result = $wishlist->addNewItem($product, $buyRequest);
            $wishlist->save();
        }
        Mage::getModel('core/cookie')->delete('productid');
    }

}
