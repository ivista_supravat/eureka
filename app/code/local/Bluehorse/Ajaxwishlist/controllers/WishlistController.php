<?php
class Bluehorse_Ajaxwishlist_WishlistController extends Mage_Core_Controller_Front_Action
{
    
    public function compareAction(){
        $response = array();
 
        if ($productId = (int) $this->getRequest()->getParam('product')) {
            $product = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($productId);
            if ($product->getId() /* && !$product->isSuper() */ ) {
            $categoryIds = $product->getCategoryIds();
            if (is_array($categoryIds) and count($categoryIds) > 1) {
            $cat = Mage::getModel('catalog/category')->load($categoryIds[0]);
            if(!isset($_SESSION['compare']))
            {
            $_SESSION['compare']=$cat->getId();
            $_SESSION['comparecount']=0;
            }
            if($cat->getId() == $_SESSION['compare'] /* || $cat->getId() == 13 ||$cat->getId() == 18 ||$cat->getId() == 35*/ ) ///come back here
            {
            $_SESSION['comparecount']++;
            Mage::getSingleton('catalog/product_compare_list')->addProduct($product);
            Mage::dispatchEvent('catalog_product_compare_add_product', array('product'=>$product));
            $response['status'] = 'SUCCESS';
            $response['message'] = $this->__('The product %s has been added to comparison list.', Mage::helper('core')->escapeHtml($product->getName()));
            Mage::register('referrer_url', $this->_getRefererUrl());
            Mage::helper('catalog/product_compare')->calculate();
            Mage::dispatchEvent('catalog_product_compare_add_product', array('product'=>$product));
            $this->loadLayout();
            $sidebar_block = $this->getLayout()->getBlock('left.catalog.compare.sidebar');
            $sidebar_block->setTemplate('catalog/product/compare/sidebar.phtml');
            $sidebar = $sidebar_block->toHtml();
            $response['sidebar'] = $sidebar;
            }else
            {
            $response['status'] = 'ERROR';
            $response['message'] = $this->__('The product %s not possible to add in comparison list.', Mage::helper('core')->escapeHtml($product->getName()));
            }
            }
        }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }
    
    public function clearAction()
    {
        $items = Mage::getResourceModel('catalog/product_compare_item_collection');

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $items->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId());
        } elseif ($this->_customerId) {
            $items->setCustomerId($this->_customerId);
        } else {
            $items->setVisitorId(Mage::getSingleton('log/visitor')->getId());
        }

        /** @var $session Mage_Catalog_Model_Session */
        $session = Mage::getSingleton('catalog/session');

        try {
            $items->clear();
            unset($_SESSION['compare']);
            $session->addSuccess();
            Mage::helper('catalog/product_compare')->calculate();
            $response['status'] = 'SUCCESS';
            $response['message'] = $this->__('The comparison list was cleared.');
            $this->loadLayout();
            $sidebar_block = $this->getLayout()->getBlock('left.catalog.compare.sidebar');
            $sidebar_block->setTemplate('catalog/product/compare/sidebar.phtml');
            $sidebar = $sidebar_block->toHtml();
            $response['sidebar'] = $sidebar;
        } catch (Mage_Core_Exception $e) {
            $response['status'] = 'ERROR';
            $response['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['status'] = 'ERROR';
            $response['message'] = $this->__('An error occurred while clearing comparison list.');
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }
    public function removeAction()
    {
        if ($productId = (int) $this->getRequest()->getParam('product')) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);

            if($product->getId()) {
                /** @var $item Mage_Catalog_Model_Product_Compare_Item */
                $item = Mage::getModel('catalog/product_compare_item');
                if(Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $item->addCustomerData(Mage::getSingleton('customer/session')->getCustomer());
                } elseif ($this->_customerId) {
                    $item->addCustomerData(
                        Mage::getModel('customer/customer')->load($this->_customerId)
                    );
                } else {
                    $item->addVisitorId(Mage::getSingleton('log/visitor')->getId());
                }

                $item->loadByProduct($product);

                if($item->getId()) {
                    $item->delete();
                    $_SESSION['comparecount']--;
                    if($_SESSION['comparecount'] == 0) { unset($_SESSION['comparecount']); unset($_SESSION['compare']); }
                    Mage::dispatchEvent('catalog_product_compare_remove_product', array('product'=>$item));
                    Mage::helper('catalog/product_compare')->calculate();
                    $response['status'] = 'SUCCESS';
                    $response['message'] =  $this->__('The product %s has been removed from comparison list.', $product->getName());
                    $this->loadLayout();
                    $sidebar_block = $this->getLayout()->getBlock('left.catalog.compare.sidebar');
                    $sidebar_block->setTemplate('catalog/product/compare/sidebar.phtml');
                    $sidebar = $sidebar_block->toHtml();
                    $response['sidebar'] = $sidebar;
                }
            }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        
    }
    
    public function countdowncontAction() {
       $cartHelperAjax = Mage::helper('checkout/cart');
       $cart = Mage::getModel('checkout/cart')->getQuote()->getData();
       $qtyCart = (int)$cart['items_qty'];
       if($qtyCart > 0){
           $updateAt = $cartHelperAjax->getQuote()->getUpdatedAt(); // cart update date
           $currentDate = strtotime($updateAt);
           $futureDate = $currentDate+(60*1);//target date
           $date1 = date("Y-m-d H:i:s"); 
           $date1 = strtotime($date1); // current date

           //$futureDate = date();

           $dateFormat = "d F Y -- g:i a";
           $targetDate = $futureDate;//Change the 25 to however many minutes you want to countdown
           $actualDate = $date1;
           $secondsDiff = $targetDate - $actualDate;
           $remainingDay     = floor($secondsDiff/60/60/24);
           $remainingHour    = floor(($secondsDiff-($remainingDay*60*60*24))/60/60);
           $remainingMinutes = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))/60);
           $remainingSeconds = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))-($remainingMinutes*60));
           $actualDateDisplay = date($dateFormat,$actualDate);
           $targetDateDisplay = date($dateFormat,$targetDate);

        echo   $total_remainTime =  $remainingDay ."/".$remainingHour."/".$remainingMinutes."/".$remainingSeconds;
       }         
   }
   public function shortlistAction(){
       $productid = $this->getRequest()->getParam('id');
       $isajax =  $this->getRequest()->getParam('isAjax');
       $product_id = Mage::getModel('core/cookie')->get('productid'); 
       
       if($isajax==1){
            if(Mage::getModel('core/cookie')->get('productid')!=''){
                $value = $product_id.','.$productid;
            } else {
                $value = $productid;
            }   
                Mage::getModel('core/cookie')->set('productid', $value, time()+86400);
                $response['status'] = 'SUCCESS';
                $product_ids = Mage::getModel('core/cookie')->get('productid');
                $explodeproductids = explode(',', $value);
                $sidebar .= '<div class="block block-list block-compare"><div class="block-title"><strong><span> '.$this->__('Shortlist Product').' </span></strong></div>
                            <div class="block-content"><ol id="compare-items">';
                foreach ($explodeproductids as $productid){
                            $product = Mage::getModel('catalog/product')->load($productid);
                            $full_path_url = Mage::helper('catalog/image')->init($product, 'thumbnail');
                            $product_url = $product->getProductUrl();
                            $product_name = $product->getName();
                            $product_price = Mage::helper('core')->currency($product->getFinalPrice(),true,false);        
                
                            $sidebar .=  '<li class="item">
                                        <input type="hidden" class="compare-item-id" value="'.$productid.'" />
                                         <p class="product-name"><a href="'.$product_url.'"><img src="'.$full_path_url.'" style="float: left;">'.$product_name.'</a></p>   
                                        <p>'.$product_price.'</p>
                                        <a href="javascript:void(0)" onclick="deleteShortlistItem('.$productid.')">delete</a>    
                                    ';

                }            
                $sidebar .= '</ol><a href="javascript:void(0);" title="'. Mage::helper('core')->quoteEscape($this->__('Remove This Item')).'" onclick="removeAllShortlistProduct()">'.$this->__('Remove All').'</a>
                        </div></div>';
                $response['sidebar'] = $sidebar;
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
                return;
       } else {
           $response['status'] = 'ERROR';
           $response['message'] = 'you make a direct hit on url';
           $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
       }
   }
   public function deletecookieAction(){
       Mage::getModel('core/cookie')->delete('productid');
       $response['status'] = 'SUCCESS';
       $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
   }
   public function deletesingleitemAction(){
       Mage::getModel('core/cookie')->delete('productid');
       Mage::getModel('core/cookie')->set('productid', $this->getRequest()->getParam('product_ids'), time()+86400);
       $response['status'] = 'SUCCESS';
       $response['value'] = $this->getRequest()->getParam('product_ids');
       $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
   }
   public function indexAction()
    {
        if (!$this->_getWishlist()) {
            return $this->norouteAction();
        }
        $this->loadLayout();
        $session = Mage::getSingleton('customer/session');
        $block   = $this->getLayout()->getBlock('customer.wishlist');
        $referer = $session->getAddActionReferer(true);
        if ($block) {
            $block->setRefererUrl($this->_getRefererUrl());
            if ($referer) {
                $block->setRefererUrl($referer);
            }
        }
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('wishlist/session');
        $this->renderLayout();
    }
   protected function _getWishlist()
    {
        $wishlist = Mage::registry('wishlist');
        if ($wishlist) {
            return $wishlist;
        }
        try {
            $wishlist = Mage::getModel('wishlist/wishlist')
            ->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);
            Mage::register('wishlist', $wishlist);
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('wishlist/session')->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::getSingleton('wishlist/session')->addException($e,
            Mage::helper('wishlist')->__('Cannot create wishlist.')
            );
            return false;
        }
        return $wishlist;
    }
    public function addAction()
    {
        $response = array();
        if (!Mage::getStoreConfigFlag('wishlist/general/active')) {
            $response['status'] = 'ERROR';
            $response['message'] = $this->__('Wishlist Has Been Disabled By Admin');
        }
        if(!Mage::getSingleton('customer/session')->isLoggedIn()){
            $response['status'] = 'ERROR';
            $response['message'] = $this->__('Please Login First');
        }
        if(empty($response)){
            $session = Mage::getSingleton('customer/session');
            $wishlist = $this->_getWishlist();
            if (!$wishlist) {
                $response['status'] = 'ERROR';
                $response['message'] = $this->__('Unable to Create Wishlist');
            }else{
                $productId = (int) $this->getRequest()->getParam('product');
                if (!$productId) {
                    $response['status'] = 'ERROR';
                    $response['message'] = $this->__('Product Not Found');
                }else{ 
                    $product = Mage::getModel('catalog/product')->load($productId);
                    if (!$product->getId() || !$product->isVisibleInCatalog()) {
                        $response['status'] = 'ERROR';
                        $response['message'] = $this->__('Cannot specify product.');
                    }else{
                        try {
                            //$requestParams = $this->getRequest()->getParams();
                            //$buyRequest = new Varien_Object($requestParams);
                            $buyRequest = new Varien_Object(array());
                            $result = $wishlist->addNewItem($product, $buyRequest);
                            if (is_string($result)) {
                                Mage::throwException($result);
                            }
                            $wishlist->save();
                            Mage::dispatchEvent(
                                'wishlist_add_product',
                            array(
                                'wishlist'  => $wishlist,
                                'product'   => $product,
                                'item'      => $result
                            )
                            );
                            Mage::helper('wishlist')->calculate();
                            $message = $this->__('%1$s has been added to your wishlist.', $product->getName(), $referer);
                            $response['status'] = 'SUCCESS';
                            $response['message'] = $message;
                            Mage::unregister('wishlist');
                            $this->loadLayout();
                            $sidebar_block = $this->getLayout()->getBlock('wishlist_sidebar');
                            $sidebar = $sidebar_block->toHtml();
                            $response['sidebar'] = $sidebar;
                        }
                        catch (Mage_Core_Exception $e) {
                            $response['status'] = 'ERROR';
                            $response['message'] = $this->__('An error occurred while adding item to wishlist: %s', $e->getMessage());
                        }
                        catch (Exception $e) {
                            mage::log($e->getMessage());
                            $response['status'] = 'ERROR';
                            $response['message'] = $this->__('An error occurred while adding item to wishlist.');
                        }
                    }
                }
            }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }
    public function removeallwishlistAction(){
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $itemCollection = Mage::getModel('wishlist/item')->getCollection()->addCustomerIdFilter($customerId);
        foreach($itemCollection as $item) {
            $item->delete();
        }
        $this->_redirectReferer();    
   }
   
}
?>