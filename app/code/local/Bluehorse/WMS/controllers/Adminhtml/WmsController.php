<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

class Bluehorse_WMS_Adminhtml_WmsController extends Mage_Adminhtml_Controller_Action {

    /**
     * Initialize layout.
     *
     * @return Bluehorse_Warehouse_Adminhtml_ExportwarehouseController
     */
    protected function _initAction() {
        $this->_title($this->__('Retails Store Management'))
                ->loadLayout()
                ->_setActiveMenu('wms/warehouse');

        return $this;
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('wms/items');
    }

    public function indexAction() {
        $this->loadLayout();
        $myblock = $this->getLayout()->createBlock('wms/adminhtml_wms');
        $this->_addContent($myblock);
        $this->renderLayout();
    }

    public function newAction() {

        $this->_title($this->__("Warehouse"));
        $this->_title($this->__("Warehouse"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("wms/zipcode")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("wms_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("wms/zipcode");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Warehouse Manager"), Mage::helper("adminhtml")->__("Warehouse Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Warehouse Description"), Mage::helper("adminhtml")->__("Warehouse Description"));


        $this->_addContent($this->getLayout()->createBlock("wms/adminhtml_wms_edit"))
                ->_addLeft($this->getLayout()->createBlock("wms/adminhtml_wms_edit_tabs"));

        $this->renderLayout();
    }

    public function saveAction() {

        $post_data = $this->getRequest()->getPost();

        
        $post_data['state'] = (string) $post_data['state'];
        if ($post_data) {

            try {
                if (isset($data['links'])) {
                    $zipcodes = Mage::helper('adminhtml/js')->decodeGridSerializedInput($data['links']['zipcodes']);
                }
                $model = Mage::getModel("wms/zipcode")
                        ->addData($post_data)
                        ->setId($this->getRequest()->getParam("id"))
                        ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Warehouse was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setWarehouseData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setWarehouseData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    public function editAction() {
        $this->_title($this->__("Warehouse"));
        $this->_title($this->__("Warehouse"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("wms/zipcode")->load($id);
        if ($model->getId()) {
            Mage::register("wms_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("wms/wms");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Warehouse Manager"), Mage::helper("adminhtml")->__("Warehouse Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Warehouse Description"), Mage::helper("adminhtml")->__("Warehouse Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("wms/adminhtml_wms_edit"))->_addLeft($this->getLayout()->createBlock("wms/adminhtml_wms_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("wms")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("warehouse/warehouse");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction() {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("warehouse/warehouse");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /* pincpde grid */

    public function pincodesAction() {


        $this->loadLayout();
        $this->getLayout()->getBlock('warehouse.edit.tab.pincodes')
                ->setWareId($this->getRequest()->getPost('id', null));
        $this->renderLayout();
    }

    public function pincodesGridAction() {

        $this->loadLayout();
        $this->getLayout()->getBlock('warehouse.edit.tab.pincodes')
                ->setWareId($this->getRequest()->getPost('id', null));
        $this->renderLayout();
    }

    /* Product  Grid */

    public function productAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('wms.edit.tab.product')
                ->setZipcodeId($this->getRequest()->getPost('id', null));
        $this->renderLayout();
    }

    public function productGridAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('wms.edit.tab.product')
                ->setZipcodeId($this->getRequest()->getPost('id', null));
        $this->renderLayout();
    }

    public function exportCsvAction() {

        $rows = [];

        $collection = Mage::getModel('wms/zipcode')->getCollection();
        $j = 1;
        $rows[0][0] = 'pincode';
        $rows[0][1] = 'Service availability';
        $rows[0][2] = 'cod';
        $rows[0][3] = 'is_primary';
        $rows[0][4] = 'statename';
        $rows[0][5] = 'region';
		$rows[0][6] = 'vertical';
		
        $rows[0][7] = 'sku';
        $rows[0][8] = 'direct_qty';
        $rows[0][9] = 'consumer_qty';
        
        
        foreach ($collection as $items) {

            $inventory = Mage::getModel('wms/inventory')->getCollection()
                    ->addFieldToFilter('zipcode_id', $items->getId());
            foreach ($inventory as $inventory_item) {
                $inv = $inventory_item->getData();


                $rows[$j][] = $items->getData('zipcode');
                $rows[$j][] = (0 == $items->getData('service_availability'))? "Y" : "N";

                $rows[$j][] = (0 == $items->getData('cod')) ? "Y" : "N";
                $rows[$j][] = (0 == $items->getData('is_primary')) ? "Y" : "N";
                $rows[$j][] = $this->getStateNameById($items->getData('state'));
                $rows[$j][] = $this->getRegionNameById($items->getData('region'));
                $rows[$j][] = $items->getData('vertical');
                
                $rows[$j][] = $inventory_item->getData('product_sku');
                $rows[$j][] = $inventory_item->getData('direct_qty');
                $rows[$j][] = $inventory_item->getData('consumer_qty');
                
                $j++;
            }
            $i++;
        }

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=warehouse.csv");
        header("Pragma: no-cache");
        header("Expires: 0");


        $this->outputCSV($rows);
        //die;
        //$content = $this->_generateCsv($rows);
        //var_dump($str);
        //$content = $this->getLayout()->createBlock('warehouse/adminhtml_warehouse_grid')->getCsv();
        //var_dump($content);
        //die;
        //$this->_sendUploadResponse($fileName, $content);
    }

    function outputCSV($data) {
        $outputBuffer = fopen("php://output", 'w');
        foreach ($data as $val) {
            fputcsv($outputBuffer, $val);
        }
        fclose($outputBuffer);
    }

    function _generateCsv($data, $delimiter = ',', $enclosure = '"') {
        $handle = fopen('php://temp', 'r+');
        foreach ($data as $line) {
            fputcsv($handle, $line, $delimiter, $enclosure);
        }
        rewind($handle);
        while (!feof($handle)) {
            $contents .= fread($handle, 8192);
        }
        fclose($handle);
        return $contents;
    }

    public function exportExcelAction() {

        $fileName = 'warehouse-' . date('d-m-Y-H-i-s');
        $rows = [];
        $warehouseItems = Mage::getModel('warehouse/warehouse')->getCollection();

        foreach ($warehouseItems as $warehouseItem) {
            //$rows[$i]['warehouse_id'] = $warehouseItem->getId();
            $zipcodes = Mage::getModel('warehouse/zipcode')->getCollection()
                    ->addFieldToFilter('wid', $warehouseItem->getId());
            foreach ($zipcodes as $item) {
                $zip = $item->getData();
                //$rows[$i]['zipcode'] = $zip['zipcode'];

                $products = Mage::getModel('warehouse/product')->getCollection()
                        ->addFieldToFilter('zipcode_id', $zip['id']);
                $j = 1;
                $rows[0][0] = 'warehouse_id';
                $rows[0][1] = 'associate_zipcode';
                $rows[0][2] = 'associate_product';
                $rows[0][3] = 'associate_product_qty';

                foreach ($products as $product) {
                    $pro = $product->getData();
                    $rows[$j][] = $warehouseItem->getId();
                    $rows[$j][] = $zip['zipcode'];
                    $rows[$j][] = $pro['product_sku'];
                    $rows[$j][] = $pro['product_qty'];
                    $j++;
                }
            }
            $i++;
        }

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}.xls");
        header("Pragma: no-cache");
        header("Expires: 0");


        $this->outputCSV($rows);
        //$fileName = 'warehouse-' . date('d-m-Y-H-i-s') . '.xls';
        // $content = $this->getLayout()->createBlock('warehouse/adminhtml_warehouse_grid')->getExcelFile();
        //$this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export csv and xml
     *
     */
    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

    public function getStateNameById($stateId) {

        $region = Mage::getModel('directory/region')->loadByCode($stateId, 'IN');
        
        return $region->getName();
    }
    
    public function getRegionNameById($regionID) {
		
		$regionArray = Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getRegions();
		
		
		return $regionArray[$regionID];
		
	}

}
