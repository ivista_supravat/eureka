<?php

class Bluehorse_WMS_Model_Mysql4_Zipcode extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init("wms/zipcode", "id");
    }

    public function uploadAndImport($object) {

        $this->_behavior = $object->getRequest()->getPost('behavior');
        $importFile = $_FILES['import_file'];
        if (empty($importFile['tmp_name'])) {
            return;
        }
        $csvFile = $importFile['tmp_name'];

        $this->_importErrors = array();
        $this->_importedRows = 0;



        $io = new Varien_Io_File();
        $info = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        // check and skip headers
        $headers = $io->streamReadCsv();
        if ($headers === false || count($headers) < 2) {
            $io->streamClose();
            Mage::throwException(Mage::helper('wms')->__('Invalid Stores File Format'));
        }
        $adapter = $this->_getWriteAdapter();
        $adapter->beginTransaction();
        try {
            $rowNumber = 1;
            $importData = array();
            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;

                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->_getImportRow($csvLine, $rowNumber);

                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = array();
                }
            }
            if ($this->_importErrors) {
                $error = Mage::helper('wms')->__('File has not been imported. See the following list of errors: <br>%s', implode(" <br>", $this->_importErrors));
                Mage::throwException($error);
            }

            if (empty($this->_importErrors)) {
                $this->_saveImportData($importData);
            }

            $io->streamClose();
        } catch (Mage_Core_Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            Mage::throwException(Mage::helper('wms')->__('An error occurred while import stores csv.'));
        }
        $adapter->commit();

        //add success message
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wms')->__($this->_importedRows . ' - Rows imported successfully.'));
        return $this;
    }

    protected function _getImportRow($row, $rowNumber = 0) {
        // validate row

        if (count($row) < 8) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid stores format in the Row #%s', $rowNumber);
            return false;
        }

        // strip whitespace from the beginning and end of each row
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }


        // validate store name
        if (empty($row[0])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid pincode "%s" in the Row #%s.', $row[0], $rowNumber);
            return false;
        }

        if (empty($row[1])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid Service availability  value "%s" in the Row #%s.', $row[1], $rowNumber);
            return false;
        }

        // validate Zipcode
        if (empty($row[2])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid cod  "%s" in the Row #%s.', $row[2], $rowNumber);
            return false;
        }

        if (empty($row[3])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid is primary "%s" in the Row #%s.', $row[3], $rowNumber);
            return false;
        }

        if (empty($row[4])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid State name  "%s" in the Row #%s.', $row[4], $rowNumber);
            return false;
        }

        if (empty($row[5])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid Region name  "%s" in the Row #%s.', $row[5], $rowNumber);
            return false;
        }

        if (empty($row[6])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid vertical "%s" in the Row #%s.', $row[6], $rowNumber);
            return false;
        }

        if (empty($row[7])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid sku "%s" in the Row #%s.', $row[7], $rowNumber);
            return false;
        }

        // validate product qty
        if (empty($row[8])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid direct qty "%s" in the Row #%s.', $row[8], $rowNumber);
            return false;
        }

        if (empty($row[9])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid Consuler qty "%s" in the Row #%s.', $row[9], $rowNumber);
            return false;
        }

        if (empty($row[10])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid Delivery time "%s" in the Row #%s.', $row[10], $rowNumber);
            return false;
        }

        $storeRow = array(
            'zipcode' => $row[0],
            'service_availability' => $row[1],
            'cod' => $row[2],
            'is_primary' => $row[3],
            'statename' => $row[4],
            'region' => $row[5],
            'vertical' => $row[6],
            'sku' => $row[7],
            'direct_qty' => $row[8],
            'consumer_qty' => $row[9],
            'delivery_time' => $row[10],
            'status' => 0,
            'position' => 1,
            'created_at' => Mage::getModel('core/date')->timestamp(time())
        );
        return $storeRow;
    }

    protected function _saveImportData(array $stores) {

        if (!empty($stores)) {
            $transactionSave = Mage::getModel('core/resource_transaction');
            foreach ($stores as $store) {
                //var_dump($store); die;


                $zipcodeId = '';
                $productId = '';
                $store['service_availability'] = ("Y" == $store['service_availability']) ? 0 : 1;
                $store['cod'] = ("Y" == $store['cod']) ? 0 : 1;
                $store['is_primary'] = ("Y" == $store['is_primary']) ? 0 : 1;
                $store['is_primary'] = ("Y" == $store['is_primary']) ? 0 : 1;

                $zipModel = Mage::getModel('wms/zipcode');
                $inventory = Mage::getModel('wms/inventory');

                if (!empty($store['zipcode'])) {
                    $zipModel->load($store['zipcode'], 'zipcode');
                    if ($zipModel->getId()) {
                        $zipcodeId = $zipModel->getId();
                    }
                }

                $satename = Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getState();
                $staeArray = array_map('strtolower', $satename);
                $arrayKey = array_search(strtolower($store['statename']), $staeArray);

                $store['state'] = $arrayKey;

                $regions = Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getRegions();
                $yourArray = array_map('strtolower', $regions);

                $key = array_search(strtolower($store['region']), $yourArray);

                $store['region'] = $key;

                $zipModel->setData($store);

                if (!empty($zipcodeId)) {
                    $zipModel->setId($zipcodeId);
                }
                $zipModel->save();

                /*
                 * Save product page
                 */

                if (!empty($store['sku'])) {
                    $inventory->load($store['sku'], 'product_sku');
                    if ($inventory->getId()) {
                        $productId = $inventory->getId();
                    }
                }


                $prdData['zipcode_id'] = (int) $zipModel->getId();
                $prdData['region'] = (int) $store['region'];
                $prdData['product_id'] = 0;
                $prdData['product_sku'] = $store['sku'];
                $prdData['direct_qty'] = (int) $store['direct_qty'];
                $prdData['consumer_qty'] = (int) $store['consumer_qty'];
                $prdData['status'] = 0;
                $prdData['position'] = 0;

                $inventory->setData($prdData);

                if (!empty($productId)) {
                    $inventory->setId($productId);
                }
                $inventory->save();
                //check for overwrite existing store(s) 
                if ($this->_behavior == 1 || empty($inventory)) {
                    //$transactionSave->addObject($prodModel);
                }
                unset($inventory);
                unset($zipModel);
            }
            $this->_importedRows += count($stores);
        }
        return $this;
    }

}
