<?php

class Bluehorse_WMS_Block_Adminhtml_Wms_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('wms_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('wms')->__('Warehouse '));
    }

    protected function _beforeToHtml() {
        $this->addTab('wms', array(
            'label' => Mage::helper('wms')->__('Warehouse Information'),
            'title' => Mage::helper('wms')->__('Warehouse Information'),
            'content' => $this->getLayout()->createBlock('wms/adminhtml_wms_edit_tab_form')->toHtml(),
        ));
        $this->addTab('product', array(
            'label' => Mage::helper('wms')->__('Associated Product'),
            'title' => Mage::helper('wms')->__('Associated Product'),
            'url' => $this->getUrl('*/*/Product', array('_current' => true)),
            'class' => 'ajax',
        ));


        return parent::_beforeToHtml();
    }

}
