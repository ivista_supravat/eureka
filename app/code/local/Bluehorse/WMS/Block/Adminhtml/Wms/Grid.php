<?php

class Bluehorse_WMS_Block_Adminhtml_Wms_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('wmsGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
//load collection 
        $collection = Mage::getModel('wms/zipcode')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('id', array(
            'header' => Mage::helper('wms')->__('ID #'),
            'align' => 'left',
            'index' => 'id',
        ));

        $this->addColumn('state', array(
            'header' => Mage::helper('wms')->__('State Name'),
            'align' => 'left',
            'index' => 'state',
            'type' => 'options',
            'options' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getState()
        ));

        $this->addColumn('region', array(
            'header' => Mage::helper('wms')->__('Region Name'),
            'align' => 'left',
            'index' => 'region',
            'type' => 'options',
            'options' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getRegions()
        ));

        $this->addColumn('zipcode', array(
            'header' => Mage::helper('wms')->__('Zipcode'),
            'align' => 'left',
            'index' => 'zipcode',
        ));


        $this->addColumn('vertical', array(
            'header' => Mage::helper('wms')->__('Vertical'),
            'align' => 'left',
            'index' => 'vertical',
            'type' => 'options',
            'options' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getVertical()
        ));

        $this->addColumn('delivery_time', array(
            'header' => Mage::helper('wms')->__('Delivery Day'),
            'align' => 'left',
            'index' => 'delivery_time',
        ));
        $this->addColumn('is_primary', array(
            'header' => Mage::helper('wms')->__('Is Primary'),
            'align' => 'left',
            'index' => 'is_primary',
            'type' => 'options',
            'options' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getIsPrimaryOptionsArray()
        ));
        $this->addColumn('service_availability', array(
            'header' => Mage::helper('wms')->__('Service Availability'),
            'align' => 'left',
            'index' => 'service_availability',
            'type' => 'options',
            'options' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getIsPrimaryOptionsArray()
        ));
        $this->addColumn('cod', array(
            'header' => Mage::helper('wms')->__('COD'),
            'align' => 'left',
            'index' => 'cod',
            'type' => 'options',
            'options' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getIsPrimaryOptionsArray()
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('wms')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getOptionArray15(),
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('wms')->__('Sample CSV'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_warehouse', array(
            'label' => Mage::helper('wms')->__('Remove'),
            'url' => $this->getUrl('*/wms/massRemove'),
            'confirm' => Mage::helper('wms')->__('Are you sure?')
        ));
        return $this;
    }

    static public function getOptionArray15() {
        $data_array = array();
        $data_array[0] = 'Enable';
        $data_array[1] = 'Disasble';
        return($data_array);
    }

    static public function getValueArray15() {
        $data_array = array();
        foreach (Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getOptionArray15() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return($data_array);
    }

    static public function getIsPrimaryOptionsArray() {
        $data_array = array();
        $data_array[0] = 'Yes';
        $data_array[1] = 'No';
        return($data_array);
    }

    static public function getIsPrimaryOptionsValueArray15() {
        $data_array = array();
        foreach (Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getIsPrimaryOptionsArray() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return($data_array);
    }

    static public function getState() {
        $data_array = array();
        $states = Mage::getModel('directory/country')->load('IN')->getRegions();
        foreach ($states as $state) {
            $data_array[$state->getCode()] = $state->getName();
        }

        return($data_array);
    }

    static public function getStateValue() {
        $data_array = array();
        foreach (Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getState() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return($data_array);
    }

    static public function getRegionsValue() {
        $data_array = array();
        foreach (Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getRegions() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return($data_array);
    }

    static public function getRegions() {
        $regions[1] = "Hyderabad";
        $regions[2] = "Vijaywada";
        $regions[3] = "Patna";
        $regions[4] = "Raipur";
        $regions[5] = "Delhi";
        $regions[6] = "Ahmedabad";
        $regions[7] = "Gurgaon";
        $regions[8] = "Parwanoo";
        $regions[9] = "Jammu";
        $regions[10] = "Ranchi";
        $regions[11] = "Bangalore";
        $regions[12] = "Cochin";
        $regions[13] = "Indore";
        $regions[14] = "Bhiwandi";
        $regions[15] = "Guwahati";
        $regions[16] = "Bhubaneshwar";
        $regions[17] = "Chennai";
        $regions[18] = "Zirakpur (new)";
        $regions[19] = "Jaipur";
        $regions[20] = "Chennai";
        $regions[21] = "DEHRADUN";
        $regions[22] = "Lucknow";
        $regions[23] = "Gaziabad / (Noida)";
        $regions[24] = "Kolkata";
        return($regions);
    }

    static public function getVertical() {
        $arry[1] = "Direct";
        $arry[2] = "Consumer";

        return $arry;
    }

    static public function getVerticalValue() {
        $arry = array();
        foreach (Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getVertical() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return $arry;
    }

}
