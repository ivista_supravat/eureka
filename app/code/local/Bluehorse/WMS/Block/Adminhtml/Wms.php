<?php

class Bluehorse_WMS_Block_Adminhtml_Wms extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {

        $this->_controller = 'adminhtml_wms';
        $this->_blockGroup = 'wms';
        $this->_headerText = Mage::helper('wms')->__('Warehouse Manager');
        $this->_addButtonLabel = Mage::helper('wms')->__('Add Warehouse');
        parent::__construct();
    }

}
