<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->modifyColumn($installer->getTable('wms/zipcode'), 'state', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => false,
    'length' => 255,
    'after' => 'id',
    'comment' => 'State Code'
));
$installer->endSetup();
?>
