<?php

$installer = $this;
$installer->startSetup();


$installer->getConnection()
	->addColumn($installer->getTable('wms/zipcode'), 'state', array(
		'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
		'nullable' => false,
		'length' => null,
		'after' => 'id',
		'comment' => 'State ID'
	));
$installer->endSetup();
?>
