<?php

$installer = $this;
$installer->startSetup();


$installer->getConnection()
	->addColumn($installer->getTable('wms/zipcode'), 'vertical', array(
		'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable' => false,
		'length' => 255,
		'after' => 'zipcode',
            'comment' => 'vertical type'
	));
	$installer->getConnection()->dropColumn($installer->getTable('wms/inventory'), 'product_id', null);
	$installer->getConnection()->dropColumn($installer->getTable('wms/inventory'), 'product_qty', null);
	$installer->getConnection()->dropColumn($installer->getTable('wms/inventory'), 'product_price', null);
	$installer->getConnection()->dropColumn($installer->getTable('wms/inventory'), 'vertical', null);
$installer->endSetup();
?>
