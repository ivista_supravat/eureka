<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
        ->addColumn($installer->getTable('wms/inventory'), 'vertical', array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => false,
            'length' => 255,
            'after' => 'product_id',
            'comment' => 'vertical type'
        ));
$installer->getConnection()
        ->addColumn($installer->getTable('wms/inventory'), 'consumer_qty', array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable' => false,
            'length' => null,
            'after' => 'product_id',
            'comment' => 'Consumer Qty'
        ));
$installer->getConnection()
        ->addColumn($installer->getTable('wms/inventory'), 'direct_qty', array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable' => false,
            'length' => null,
            'after' => 'product_id',
            'comment' => 'Direct Qty'
        ));
$installer->endSetup();
?>
