<?php

$installer = $this;
$installer->startSetup();

/**
 * Sql script for pincode table
 */
$table = $installer->getConnection()
        ->newTable($installer->getTable('wms/zipcode'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('region', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Region Id')
        ->addColumn('zipcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'Zipcode')
        ->addColumn('is_primary', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
            'default' => '0'
                ), 'is_primary')
        ->addColumn('service_availability', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0',
                ), 'service availability')
        ->addColumn('cod', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0',
                ), 'Cash On Delivery')
        ->addColumn('delivery_time', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'delivery_time time in day unit')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0',
                ), 'Status')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0',
                ), 'position')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                ), 'Updated At')
        ->setComment('Zipcpde table');

$installer->getConnection()->createTable($table);

/**
 * Sql script for inventory table
 */
$table = $installer->getConnection()
        ->newTable($installer->getTable('wms/inventory'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('zipcode_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Zipcode Id')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
            'default' => '0'
                ), 'is_primary')
        ->addColumn('product_sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
            'default' => null
                ), 'is_primary')
        ->addColumn('product_qty', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
            'default' => '0'
                ), 'product_qty')
        ->addColumn('product_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
                ), 'Product Price Amount')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0',
                ), 'Status')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0',
                ), 'position')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                ), 'Updated At')
        ->addIndex($installer->getIdxName('wms/zipcode', array('zipcode_id')), array('zipcode_id'))
        ->addForeignKey(
                $installer->getFkName(
                        $installer->getTable('wms_inventory'), 'zipcode_id', $installer->getTable('wms_pincode'), 'id'
                ), 'zipcode_id', $installer->getTable('wms_pincode'), 'id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->setComment('Product Inventory Table');
;
$installer->getConnection()->createTable($table);
$installer->endSetup();
?>
