<?php
/* @copyright   Copyright (c) 2015 BlueHorse */ 
class Bluehorse_CancelOrder_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      $session = Mage::getSingleton('customer/session');
	  if ($session->isLoggedIn()) {
          $this->loadLayout();
          $myBlock = $this->getLayout()->createBlock('cancelorder/index');
          $myBlock->setTemplate('cancelorder/index.phtml');
          $myHtml =  $myBlock->toHtml();    
          $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody($myHtml);
            return;
	  }else{
		Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account'));
	  }
    }
	
	public function postAction()
    {
	  $session = Mage::getSingleton('customer/session');
	  if ($session->isLoggedIn()) {
		if ( $this->getRequest()->getPost() ) {
            try {
                $postData = $this->getRequest()->getPost();
				$createdate=now();
                 $Model = Mage::getModel('cancelorder/cancel');
                 $Model->setOrderId($postData['order_id'])
                    ->setName($postData['name'])
					->setEmail($postData['email'])
					->setPhone($postData['phone'])
					->setReason($postData['reason'])
					->setStatus(0)
					->setRequestDate($createdate)
                    ->save();
				$order_id = $postData['order_id']; //use your own order id 
			 	$order = Mage::getModel("sales/order")->loadByIncrementId($order_id); //load order by order id 
			 	$order->setStatus("cancel_requested");
			 	$order->save();
					//echo '<pre>'; print_r($order);exit;
                /* Send mail to Support */
                $useremail = $postData['email']; 
                $Username = $postData['name'];
                $emailTemp = Mage::getModel('core/email_template')->load(29);
                $emailTempVariables = array();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $postData['order_id'];
                $emailTempVariables['reason'] =  $postData['reason'];
                $emailTempVariables['phoneno'] = $postData['phone'];
                $emailTempVariables['emailid'] = $postData['email'];
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
                $emailTemp->setSenderName($Username);
                $emailTemp->setSenderEmail($useremail);
				
                $emailTemp->send($adminEmail,$adminUsername,$emailTempVariables);
                /* Send mail to Support */

                /* Send mail coy to Customer */
                
                $Username = $postData['name'];
                $emailTemp = Mage::getModel('core/email_template')->load(30);
                $emailTempVariables1 = array();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $adminUsername = 'Admin';
				$emailTempVariables1['content']='Product cancel request came from your side, wait for another confirmation call from our side.';
                $emailTempVariables1['customername'] = $postData['name'];
                $emailTempVariables1['orderno'] =  $postData['order_id'];
                $emailTempVariables1['reason'] = $postData['reason'];
				
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables1);
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
				
                $emailTemp->send($useremail,$Username,$emailTempVariables1);
				//echo '<pre>';print_r($emailTemp);exit;
                /* Send mail coy to Customer */
				/*send mail to vendor*/
				foreach ($order->getItemsCollection() as $item){
					$productid=$item->getProductId();
					$collection_product = Mage::getModel('marketplace/product')->getCollection();
					$collection_product->addFieldToFilter('mageproductid',array('eq'=>$item->getProductId()));
					foreach($collection_product as $selid){
						$seller_id=$selid->getuserid();
					}
					if($seller_id){
					$customer = Mage::getModel('customer/customer')->load($seller_id);
					}
					$customername= $customer->getName();
					$customermail=$customer->getEmail();
				//echo '<pre>'; print_r($customername);exit;
                $emailTemp = Mage::getModel('core/email_template')->load(30);
                $emailTempVariables = array();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $adminUsername = 'Admin';
                $emailTempVariables1['customername'] = $customername;
				$emailTempVariables1['content']='I would like to inform you that your product, which is been sold from our store is request for cancel';
                $emailTempVariables1['orderno'] =  $postData['order_id'];
                $emailTempVariables1['reason'] = $postData['reason'];
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
                $emailTemp->send($customermail,$customername,$emailTempVariables);
				}
				/*send mail to vendor*/
				
				
				
                Mage::getSingleton('core/session')->addSuccess('Order Cancellation Request has been successfully send');
		$this->_redirect('sales/order/history/');
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage());
                $this->_redirect('sales/order/history/');
            }
        }
        $this->_redirect('sales/order/history/');
	  }else{
		Mage::app()->getFrontController()->getResponse()->setRedirect('customer/account');
	  } 

    }

}