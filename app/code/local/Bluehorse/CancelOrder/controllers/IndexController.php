<?php
/* @copyright   Copyright (c) 2015 BlueHorse */ 
class Bluehorse_CancelOrder_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      $session = Mage::getSingleton('customer/session');
	  if ($session->isLoggedIn()) {
          $this->loadLayout();
          $myBlock = $this->getLayout()->createBlock('cancelorder/index');
          $myBlock->setTemplate('cancelorder/index.phtml');
          $myHtml =  $myBlock->toHtml();    
          $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody($myHtml);
            return;
	  }else{ echo 2;return;
		Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account'));
	  }
    }
	
	public function postAction()
    {
	  $session = Mage::getSingleton('customer/session');
	  if ($session->isLoggedIn()) {
		if ( $this->getRequest()->getPost() ) {
            try {
                $postData = $this->getRequest()->getPost();
		$createdate=now();
                 $Model = Mage::getModel('cancelorder/cancel');
                 $Model->setOrderId($postData['order_id'])
                    ->setName($postData['name'])
					->setEmail($postData['email'])
					->setPhone($postData['phone'])
					->setReason($postData['reason'])
					->setStatus(0)
					->setRequestDate($createdate)
                                        ->save();
                $order_id = $postData['order_id']; //use your own order id 
                $order = Mage::getModel("sales/order")->loadByIncrementId($order_id); //load order by order id 
                $order->setStatus("cancel_request");
                $history = $order->addStatusHistoryComment('Order has been canceled by '.$postData['name'].'', false);
                $history->setIsCustomerNotified(true);
		$order->save();
                $orderItems = $order->getItemsCollection();
                foreach ($orderItems as $item){
                    $_product = Mage::getModel('catalog/product');
                    $_product ->setStoreId($item->getOrder()->getStoreId());
                    $imageurl = (string)Mage::helper('catalog/image')->init($_product, 'image');
                    $product_price = intval($item->getPrice());
                    $product_name = $item->getName();
                    $itemqty = intval($item->getQtyOrdered());
                    $html1 = '<tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#CCC solid thin;"><tbody><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tbody><tr><td><img src="'.$imageurl.'" alt="" width="49" height="75"></td><td class="aur-text" style="color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></tbody></table></td><td style=" border-left:1px solid #ddd;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td class="aur-text" style=" border-left:1px solid #ddd;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html = '<table class="body-top" border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:15px 0px 0px 5px;"><tbody><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tbody><tr><td style="width:60%; font-family: \'Open Sans\', sans-serif; font-size:13px;  color:#757575; font-weight:50%;">PRODUCT</td><td style="width:15%; padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px;  text-align:center; color:#757575; font-weight:600;">QTY</td><td style="width:20%; padding: 8px;; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#757575; font-weight:600;">PRICE</td></tr></tbody></table></td></tr></tbody></table><table class="body-top" border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;"><tbody>'.$html1.' </tbody></table></td></tr></tbody></table><table class="body-top" border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;"><tbody><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;"><tbody><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By<span style=" color:#4f4f4f; font-weight:600"> : Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal :</td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:500; line-height:2px;">Discount:</td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total:</td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></tbody></table></td></tr></tbody></table>';
                
                $useremail = $postData['email']; 
                $Username = $postData['name'];
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(3);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $order->getCreatedAt();
                $emailTempVariables['cancelationapprovedate'] =  time();
                $emailTempVariables['phoneno'] =  $postData['phone'];
                $emailTempVariables['name'] =  $postData['name'];
                $emailTempVariables['reason'] =  $postData['reason'];
                $emailTempVariables['emailid'] = $postData['email'];
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
                
                /* Send mail to Support */
                $emailTemp->setSenderName($Username);
                $emailTemp->setSenderEmail($useremail);
                $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
                /* Send mail to Support */

                /* Send mail coy to Customer */
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
                $emailTemp->send($useremail,$Username,$emailTempVariables);
                /* Send mail coy to Customer */
                
                Mage::getSingleton('core/session')->addSuccess('Order Cancellation Request has been successfully sent.');
		$this->_redirect('sales/order/history/');
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage());
                $this->_redirect('sales/order/history/');
            }
        }
        $this->_redirect('sales/order/history/');
	  }else{
		Mage::app()->getFrontController()->getResponse()->setRedirect('customer/account');
	  } 

    }
	public function acceptAction()
	{
		// echo '<pre>'; print_r($this->getRequest()->getPost());exit;
	  $session = Mage::getSingleton('customer/session');
	  if ($session->isLoggedIn()) {
		if ( $this->getRequest()->getPost() ) {
            try {
               $postData = $this->getRequest()->getPost();
			   // echo '<pre>'; print_r($postData);exit;
			 $order_id = $postData['order_id']; //use your own order id 
			 $order = Mage::getModel("sales/order")->loadByIncrementId($order_id); //load order by order id 
			 
			 $order->setStatus($postData['cancelstatus']);
			 $order->save();

			$col=Mage::getModel('cancelorder/cancel')->getCollection()->addFieldToFilter('order_id',$postData['order_id'])->getFirstItem();
				if($postData['cancelstatus']=='cancel_initiated'){
                $col->setStatus(1);
				} else if($postData['cancelstatus']=='cancel_rejected'){
				 $col->setStatus(2);
				}
				$col->save();
			 	
			 	/* Send mail coy to Customer */
				$admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
     
                $getCustomerId = $order->getCustomerId();
				$customer = Mage::getModel('customer/customer')->load($getCustomerId);
				$customername= $customer->getName();
				$customeremail=$customer->getEmail(); 
				//echo $customername; echo $customeremail; exit;
                $emailTemp = Mage::getModel('core/email_template')->load(31);
                $emailTempVariables = array();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $adminUsername = 'Admin';
                 $emailTempVariables['customername'] = $customername;
                $emailTempVariables['orderno'] =  $postData['order_id'];
				$emailTempVariables['orderstatus']=$postData['cancelstatus'];
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
                $emailTemp->send($customeremail,$customername,$emailTempVariables);
                /* Send mail coy to Customer */				
			 Mage::getSingleton('core/session')->addSuccess('Order Cancel Status successfully Updated.');

			 Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

             return;

            } 

	        catch (Exception $e) {

                Mage::getSingleton('customer/session')->addError($e->getMessage());

                Mage::getSingleton('customer/session')->setReturnData($this->getRequest()->getPost());

                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

                return;

            }

        }

        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

		return;

	  }

	  else{

		Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account'));

	  } 
}
}