<?php

class Bluehorse_Orderstate_Adminhtml_OrderstatuschangeController extends Mage_Adminhtml_Controller_Action {

    public function productdeliveredAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order->getCustomerName();
        $order->setStatus("delivery_complete");
        $shipmentCollection = Mage::getResourceModel('sales/order_shipment_collection')
            ->setOrderFilter($order)
        ->load();
        foreach ($shipmentCollection as $shipment){
            // This will give me the shipment IncrementId, but not the actual tracking information.
            foreach($shipment->getAllTracks() as $tracknum)
            {
                $tracknums[]=$tracknum->getNumber();
                $trackname[]=$tracknum->getTitle();
            }

        }
        $order_id = $order['increment_id'];
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width:50%;"><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75"/></td><td style="color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;font-size: 12px">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600; text-align:center;font-size: 12px">RS. '.$product_price.'/</td></tr></table></td></tr></table>' ;
        }   
        $ordersubtotal = intval($order->getSubtotal());
        $discount = intval($order->getDiscountAmount());
        $grandtotal = intval($order->getGrandTotal());   
        $html .= '<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width:50%;"><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:20px;"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; font-weight:100; color: #868686;">Free Gift : <span style=" color:#4f4f4f; font-weight:600">Gift Voucher</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:100; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:100; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; font-weight:100; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">'.$paymentmode.'</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:100; line-height:2px;">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:100; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td  style=" width:100px;font-size:8px;font-family: \'Open Sans\', sans-serif; color:#757575; font-weight:500;width:60%"><a href="'.$carturl.'" ><input type="button" value="TRACK / CANCEL ORDER" style="width:118px;font-size:8px;padding:10px; border-radius:4px;  color:#2762a8; font-weight:bold; margin-top:10px; background:#fff601;-webkit-box-shadow: 0px 3px 1px 0px #ded800; -moz-box-shadow:    0px 3px 1px 0px #ded800;box-shadow:0px 3px 1px 0px #ded800; border:#ded800"/></a></td><td style=" padding:8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $shippingaddress = '<span style=" color:#4f4f4f; font-weight:bold; line-height:20px;">'.$order->getCustomerFirstname().' '.$order->getCustomerLastname().'<br>'.$streetaddress[0].',<br> '.$order->getShippingAddress()->getCity().'<br>'.$order->getShippingAddress()->getRegion().' - '.$order->getShippingAddress()->getPostcode().'</span>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                //$zipcode = Mage::getModel('wms/zipcode')->load($this->getShippingAddress()->getPostcode(),'zipcode');
                //$i = $zipcode->getDeliveryTime();
                $deliverydate = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false). ' + 7 day'));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
        $emailTemp = Mage::getModel('core/email_template')->load(13);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['delivery_date'] =  $deliverydate;
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $emailTempVariables['shippingaddress'] = $shippingaddress;
                $emailTempVariables['customerid'] = $order->getCustomerId();
                $emailTempVariables['trackno'] = $tracknums[0];
                $emailTempVariables['carriername'] = $trackname[0];
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('This product has been delivered successfully', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productreadytoshipAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order->getCustomerName();
        $order->setStatus("redytoship");
        $order_id = $order['increment_id'];
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width:50%;"><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75"/></td><td style="color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;font-size: 12px">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600; text-align:center;font-size: 12px">RS. '.$product_price.'/</td></tr></table></td></tr></table>' ;
        }   
        $ordersubtotal = intval($order->getSubtotal());
        $discount = intval($order->getDiscountAmount());
        $grandtotal = intval($order->getGrandTotal());   
        $html .= '<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width:50%;"><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:20px;"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; font-weight:100; color: #868686;">Free Gift : <span style=" color:#4f4f4f; font-weight:600">Gift Voucher</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:100; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:100; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; font-weight:100; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">'.$paymentmode.'</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:100; line-height:2px;">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:100; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td  style=" width:100px;font-size:8px;font-family: \'Open Sans\', sans-serif; color:#757575; font-weight:500;width:60%"><a href="'.$carturl.'" ><input type="button" value="TRACK / CANCEL ORDER" style="width:118px;font-size:8px;padding:10px; border-radius:4px;  color:#2762a8; font-weight:bold; margin-top:10px; background:#fff601;-webkit-box-shadow: 0px 3px 1px 0px #ded800; -moz-box-shadow:    0px 3px 1px 0px #ded800;box-shadow:0px 3px 1px 0px #ded800; border:#ded800"/></a></td><td style=" padding:8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $shippingaddress = '<span style=" color:#4f4f4f; font-weight:bold; line-height:20px;">'.$order->getCustomerFirstname().' '.$order->getCustomerLastname().'<br>'.$streetaddress[0].',<br> '.$order->getShippingAddress()->getCity().'<br>'.$order->getShippingAddress()->getRegion().' - '.$order->getShippingAddress()->getPostcode().'</span>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                //$zipcode = Mage::getModel('wms/zipcode')->load($this->getShippingAddress()->getPostcode(),'zipcode');
                //$i = $zipcode->getDeliveryTime();
                $deliverydate = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false). ' + 7 day'));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(12);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['delivery_date'] =  $deliverydate;
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $emailTempVariables['shippingaddress'] = $shippingaddress;
                $emailTempVariables['customerid'] = $order->getCustomerId();
                $emailTempVariables['myaccounturl'] = Mage::getBaseUrl().'customer/account/';
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('This order is ready to ship', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('This order is ready to Ship'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productinstallAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $order->setStatus('install');
        $order->save();
        $history = $order->addStatusHistoryComment('This product has been installed properly', true);
        $history->setIsCustomerNotified(false);
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productreturnrequestAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("return_request");
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
        $emailTemp = Mage::getModel('core/email_template')->load(6);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  date("d-m-Y");
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('Order return request by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order return request by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productexchangerequestAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("exchange_request");
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(9);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  date("d-m-Y");
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('Order exchange request by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order exchange request by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productreturnapproveAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("return_request_approve");
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(8);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  date("d-m-Y");
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('Order return approve by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order return approve by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productreturnrejectAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $history1 = $order->getStatusHistoryCollection();
        foreach($history1 as $orderstatus)
        {
            if (strpos($orderstatus->getStatus(), 'cancel_request') !== false) {
                $timearray = $orderstatus->getCreatedAt();
            }
        }
        $timearray = date('d-m-Y', strtotime($timearray));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("return_request_reject");
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
        $emailTemp = Mage::getModel('core/email_template')->load(7);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  $timearray;
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('Order return reject by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order exchange request by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productreturninitiatedAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order->getCustomerName();
        $order->setStatus("return_request_initiated");
        $history = $order->addStatusHistoryComment('Order return initiated.', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order return initiated.'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productrefundinitiatedAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("refund_initiated");
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(15);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  date("d-m-Y");
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $emailTempVariables['refundamount'] = $grandtotal;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('Refund initiated', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Refund initiated'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productcancelrequestAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus('cancel_request');
        $orderItems = $order->getItemsCollection();
                foreach ($orderItems as $item){
                    $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'image');
                    $product_price = intval($item->getPrice());
                    $product_name = $item->getName();
                    $itemqty = intval($item->getQtyOrdered());
                    $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(3);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  date("d-m-Y");
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
                
                /* Send mail to Support */
                $emailTemp->setSenderName($Username);
                $emailTemp->setSenderEmail($useremail);
                $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
                /* Send mail to Support */

                /* Send mail coy to Customer */
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
                $emailTemp->send($useremail,$Username,$emailTempVariables);
        $history = $order->addStatusHistoryComment('Order has been canceled by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order has been canceled by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productacceptcancelrequestAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("cancel_request_approve");
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
        }   
        $ordersubtotal = intval($order->getSubtotal());
        $discount = intval($order->getDiscountAmount());
        $grandtotal = intval($order->getGrandTotal());   
        $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(5);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  date("d-m-Y");
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('Order cancel request by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order cancel request by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productrejectcancelrequestAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $history1 = $order->getStatusHistoryCollection();
        foreach($history1 as $orderstatus)
        {
            if (strpos($orderstatus->getStatus(), 'cancel_request') !== false) {
                $timearray = $orderstatus->getCreatedAt();
            }
        }
        $timearray = date('d-m-Y', strtotime($timearray));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("cancel_request_reject");
        $orderItems = $order->getItemsCollection();
                foreach ($orderItems as $item){
                    $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
                    $product_price = intval($item->getPrice());
                    $product_name = $item->getName();
                    $itemqty = intval($item->getQtyOrdered());
                    $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(4);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  $timearray;
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
                /* Send mail to Support */
                $emailTemp->setSenderName($Username);
                $emailTemp->setSenderEmail($useremail);
                $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
                /* Send mail to Support */

                /* Send mail coy to Customer */
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
                $emailTemp->send($useremail,$Username,$emailTempVariables);
        $history = $order->addStatusHistoryComment('Order cancel request reject by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order cancel request reject by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productacceptexchangerequestAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("exchange_request_approve");
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(11);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  date("d-m-Y");
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('Order exchange request approve by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order exchange request approve by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productrejectexchangerequestAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $history1 = $order->getStatusHistoryCollection();
        foreach($history1 as $orderstatus)
        {
            if (strpos($orderstatus->getStatus(), 'cancel_request') !== false) {
                $timearray = $orderstatus->getCreatedAt();
            }
        }
        $timearray = date('d-m-Y', strtotime($timearray));
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order_id = $order['increment_id'];
        $order->setStatus("exchange_request_reject");
        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){
            $imageurl = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product')->load($item->getProductId()), 'thumbnail');
            $product_price = intval($item->getPrice());
            $product_name = $item->getName();
            $itemqty = intval($item->getQtyOrdered());
            $html .= '<table  border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;width: 50%"><tr><td><table cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#f5f2f2 solid thin;width: 96%"><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tr><td><img src="'.$imageurl.'" alt="'.$product_name.'" title="'.$product_name.'" width="59" height="75" /></td><td style="font-size:12px;color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></table></td><td style=" border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td style="font-size:12px; border-left:1px solid #f5f2f2;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr></table></td></tr></table>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html .='<table border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;width: 50%"><tr><td><table  cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;width: 96%"><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By : <span style=" color:#4f4f4f; font-weight:600">Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:00; line-height:2px; font-weight:500; ">Discount : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total : </td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></table></td></tr></table>';
                $ordertime = date('d-m-Y', strtotime(Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', $showTime=false)));
                $telephone = substr($order->getBillingAddress()->getTelephone(), 0, 4) . ' ' . substr($order->getBillingAddress()->getTelephone(), 4);
                $useremail = $order->getCustomerEmail(); 
                $Username = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(10);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $ordertime;
                $emailTempVariables['cancelationapprovedate'] =  $timearray;
                $emailTempVariables['phoneno'] = $telephone;
                $emailTempVariables['name'] =  $order->getCustomerFirstname().' '.$order->getCustomerLastname();
                $emailTempVariables['emailid'] = $order->getCustomerEmail();
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        /* Send mail to Support */
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
        /* Send mail to Support */

        /* Send mail coy to Customer */
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        $history = $order->addStatusHistoryComment('Order exchange request reject by admin', false);
        $history->setIsCustomerNotified(true);
        $order->save();
        $this->_getSession()->addSuccess($this->__('Order exchange request reject by admin'));
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function productcancelbyadminAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $order->setStatus('canceled')->save();
        $this->_getSession()->addSuccess($this->__('The order has been cancelled.'));
        $history = $order->addStatusHistoryComment('This product has been cancelled properly', true);
        $history->setIsCustomerNotified(false);
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
     public function productreturnrequestrejectAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $history = $order->getStatusHistoryCollection();
        $statusarray = array();$i=0;
        foreach($history as $orderstatus)
        {
            $i++;
            $statusarray[$i] = $orderstatus->getStatus();
        }
        print_r($statusarray);
        if (in_array("install", $statusarray)){$orderstatus='install';} 
        else if (in_array("delivery_complete", $statusarray)){$orderstatus='delivery_complete';} 
        else {$orderstatus='exchange_request_reject';} 
        $order->setStatus($orderstatus);
        $order->save();
        $this->_getSession()->addSuccess($this->__('The order has been change to '.$orderstatus.'.'));
        $history = $order->addStatusHistoryComment('The order has been change to '.$orderstatus.'.', true);
        $history->setIsCustomerNotified(false);
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
     public function productcancelrequestrejectAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $history = $order->getStatusHistoryCollection();
        $statusarray = array();$i=0;
        foreach($history as $orderstatus)
        {
            $i++;
            $statusarray[$i] = $orderstatus->getStatus();
        }
        //print_r($statusarray);
        if (in_array("despatch", $statusarray)){$orderstatus='despatch';} 
        else if (in_array("redytoship", $statusarray)){$orderstatus='redytoship';} 
        else if (in_array("processing", $statusarray)){$orderstatus='processing';} 
        else if (in_array("pending", $statusarray)){$orderstatus='pending';} 
        else {$orderstatus='cancel_request_reject';} 
        $order->setStatus($orderstatus);
        $order->save();
        $this->_getSession()->addSuccess($this->__('The order has been change to '.$orderstatus.'.'));
        $history = $order->addStatusHistoryComment('The order has been change to '.$orderstatus.'.', true);
        $history->setIsCustomerNotified(false);
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
     public function productexchangerequestrejectAction() {
        $order = Mage::getModel("sales/order")->load($this->getRequest()->getParam('order_id'));
        $history = $order->getStatusHistoryCollection();
        $statusarray = array();$i=0;
        foreach($history as $orderstatus)
        {
            $i++;
            $statusarray[$i] = $orderstatus->getStatus();
        }
        print_r($statusarray);
        if (in_array("install", $statusarray)){$orderstatus='install';} 
        else if (in_array("delivery_complete", $statusarray)){$orderstatus='delivery_complete';} 
        else {$orderstatus='exchange_request_reject';} 
        $order->setStatus($orderstatus);
        $order->save();
        $this->_getSession()->addSuccess($this->__('The order has been change to '.$orderstatus.'.'));
        $history = $order->addStatusHistoryComment('The order has been change to '.$orderstatus.'.', true);
        $history->setIsCustomerNotified(false);
        $this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
    public function orderawsnoconfirmAction(){
        $html ='';
        $orderid = $this->getRequest()->getParam('order_id');
        $formkey = Mage::getSingleton('core/session')->getFormKey();
        $url = Mage::helper("adminhtml")->getUrl('adminhtml/orderstatuschange/productdispatchpopup', array('order_id' => $orderid));
        echo $html .= '<form id="saveawsno"  method="post" action="'.$url.'"> <div class="box-left">
                        <input type="hidden" name="form_key" value="'.$formkey.'" />
                        <input type="hidden" name="awsconforderid" id="awsconforderid" value="'.$orderid.'" />    
                        <div class="entry-edit">
                            <div class="fieldset">
                                <table cellspacing="0" class="form-list">
                                    <tbody>
                                        <tr>
                                            <td class="label"><label>Delivered By</label></td>
                                            <td class="value">
                                            <select id="deliveredby" name="deliveredby" class="required-entry">
                                            <option value="">--Select delivery provider name-- </option>
                                            <option value="gati">Gati</option>
                                            </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label"><label>Aws No:</label></td>
                                            <td class="value"><strong><input type="text" class="input-text required-entry" name="awsno" id="awsno"></strong></td>
                                        </tr>
                                        <tr>
                                        <td>&nbsp;</td>
                                            <td class="label" style="float:right"><p class="form-buttons"><button onclick="saveawsno.submit()">Submit AWS No.</button></p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> </div></form>
                    <script type="text/javascript"> saveawsno = new varienForm(\'saveawsno\');</script>';
        //echo 'keshab'.$this->getRequest()->getParam('order_id').'karmakar';die;
    }
    public function productdispatchpopupAction() {
        $deliveredby = $this->getRequest()->getParam('deliveredby'); 
        $awsno = $this->getRequest()->getParam('awsno');
        $orderid = $this->getRequest()->getParam('awsconforderid');
        $order = Mage::getModel("sales/order")->load($orderid);
        $itemsarray = array();
        //$shipmentId = Mage::getModel('sales/order_shipment_api')->create($order->getIncrementId(), $itemsarray ,'your_comment' ,false,1);
        //echo $shipmentId;die;
        $shipment_collection = Mage::getResourceModel('sales/order_shipment_collection');
        $shipment_collection->addAttributeToFilter('order_id', $orderid );
        foreach($shipment_collection as $sc) {print_r($sc);
        $shipment = Mage::getModel('sales/order_shipment');
        $shipment->load($sc->getId());echo $shipment->getId().'kk'.'<br>';
        /*if($shipment->getId() != '') {
        $track = Mage::getModel('sales/order_shipment_track')
                 ->setShipment($shipment)
                 ->setData('title', $type)
                 ->setData('number', $code)
                 ->setData('carrier_code', 'custom')
                 ->setData('order_id', $shipment->getData('order_id'))
                 ->save();
        }*/
        }
        
        
        $adminEmail= Mage::getStoreConfig('trans_email/ident_support/email');
        $order->getCustomerName();
        //$order->setStatus("despatch");
        /* Start Send mail to Support */
        $useremail = $order->getCustomerEmail(); 
        $Username = $order->getCustomerName();
        $emailTemp = Mage::getModel('core/email_template')->load(7);
        $emailTempVariables = array();
        $adminEmail=Mage::getStoreConfig('trans_email/ident_support/email');
        $adminUsername = 'Admin';
		$emailTempVariables['customername'] = $adminUsername;
		$emailTempVariables['orderid'] = $order->getIncrementId();
        $emailTempVariables['content'] = 'Order Id #'.$order->getIncrementId().' has been dispatch';
        $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        $emailTemp->setSenderName($Username);
        $emailTemp->setSenderEmail($useremail);
        $emailTemp->send($adminEmail,$adminUsername,$emailTempVariables);
        /*end Send mail to Support */

        /*Start Send mail coy to Customer */
        $Username = $order->getCustomerName();
        $useremail = $order->getCustomerEmail();
        $emailTemp = Mage::getModel('core/email_template')->load(8);
        $emailTempVariables = array();
        $adminEmail=Mage::getStoreConfig('trans_email/ident_support/email');
        $adminUsername = 'Admin';
		$emailTempVariables['orderid'] = $order->getIncrementId();
        $emailTempVariables['customername'] = $order->getCustomerName();
        $emailTempVariables['content']='Product has been dispatch';
        $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail,$Username,$emailTempVariables);
        /*end Send mail coy to Customer */
        
        
        $history = $order->addStatusHistoryComment('This product has been dispatch successfully', false);
        $history->setIsCustomerNotified(true);
        //$order->save();
        //$this->_redirect('*/sales_order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
    }
}
