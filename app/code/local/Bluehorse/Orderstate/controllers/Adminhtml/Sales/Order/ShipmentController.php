<?php

require_once 'Mage/Adminhtml/controllers/Sales/Order/ShipmentController.php';
class Bluehorse_Orderstate_Adminhtml_Sales_Order_ShipmentController extends Mage_Adminhtml_Sales_Order_ShipmentController
{

    protected function _saveShipment($shipment)
    {
        $shipment->getOrder()->setIsInProcess(true);
        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            //->addObject($shipment->getOrder())
            ->save();

        return $this;
    }


}
