<?php
    $installer = $this;
    $statusTable = $installer->getTable('sales/order_status');
    $statusStateTable = $installer->getTable('sales/order_status_state');
    $installer->getConnection()->insertArray(
        $statusTable,
        array('status', 'label'),
        array(
            array('status' => 'redytoship', 'label' => 'Ready to Ship'),
            array('status' => 'install', 'label' => 'Install'),
            array('status' => 'cancel_request_approve', 'label' => 'Cancel Request Approve'),
            array('status' => 'cancel_request_reject', 'label' => 'Cancel Request Reject'),
            array('status' => 'return_request_approve', 'label' => 'Return Request Approve'),
            array('status' => 'return_request_reject', 'label' => 'Return Request Reject'),
            array('status' => 'return_request_initiated', 'label' => 'Return Request Initiated'),
            array('status' => 'exchange_request_approve', 'label' => 'Exchange Request Approve'),
            array('status' => 'exchange_request_reject', 'label' => 'Exchange Request Reject'),
            array('status' => 'refund_initiated', 'label' => 'Refund Initiated'),
        )
    );
    $installer->getConnection()->insertArray(
        $statusStateTable,
        array('status', 'state', 'is_default'),
            array(
                array('status' => 'redytoship', 'state' => 'redytoship', 'is_default' => 1),
                array('status' => 'install', 'state' => 'install', 'is_default' => 1),
                array('status' => 'cancel_request_approve', 'state' => 'cancel_request_approve', 'is_default' => 1),
                array('status' => 'cancel_request_reject', 'state' => 'cancel_request_reject', 'is_default' => 1),
                array('status' => 'return_request_approve', 'state' => 'return_request_approve', 'is_default' => 1),
                array('status' => 'return_request_reject', 'state' => 'return_request_reject', 'is_default' => 1),
                array('status' => 'return_request_initiated', 'state' => 'return_request_initiated', 'is_default' => 1),
                array('status' => 'exchange_request_approve', 'state' => 'exchange_request_approve', 'is_default' => 1),
                array('status' => 'exchange_request_reject', 'state' => 'exchange_request_reject', 'is_default' => 1),
                array('status' => 'refund_initiated', 'state' => 'refund_initiated', 'is_default' => 1),
            )
        );
    
    