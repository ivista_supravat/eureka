<?php
    $installer = $this;
    $statusTable = $installer->getTable('sales/order_status');
    $statusStateTable = $installer->getTable('sales/order_status_state');
    $installer->getConnection()->insertArray(
        $statusTable,
        array('status', 'label'),
        array(
            array('status' => 'despatch', 'label' => 'Despatch'),
            array('status' => 'cancel_request', 'label' => 'Cancel Request'),
            array('status' => 'return_request', 'label' => 'Return Request'),
            array('status' => 'exchange_request', 'label' => 'Exchange Request'),
            array('status' => 'delivery_complete', 'label' => 'Delivery Complete'),
        )
    );
    $installer->getConnection()->insertArray(
        $statusStateTable,
        array('status', 'state', 'is_default'),
            array(
                array('status' => 'despatch', 'state' => 'despatch', 'is_default' => 1),
                array('status' => 'cancel_request', 'state' => 'cancel_request', 'is_default' => 1),
                array('status' => 'return_request', 'state' => 'return_request', 'is_default' => 1),
                array('status' => 'exchange_request', 'state' => 'exchange_request', 'is_default' => 1),
                array('status' => 'delivery_complete', 'state' => 'delivery_complete', 'is_default' => 1),
            )
        );
    
    