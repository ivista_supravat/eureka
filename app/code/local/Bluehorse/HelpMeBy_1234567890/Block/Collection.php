<?php

class Bluehorse_HelpMeBy_Block_Collection extends Mage_Catalog_Block_Product_List {

    protected function _getProductCollection() {


        if ($this->getRequest()->isPost()) {
            $productType = $this->getRequest()->getParam('product_type');
            $pincode = $this->getRequest()->getParam('pincode');
            $waterSource = $this->getRequest()->getParam('water_source');
            $budget = $this->getRequest()->getParam('budget');

            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');

            if (!empty($pincode) && !empty($waterSource)) {
                $query = 'SELECT tds_avg FROM wd_tds WHERE pincode=' . $pincode . ' AND status=1 AND water_source_code=' . $waterSource . '  LIMIT 1';

                $tds_avg = $readConnection->fetchOne($query);
            }
            if (!empty($tds_avg)) {
                $query2 = 'SELECT technology FROM wd_technology WHERE max_tds <= ' . $tds_avg . ' AND status=1';
                $technologies = $readConnection->fetchOne($query2);

                if (!empty($technologies)) {
                    $options = Mage::getModel('eav/config')->getAttribute('catalog_product', 'technology')->getSource()->getAllOptions();
                    $optionId = false;
                    foreach ($options as $option) {

                        if (strtolower($option['label']) == strtolower($technologies)) {
                            $optionId = $option['value'];
                            break;
                        }
                    }
                }
            }
        }
        Mage::log($query2, null, 'Help_Me_By.log');

        if (is_null($this->_productCollection)) {
            $collection = Mage::getModel('catalog/product')->getCollection();
            $collection
                    ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                    //->addAttributeToFilter('technology', 3)
                    ->addMinimalPrice()
                    ->addFinalPrice()
                    ->addTaxPercents();

            Mage::log($optionId, null, 'Help_Me_By.log');

            if ($optionId) {
                $collection->addAttributeToFilter('technology', $optionId);
                $collection->addAttributeToFilter('price', array('lteq' => $budget));
            }
            $collection->clear();
            $collection->setPageSize(false);
            //echo $collection->getSelect()->__toString();
            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
            $this->_productCollection = $collection;
        }
        return $this->_productCollection;
    }

}
