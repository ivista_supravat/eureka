<?php

class Bluehorse_WMS_Block_Adminhtml_Wms_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "wms";
        $this->_controller = "adminhtml_wms";
        $this->_updateButton("save", "label", Mage::helper("wms")->__("Save"));
        $this->_updateButton("delete", "label", Mage::helper("wms")->__("Delete"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("wms")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);



        $this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
    }

    public function getHeaderText() {
        if (Mage::registry("wms_data") && Mage::registry("wms_data")->getId()) {

            return Mage::helper("wms")->__("Edit  '%s'", $this->htmlEscape(Mage::registry("wms_data")->getName()));
        } else {

            return Mage::helper("wms")->__("Add New Warehouse");
        }
    }

}
