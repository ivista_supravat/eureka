<?php

class Bluehorse_WMS_Block_Adminhtml_Wms_Edit_Tab_Product extends Mage_Adminhtml_Block_Widget_Grid {

    /**
     * Set grid params
     *
     */
    public function __construct() {
        parent::__construct();
        $this->setId('wms_product_grid');
        $this->setDefaultSort('id');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {

        //load collection 
        $zipcode_id = $this->getRequest()->getParam('id');
        if (!isset($zipcode_id)) {
            $zipcode_id = 0;
        }
        $collection = Mage::getModel('wms/zipcode')->load((int) $zipcode_id);

        $inventoryCollections = Mage::getModel('wms/inventory')->getCollection();
        $inventoryCollections->addFieldToFilter('region', $collection->getRegion());
        //echo $inventoryCollections->getSelect();
        //$collection->addFieldToFilter('zipcode_id', (int) $zipcode_id);
        //join with 3rd table
        //$collection->getSelect()->join('wms_pincode', 'main_table.zipcode_id = wms_pincode.id', array('zipcode'));

        $this->setCollection($inventoryCollections);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('id', array(
            'header' => Mage::helper('wms')->__('ID'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'id',
        ));

        $this->addColumn('product_sku', array(
            'header' => Mage::helper('wms')->__('Product sku'),
            'align' => 'left',
            'index' => 'product_sku',
        ));

        $this->addColumn('product_qty', array(
            'header' => Mage::helper('wms')->__('Product Qty'),
            'align' => 'left',
            'index' => 'product_qty',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('wms')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getOptionArray15(),
        ));

        return parent::_prepareColumns();
    }

    static public function getIsPrimaryOptionsArray() {
        $data_array = array();
        $data_array[0] = 'Yes';
        $data_array[1] = 'No';
        return($data_array);
    }

    static public function getIsPrimaryOptionsValueArray15() {
        $data_array = array();
        foreach (Bluehorse_Warehouse_Block_Adminhtml_Warehouse_Edit_Tab_Zipcode::getIsPrimaryOptionsArray() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return($data_array);
    }

    public function getSelectedWareZipcode() {
        return array();
    }

}
