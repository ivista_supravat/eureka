<?php

class Bluehorse_WMS_Block_Adminhtml_Wms_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("wms_form", array("legend" => Mage::helper("wms")->__("Item information")));

        $fieldset->addField("state", "select", array(
            "label" => Mage::helper("wms")->__("State Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "region",
            'values' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getState(),
        ));
        
        $fieldset->addField("region", "select", array(
            "label" => Mage::helper("wms")->__("Region Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "region",
            'values' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getRegions(),
        ));

        $fieldset->addField("zipcode", "text", array(
            "label" => Mage::helper("wms")->__("Zipcode"),
            "class" => "required-entry",
            "required" => true,
            "name" => "zipcode",
        ));
        $fieldset->addField("delivery_time", "text", array(
            "label" => Mage::helper("wms")->__("Delivery In Day"),
            "class" => "required-entry",
            "required" => true,
            "name" => "delivery_time",
        ));
        $fieldset->addField("is_primary", "select", array(
            'label' => Mage::helper('wms')->__('Is Primary'),
            'values' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getIsPrimaryOptionsArray(),
            'name' => 'status',
        ));

        $fieldset->addField("service_availability", "select", array(
            'label' => Mage::helper('wms')->__('Service Availability'),
            'values' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getIsPrimaryOptionsArray(),
            'name' => 'service_availability',
        ));

        $fieldset->addField("cod", "select", array(
            'label' => Mage::helper('wms')->__('Is COD'),
            'values' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getIsPrimaryOptionsArray(),
            'name' => 'cod',
        ));
        $fieldset->addField("status", "select", array(
            'label' => Mage::helper('wms')->__('Status'),
            'values' => Bluehorse_WMS_Block_Adminhtml_Wms_Grid::getValueArray15(),
            'name' => 'status',
        ));
        if (Mage::getSingleton("adminhtml/session")->getWarehouseData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getWarehouseData());
            Mage::getSingleton("adminhtml/session")->setWarehouseData(null);
        } elseif (Mage::registry("wms_data")) {
            $form->setValues(Mage::registry("wms_data")->getData());
        }
        return parent::_prepareForm();
    }

}
