<?php

class Bluehorse_WMS_Model_Zipcode extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('wms/zipcode');
    }

    public function uploadAndImport($object) {
        //echo "Called model"; die;
        $resultObj = $this->_getResource()->uploadAndImport($object);
        if ($resultObj) {
            return $resultObj;
        }
    }

}
