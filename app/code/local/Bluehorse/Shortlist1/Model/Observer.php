<?php

class Bluehorse_WMS_Model_Observer {

    public function __contruct() {
        
    }

    public function updateWarehouseQty(Varien_Event_Observer $observer) {

        $order = $observer->getEvent()->getOrder();
        $ordered_items = $order->getAllItems();
        foreach ($ordered_items as $item) {

            $modelObj = Mage::getModel('warehouse/product');
            $modelObj->load($item->getSku(), 'product_sku');
            if ($modelObj->getId()) {
                $modelObj->setProductQty($modelObj->getProductQty() - $item->getQtyOrdered());
                $modelObj->save();
            }
            Mage::log($modelObj->getProductQty(), null, "order.log", true);
        }
    }

    /*
     * Check if products qty is available before placing order
     * Throw Exception if not and quit order
     */

    public function salesQuoteItemQtySetAfter(Varien_Event_Observer $observer) {
        $quoteItem = $observer->getEvent()->getItem();
        $qty = $quoteItem->getQty();



        /*
          $lastCheckResult = Mage::app()->loadCache('api_check_' . $quoteItem->getId() . '_' . $qty);
          if ($lastCheckResult == '') {
          $lastCheckResult = true;
          // Check remote SOAP API
          if (!$api->checkStockQty($quoteItem->getSku(), $qty)) {
          $lastCheckResult = false;
          }
          }

          // Cache API Response for 1 minute to minimize requests
          Mage::app()->saveCache($lastCheckResult, 'api_check_' . $quoteItem->getId() . '_' . $qty, array(), 60);
         */
        // If the qty is not available then throw an exception to display an error and disable the Checkout button in the cart page.
        /*if (!$lastCheckResult) {
            //$quoteItem->getQuote()->setHasError(true);
            $quoteItem->addErrorInfo(
                    'wms', Mage_CatalogInventory_Helper_Data::ERROR_QTY, Mage::helper('wms')->__('The requested Qty is not available')
            );
        }*/
    }

}
