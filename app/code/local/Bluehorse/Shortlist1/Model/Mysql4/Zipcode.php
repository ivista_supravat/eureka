<?php

class Bluehorse_WMS_Model_Mysql4_Zipcode extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init("wms/zipcode", "id");
    }

    public function uploadAndImport($object) {

        $this->_behavior = $object->getRequest()->getPost('behavior');
        $importFile = $_FILES['import_file'];
        if (empty($importFile['tmp_name'])) {
            return;
        }
        $csvFile = $importFile['tmp_name'];

        $this->_importErrors = array();
        $this->_importedRows = 0;



        $io = new Varien_Io_File();
        $info = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        // check and skip headers
        $headers = $io->streamReadCsv();
        if ($headers === false || count($headers) < 2) {
            $io->streamClose();
            Mage::throwException(Mage::helper('wms')->__('Invalid Stores File Format'));
        }
        $adapter = $this->_getWriteAdapter();
        $adapter->beginTransaction();
        try {
            $rowNumber = 1;
            $importData = array();
            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;

                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->_getImportRow($csvLine, $rowNumber);

                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = array();
                }
            }
            if ($this->_importErrors) {
                $error = Mage::helper('wms')->__('File has not been imported. See the following list of errors: <br>%s', implode(" <br>", $this->_importErrors));
                Mage::throwException($error);
            }

            if (empty($this->_importErrors)) {
                $this->_saveImportData($importData);
            }

            $io->streamClose();
        } catch (Mage_Core_Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            Mage::throwException(Mage::helper('wms')->__('An error occurred while import stores csv.'));
        }
        $adapter->commit();

        //add success message
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wms')->__($this->_importedRows . ' - Rows imported successfully.'));
        return $this;
    }

    protected function _getImportRow($row, $rowNumber = 0) {
        // validate row

        if (count($row) < 8) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid stores format in the Row #%s', $rowNumber);
            return false;
        }

        // strip whitespace from the beginning and end of each row
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }


        // validate store name
        if (empty($row[0])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid pincode "%s" in the Row #%s.', $row[0], $rowNumber);
            return false;
        }

        if (empty($row[1])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid Service availability  value "%s" in the Row #%s.', $row[1], $rowNumber);
            return false;
        }

        // validate Zipcode
        if (empty($row[2])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid cod  "%s" in the Row #%s.', $row[2], $rowNumber);
            return false;
        }

        if (empty($row[3])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid is primary "%s" in the Row #%s.', $row[3], $rowNumber);
            return false;
        }

        if (empty($row[4])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid Statename  "%s" in the Row #%s.', $row[4], $rowNumber);
            return false;
        }

        if (empty($row[5])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid sku "%s" in the Row #%s.', $row[5], $rowNumber);
            return false;
        }

        // validate product qty
        if (empty($row[6])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid product qty "%s" in the Row #%s.', $row[6], $rowNumber);
            return false;
        }

        if (empty($row[7])) {
            $this->_importErrors[] = Mage::helper('wms')->__('Invalid Delivert time "%s" in the Row #%s.', $row[6], $rowNumber);
            return false;
        }

        $storeRow = array(
            'zipcode' => $row[0],
            'service_availability' => $row[1],
            'cod' => $row[2],
            'is_primary' => $row[3],
            'Statename' => $row[4],
            'sku' => $row[5],
            'qty' => $row[6],
            'delivery_time' => $row[7],
            'status' => 0,
            'position' => 1,
            'created_at' => Mage::getModel('core/date')->timestamp(time())
        );
        return $storeRow;
    }

    protected function _saveImportData(array $stores) {

        if (!empty($stores)) {
            $transactionSave = Mage::getModel('core/resource_transaction');
            foreach ($stores as $store) {

                $zipcodeId = '';
                $productId = '';
                $store['service_availability'] = ("Y" == $store['service_availability']) ? 0 : 1;
                $store['cod'] = ("Y" == $store['cod']) ? 0 : 1;
                $store['is_primary'] = ("Y" == $store['is_primary']) ? 0 : 1;

                $zipModel = Mage::getModel('wms/zipcode');
                $inventory = Mage::getModel('wms/inventory');

                if (!empty($store['zipcode'])) {
                    $zipModel->load($store['zipcode'], 'zipcode');
                    if ($zipModel->getId()) {
                        $zipcodeId = $zipModel->getId();
                    }
                }
                $region = Mage::getModel('directory/region')->load('01', 'code');
                $store['region'] = $region->getId();
                $zipModel->setData($store);

                if (!empty($zipcodeId)) {
                    $zipModel->setId($zipcodeId);
                }
                $zipModel->save();

                /*
                 * Save product page
                 */

                if (!empty($store['sku'])) {
                    $inventory->load($store['sku'], 'product_sku');
                    if ($inventory->getId()) {
                        $productId = $inventory->getId();
                    }
                }


                $prdData['zipcode_id'] = (int) $zipModel->getId();
                $prdData['product_id'] = 0;
                $prdData['product_sku'] = $store['sku'];
                $prdData['product_qty'] = $store['qty'];
                $prdData['status'] = 0;
                $prdData['position'] = 0;

                $inventory->setData($prdData);

                if (!empty($productId)) {
                    $inventory->setId($productId);
                }
                $inventory->save();
                //check for overwrite existing store(s) 
                if ($this->_behavior == 1 || empty($inventory)) {
                    //$transactionSave->addObject($prodModel);
                }
            }
            $this->_importedRows += count($stores);
        }
        return $this;
    }

}
