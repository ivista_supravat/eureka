<?php

class Bluehorse_WMS_Model_Inventory extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('wms/inventory');
    }

}
