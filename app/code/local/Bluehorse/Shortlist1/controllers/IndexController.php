<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

class Bluehorse_WMS_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $this->loadLayout();
        $myblock = $this->getLayout()->createBlock('wms/adminhtml_warehouse');
        $this->_addContent($myblock);
        $this->renderLayout();
    }

    public function chkPincodeAction() {

        //Send json
        $postcode = Mage::app()->getRequest()->getParam('postcode');
        $product_sku = Mage::app()->getRequest()->getParam('product_sku');

        $response = [
            'error' => false,
            'message' => 'No data found'
        ];

        if (empty($postcode) && empty($product_sku)) {
            $response = [
                'error' => false,
                'message' => 'Invalid request'
            ];

            $result = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($result);
        }
        try {
            Mage::getSingleton('core/session')->setPincode($postcode);
            // Get entity id for search pincode
            $zipcode = Mage::getModel('wms/zipcode')->load($postcode, 'zipcode');



            if ($zipcode->getServiceAvailability()) {
                $response = [
                    'error' => false,
                    'message' => 'Service not available'
                ];
                $result = Mage::helper('core')->jsonEncode($response);
                $this->getResponse()->setHeader('Content-type', 'application/json');
                $this->getResponse()->setBody($result);
                return;
            }
            //var_dump($zipcodeLoad->getId());

            if (!empty($zipcode->getRegion()) && $zipcode->getRegion() > 0) {

                $productOb = Mage::getModel('wms/inventory')->getCollection();

                $productOb->addFieldToFilter('zipcode_id', (int) $zipcode->getId());
                $productOb->addFieldToFilter('product_sku', (string) $product_sku);
                $productObj = $productOb->getFirstItem();
                //echo $productOb->getSelect()->__toString(); 
                //echo $productObj->getProductQty();
                //die;
                if ($productOb->count() && !empty($productObj->getProductQty()) && $productObj->getProductQty() > 0) {
                    $response = [
                        'error' => false,
                        'openpanel' => '1',
                        'deliveryDate' => date('D, dS M', strtotime('+' . $zipcode->getDeliveryTime() . ' days')),
                        'cod' => (0 == $zipcode->getCod()) ? "AVAILABLE" : "UNAVAILABLE",
                        'qty' => $productObj->getProductQty(),
                        'message' => Mage::helper("wms")->__("Delivery within %s days  ", $zipcode->getDeliveryTime())
                    ];
                } else {
                    // get associate 
                    $response = [
                        'error' => false,
                        'openpanel' => '1',
                        'deliveryDate' => 'unavailable',
                        'message' => $this->__('Out of stock', $postcode),
                        'qty' => $productObj->getProductQty()
                    ];
                }
            }
        } catch (Exception $ex) {

            $response = [
                'error' => true,
                'openpanel' => '0',
                'message' => $this->__($ex->getMessage())
            ];
            //var_dump();
        }
        $result = Mage::helper('core')->jsonEncode($response);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($result);
    }

    public function chkPincodeCheckoutAction() {
        //Send json
        $postcode = Mage::app()->getRequest()->getParam('postcode');
        $product_sku = Mage::app()->getRequest()->getParam('product_skus');

        $response = [
            'error' => false,
            'message' => 'No data found'
        ];

        if (empty($postcode) && empty($product_sku)) {
            $response = [
                'error' => false,
                'message' => 'Invalid request'
            ];

            $result = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($result);
            return;
        }
        try {
            // Get entity id for search pincode

            $zipcode = Mage::getModel('wms/zipcode');
            $zipcode->load($postcode, 'zipcode');
            #Zend_Debug::dump($zipcode->getServiceAvailability()); die;


            if (null === $zipcode->getServiceAvailability() || 0 === $zipcode->getServiceAvailability()) {
                $response = [
                    'error' => false,
                    'message' => 'Service not available'
                ];
                $result = Mage::helper('core')->jsonEncode($response);
                $this->getResponse()->setHeader('Content-type', 'application/json');
                $this->getResponse()->setBody($result);
                return;
            }
            if (!(is_null($zipcode->getId())) && $zipcode->getId() > 0) {

                $productOb = Mage::getModel('wms/inventory')->getCollection();

                $productOb->addFieldToFilter('zipcode_id', (int) $zipcode->getId());
                $productOb->addFieldToFilter('product_sku', array('in' => $product_sku));
                $productObj = $productOb->getFirstItem();

                if (!empty($productObj->getProductQty()) && $productObj->getProductQty() > 0) {
                    $response = [
                        'error' => false,
                        'deliveryDate' => date('D, dS M', strtotime('+' . $zipcode->getDeliveryTime() . ' days')),
                        'cod' => (0 == $zipcode->getCod()) ? "AVAILABLE" : "UNAVAILABLE",
                        'qty' => $productObj->getProductQty(),
                        'message' => Mage::helper("wms")->__("Delivery within %s days  ", $zipcode->getDeliveryTime())
                    ];
                } else {
                    // get associate 
                    $response = [
                        'error' => false,
                        'message' => $this->__('Requested quantity not available')
                    ];
                }
            }
        } catch (Exception $ex) {

            $response = [
                'error' => true,
                'openpanel' => '0',
                'message' => $this->__($ex->getMessage())
            ];
        }
        $result = Mage::helper('core')->jsonEncode($response);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($result);
    }

}
