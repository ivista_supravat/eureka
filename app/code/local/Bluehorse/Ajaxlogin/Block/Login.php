<?php
/**
 * Customer login form block
 *
 * @category   Bluehorse
 * @package    Bluehorse_Ajaxlogin
 * @author      Amit Bera <amit.bera@bluehorse.in>
 * @copyright  Copyright (c) 2015 , Bh (http://www.bluehorse.in)
 */
class Bluehorse_Ajaxlogin_Block_Login extends Mage_Core_Block_Template
{
    
    private $_username = -1;

   

    /**
     * Retrieve form posting url
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->helper('ajaxlogin')->getLoginPostUrl();
    }

   
    /**
     * Retrieve username for form field
     *
     * @return string
     */
    public function getUsername()
    {
        if (-1 === $this->_username) {
            $this->_username = Mage::getSingleton('customer/session')->getUsername(true);
        }
        return $this->_username;
    }
}
