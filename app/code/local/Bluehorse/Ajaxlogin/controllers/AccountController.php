<?php
/**
 * @ Purpose  Customer Register &logged in by Ajax
 * @category   Bluehorse
 * @package    Bluehorse_Ajaxlogin
 * @author      Amit Bera <amit.bera@bluehorse.in>
 * @copyright  Copyright (c) 2015 , Bh (http://www.bluehorse.in)
 */
require_once Mage::getModuleDir('controllers', 'Mage_Customer').DS.'AccountController.php';
class Bluehorse_Ajaxlogin_AccountController extends Mage_Customer_AccountController{

   /*   Add new Action 
    */
    protected $_cookieCheckActions = array('loginPost', 'createpost','ajaxLoginPost','ajaxcreatepost');
   protected $defaultOpenActionList=
        array(
            'create',
            'login',
            'logoutsuccess',
            'forgotpassword',
            'forgotpasswordpost',
            'resetpassword',
            'resetpasswordpost',
            'confirm',
            'confirmation',
           'loginPost', 
           'createpost'
        );
    
    protected  $newOpenActionList= array(
            'ajaxloginPost',
            'ajaxforgotpasswordpost',
			'ajaxcreatepost'
        );
   
            


    /* Check customer authentication for some actions */
    public function preDispatch() {
        
         $currenAction=$this->getRequest()->getActionName();
        
        $pattern = '/^(' . implode('|', $this->newOpenActionList) . ')/i';
       
        if (preg_match($pattern, $currenAction)):
             
            $TempAction=  $this->getRequest()->setActionName('login');
         endif;
        
         parent::preDispatch();
        
       if($currenAction!=$this->getRequest()->getActionName()){
            $this->getRequest()->setActionName($currenAction);
        }
        
        if(!$this->getRequest()->isDispatched()){
            return;
        }
        
        if (!preg_match('/^('.$this->_getValidActions().')/i', $currenAction)) {
           
             if (!$this->_getSession()->authenticate($this)) {
              $this->setFlag('', 'no-dispatch', true);
             }
        } else {
            
             $this->_getSession()->setNoReferer(true);
        }

     }
     protected function _getValidActions(){
	  return implode("|", array_merge($this->defaultOpenActionList, $this->newOpenActionList));
	  }
    public function ajaxLoginPostAction(){
        $session = $this->_getSession();

        $result = array();
   
        /* If Request is not ajax then redirect to login page */
        if (!$this->getRequest()->isXmlHttpRequest()) {
             $this->_redirect('*/*/');
            return;
        }
        
        if (!$this->_validateFormKey()) {
                $result['success'] = 0;
                $result['error'] = $this->_getHelper('customer')->__('Invalid form key.');
              //  Mage::throwException('Invalid form key');
                $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
             return;
        }

            if ($this->_getSession()->isLoggedIn()) {
				$result['success'] = 1;
				$result['message'] = $this->__('Already loggedin,please refresh your page');
				$this->getResponse()->clearHeaders();
				$this->getResponse()->setHeader('Content-type','application/json',true);
				$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
				return;
            }

        if ($this->getRequest()->isPost()) {
           
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $session->login($login['username'], $login['password']);
                    if ($session->getCustomer()->getIsJustConfirmed()) {
						
                        $result=$this->_AjaxwelcomeCustomer($session->getCustomer(), true);
                    }else{
                       $result['success'] = 1;
                       $result['message'] = $this->__('Successfully loggedin,Please wait');
                        
                    }
                      
                } catch (Mage_Core_Exception $e) {
                   
                    
                  
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $value = $this->_getHelper('customer')->getEmailConfirmationUrl($login['username']);
                            $message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $session->setUsername($login['username']);
					$result['success'] = 0;
					$result['error'] =$message;

                } catch (Exception $e) {
                     
                    // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                $result['success'] = 0;
                $result['error'] =$e->getMessage();

                }
            } else {
                  
                $result['success'] = 0;
                $result['error'] =$this->__('Login and password are required.');
            }
        }
   
        $this->getResponse()->clearHeaders();
        $this->getResponse()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
       
        }
  

    protected function _AjaxwelcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
    {
        

        $result=array();

        $result['success'] = 1;
        $result['message'] = $this->__('Thank you for registering with %s.', Mage::app()->getStore()->getFrontendName());

        if ($this->_isVatValidationEnabled()) {
            // Show corresponding VAT message to customer
            $configAddressType =  $this->_getHelper('customer/address')->getTaxCalculationAddressType();
            $userPrompt = '';
            switch ($configAddressType) {
                case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation',
                        $this->_getUrl('customer/address/edit'));
                    break;
                default:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation',
                        $this->_getUrl('customer/address/edit'));
            }
			
			$result['success'] = 1;
			$result['message'] = $userPrompt;
        }

        $customer->sendNewAccountEmail(
            $isJustConfirmed ? 'confirmed' : 'registered',
            '',
            Mage::app()->getStore()->getId()
        );

        return $result;
    }
    public function ajaxforgotpasswordpostAction()
    {
        $result = array();
   
        /* If Request is not ajax then redirect to login page */
        if (!$this->getRequest()->isXmlHttpRequest()) {
             $this->_redirect('*/*/');
            return;
        }
        $email = (string) $this->getRequest()->getParam('email');
        if ($email) {
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                
                $result['error'] = $this->__('Invalid email address.');
                $result['success'] = 0;
                $this->getResponse()->clearHeaders();
                $this->getResponse()->setHeader('Content-type','application/json',true);
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
             return;
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken =  $this->_getHelper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                } catch (Exception $exception) {
 
                    $result['error'] = $exception->getMessage();
                    $result['success'] = 0;
                    $this->getResponse()->clearHeaders();
                    $this->getResponse()->setHeader('Content-type','application/json',true);
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));                    
                    return;
                }
            }

            $result['success'] = 1;
            $result['message'] = $this->_getHelper('customer')
                ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                    $this->_getHelper('customer')->escapeHtml($email));
        } else {

            $result['error'] = $this->__('Please enter your email.');
            $result['success'] = 0;            
            
        }
        $this->getResponse()->clearHeaders();
        $this->getResponse()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));                    

    }
	
	    /**
     * Create customer account action
     */
    public function ajaxcreatepostAction()
    {
        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        $result = array();
        /* If Request is not ajax then redirect to login page */
        if (!$this->getRequest()->isXmlHttpRequest()) {
             $this->_redirect('*/*/');
            return;
        }
        if ($session->isLoggedIn()) {
				$result['success'] = 1;
				$result['message'] = $this->__('Already logged in,please refresh your page');
				$this->getResponse()->clearHeaders();
				$this->getResponse()->setHeader('Content-type','application/json',true);
				$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
				return;
        }
        $session->setEscapeMessages(true); // prevent XSS injection in user input
        if (!$this->getRequest()->isPost()) {
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
            return;
        }

        $customer = $this->_getCustomer();

        try {
            $errors = $this->_getCustomerErrors($customer);

            if (empty($errors)) {
                $customer->cleanPasswordsValidationData();
                $customer->save();
                $this->_dispatchRegisterSuccess($customer);
             	$result=$this->_AjaxsuccessProcessRegistration($customer);
				$this->getResponse()->clearHeaders();
				$this->getResponse()->setHeader('Content-type','application/json',true);
				$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));                    
				
                return;
            } else {
              //  $this->_addSessionError($errors);
					foreach ($errors as $message) {
						if ($message) {
						// Escape HTML entities in quote message to prevent XSS
						$messages =$messages. Mage::helper('core')->escapeHtml($message).'\n';
						}
					}

				$result['error'] = $messages ;
				$result['success'] = 0;
				$this->getResponse()->clearHeaders();
				$this->getResponse()->setHeader('Content-type','application/json',true);
				$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));                    
                return;
				

            }
        } catch (Mage_Core_Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $url = $this->_getUrl('customer/account/forgotpassword');
                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                    $result['error'] = $message ;
                    $result['success'] = 0;
            } else {
                $message = $e->getMessage();
            }
			$result['error'] = $message ;
			$result['success'] = 0;
        } catch (Exception $e) {
			$result['error'] = $this->__('Cannot save the customer.') ;
			$result['success'] = 0;
			
        }
        $this->getResponse()->clearHeaders();
        $this->getResponse()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));                    
		
    }
	    protected function _AjaxsuccessProcessRegistration(Mage_Customer_Model_Customer $customer)
    {
        $session = $this->_getSession();
        if ($customer->isConfirmationRequired()) {
            /** @var $app Mage_Core_Model_App */
            $app = $this->_getApp();
            /** @var $store  Mage_Core_Model_Store*/
            $store = $app->getStore();
            $customer->sendNewAccountEmail(
                'confirmation',
                $session->getBeforeAuthUrl(),
                $store->getId()
            );
            $customerHelper = $this->_getHelper('customer');
				$result['success'] = 1;
				$result['message'] = $this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.',
                $customerHelper->getEmailConfirmationUrl($customer->getEmail()));
        } else {
            $session->setCustomerAsLoggedIn($customer);
            $result = $this->_AjaxwelcomeCustomer($customer);
        }
        return $result;
    }
   // protected function _AjaxwelcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
//    {
//		$result=array();
//		$result['success'] = 1;
//		$message = $this->__('Thank you for registering with %s.', Mage::app()->getStore()->getFrontendName());		
//        if ($this->_isVatValidationEnabled()) {
//            // Show corresponding VAT message to customer
//            $configAddressType =  $this->_getHelper('customer/address')->getTaxCalculationAddressType();
//            $userPrompt = '';
//            switch ($configAddressType) {
//                case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
//                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation',
//                        $this->_getUrl('customer/address/edit'));
//                    break;
//                default:
//                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation',
//                        $this->_getUrl('customer/address/edit'));
//            }
//			$message=$message.'.'.$message;
//;        }
//
//        $customer->sendNewAccountEmail(
//            $isJustConfirmed ? 'confirmed' : 'registered',
//            '',
//            Mage::app()->getStore()->getId()
//        );
//
//;		$result['message']=$message;
//        return $result;
//    }	
//

 

}

