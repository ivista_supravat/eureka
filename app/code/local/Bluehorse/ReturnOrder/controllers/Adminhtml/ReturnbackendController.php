<?php
/* @copyright   Copyright (c) 2015 BlueHorse */ 
class Bluehorse_ReturnOrder_Adminhtml_ReturnbackendController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Return"));
	   $this->renderLayout();
    }
}