<?php

/* @copyright   Copyright (c) 2015 BlueHorse */

class Bluehorse_ReturnOrder_IndexController extends Mage_Core_Controller_Front_Action {

    public function IndexAction() {

        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {

            $this->loadLayout();

            $this->getLayout()->getBlock("head")->setTitle($this->__("Return"));

            $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");

            $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link" => Mage::getBaseUrl()
            ));



            $breadcrumbs->addCrumb("return", array(
                "label" => $this->__("Return"),
                "title" => $this->__("Return")
            ));



            $this->_initLayoutMessages('customer/session');

            $this->_initLayoutMessages('catalog/session');

            $this->renderLayout();
        } else {

            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account'));
        }
    }

    public function postAction() {

        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {

            if ($this->getRequest()->getPost()) {

                try {

                    $postData = $this->getRequest()->getPost();



                    $createdate = now();

                    if (isset($_FILES)) {

                        if ($_FILES['image']['name']) {

                            $io = new Varien_Io_File();

                            $path = Mage::getBaseDir('media') . DS . 'return' . DS . 'return' . DS;

                            $uploader = new Varien_File_Uploader('image');

                            $uploader->setAllowedExtensions(array('jpg', 'png', 'gif'));

                            $uploader->setAllowRenameFiles(true);

                            $uploader->setFilesDispersion(false);

                            $destFile = $path . $_FILES['image']['name'];

                            $filename = $uploader->getNewFileName($destFile);

                            $uploader->save($path, $filename);

                            $post_data_image = 'return/return/' . $filename;
                        }
                    }



                    $Model = Mage::getModel('returnorder/return');

                    $Model->setOrderId($postData['order_id'])
                            ->setName($postData['name'])
                            ->setEmail($postData['email'])
                            ->setPhone($postData['phone'])
                            ->setReason($postData['reason'])
                            ->setImage($post_data_image)
                            ->setStatus(0)
                            ->setReturnItem(implode(',', $postData['returnorder_item']))
                            ->setRequestDate($createdate)
                            ->save();
                    $order_id = $postData['order_id']; //use your own order id 

                    $order = Mage::getModel("sales/order")->loadByIncrementId($order_id); //load order by order id 
                    if ($postData['return_type'] == 'return') {
                        $order->setStatus("return_request");
                    } else {
                        $order->setStatus("exchange_request");
                    }
                    $history = $order->addStatusHistoryComment('Order Return request by '.$postData['name'].'', false);
                    $history->setIsCustomerNotified(true);
                    $order->save();

                    $orderItems = $order->getItemsCollection();
                foreach ($orderItems as $item){
                    $_product = Mage::getModel('catalog/product');
                    $_product ->setStoreId($item->getOrder()->getStoreId());
                    $imageurl = (string)Mage::helper('catalog/image')->init($_product, 'image');
                    $product_price = intval($item->getPrice());
                    $product_name = $item->getName();
                    $itemqty = intval($item->getQtyOrdered());
                    $html1 = '<tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border:#CCC solid thin;"><tbody><tr><td style="  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color: #868686;"><table width="100%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tbody><tr><td><img src="'.$imageurl.'" alt="" width="49" height="75"></td><td class="aur-text" style="color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600;">'.$product_name.'</td></tr></tbody></table></td><td style=" border-left:1px solid #ddd;  padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:50px; text-align:center;">'.$itemqty.'</td><td class="aur-text" style=" border-left:1px solid #ddd;  padding: 8px; font-family: \'Open Sans\', sans-serif; color:#1656b0; font-family: \'Open Sans\', sans-serif; font-weight:600; text-align:center">RS.'.$product_price.'/</td></tr>';
                }   
                $ordersubtotal = intval($order->getSubtotal());
                $discount = intval($order->getDiscountAmount());
                $grandtotal = intval($order->getGrandTotal());   
                $html = '<table class="body-top" border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:15px 0px 0px 5px;"><tbody><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF;"><tbody><tr><td style="width:60%; font-family: \'Open Sans\', sans-serif; font-size:13px;  color:#757575; font-weight:50%;">PRODUCT</td><td style="width:15%; padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px;  text-align:center; color:#757575; font-weight:600;">QTY</td><td style="width:20%; padding: 8px;; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#757575; font-weight:600;">PRICE</td></tr></tbody></table></td></tr></tbody></table><table class="body-top" border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:0px 0px;"><tbody>'.$html1.' </tbody></table></td></tr></tbody></table><table class="body-top" border="0" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; padding:5px 0px 0px 5px;"><tbody><tr><td><table width="93%" cellspacing="0" cellpadding="0" style=" margin:auto; background:#FFF; border-bottom:#dbdbdb solid 1px; padding-bottom:10px;"><tbody><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:50%; font-weight:500; color: #868686;">Paid By<span style=" color:#4f4f4f; font-weight:600"> : Credit Card</span></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; font-weight:500; color: #6e6e6e;">Item Subtotal :</td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6e6e6e; font-weight:500; float:right; ">RS.'.$ordersubtotal.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#757575; font-weight:500; line-height:2px;">Discount:</td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; color:#757575; font-weight:500; float:right; line-height:2px; float:right">RS.'.$discount.'</td></tr><tr><td style=" font-family: \'Open Sans\', sans-serif; font-size:13px; width:40%; color:#757575; font-weight:500;"></td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; width:30%; color:#6d6d6d; font-weight:bold; line-height:30px;">Order Total:</td><td style=" padding: 8px; font-family: \'Open Sans\', sans-serif; font-size:13px; text-align:center; color:#6d6d6d; font-weight:bold; float:right; line-height:30px;">Rs.'.$grandtotal.'</td></tr></tbody></table></td></tr></tbody></table>';
                
                $useremail = $postData['email']; 
                $Username = $postData['name'];
                $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                $emailTemp = Mage::getModel('core/email_template')->load(6);
                $emailTempVariables = array();
                $adminUsername = 'Admin';
                $emailTempVariables['orderid'] = $order_id;
                $emailTempVariables['orderdate'] =  $order->getCreatedAt();
                $emailTempVariables['cancelationapprovedate'] =  time();
                $emailTempVariables['phoneno'] =  $postData['phone'];
                $emailTempVariables['name'] =  $postData['name'];
                $emailTempVariables['reason'] =  $postData['reason'];
                $emailTempVariables['emailid'] = $postData['email'];
                $emailTempVariables['html'] = $html;
                $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
                /* Send mail to Support */
                $emailTemp->setSenderName($Username);
                $emailTemp->setSenderEmail($useremail);
                $emailTemp->send($adminEmail,$adminUsername,$processedTemplate);
                /* Send mail to Support */

                /* Send mail coy to Customer */
                $emailTemp->setSenderName($adminUsername);
                $emailTemp->setSenderEmail($adminEmail);
                $emailTemp->send($useremail,$Username,$emailTempVariables);
                /* Send mail coy to Customer */

                    Mage::getSingleton('customer/session')->addSuccess(Mage::helper('returnorder')->__('Return Request was successfully send'));

                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('sales/order/history/'));

                    return;
                } catch (Exception $e) {

                    Mage::getSingleton('customer/session')->addError($e->getMessage());

                    Mage::getSingleton('customer/session')->setReturnData($this->getRequest()->getPost());

                    $this->_redirect('*/*/', array('id' => $this->getRequest()->getParam('id')));

                    return;
                }
            }

            $this->_redirect('sales/order/history/');
        } else {

            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account'));
        }
    }

    public function acceptAction() {

        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {

            if ($this->getRequest()->getPost()) {

                try {

                    $postData = $this->getRequest()->getPost();

                    $order_id = $postData['order_id']; //use your own order id 

                    $order = Mage::getModel("sales/order")->loadByIncrementId($order_id); //load order by order id 
                    //echo '<pre>'; print_r($order);exit;

                    $order->setStatus($postData['returnstatus']);

                    $order->save();

                    $col = Mage::getModel('returnorder/return')->getCollection()->addFieldToFilter('order_id', $postData['order_id'])->getFirstItem();
                    if ($postData['returnstatus'] == 'return_initiated') {
                        $col->setStatus(2);
                    } else if ($postData['returnstatus'] == 'return_rejected') {
                        $col->setStatus(1);
                    } else if ($postData['returnstatus'] == 'return_completed') {
                        $col->setStatus(3);
                    }
                    $col->save();



                    Mage::getSingleton('customer/session')->addSuccess(Mage::helper('returnorder')->__('Return Request was successfully accepted.'));

                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

                    return;
                } catch (Exception $e) {

                    Mage::getSingleton('customer/session')->addError($e->getMessage());

                    Mage::getSingleton('customer/session')->setReturnData($this->getRequest()->getPost());

                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

                    return;
                }
            }

            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

            return;
        } else {

            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account'));
        }
    }

    public function returnhtmlAction() {

        $this->_initLayoutMessages('catalog/session');

        $this->getResponse()->setBody($this->getLayout()->createBlock('returnorder/index')->setTemplate('return/index.phtml')->toHtml());
    }

    public function rejectAction() {

        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {

            if ($this->getRequest()->getPost()) {

                try {

                    $postData = $this->getRequest()->getPost();

                    $col = Mage::getModel('returnorder/return')->getCollection()->addFieldToFilter('order_id', $postData['order_id'])->getFirstItem();

                    $returnId = $col->getReturnId();

                    $Model = Mage::getModel('returnorder/return')->load($returnId);

                    $Model->setSellerComment($postData['reason'])
                            ->setStatus(1)
                            ->save();



                    $order_id = $postData['order_id']; //use your own order id 

                    $order = Mage::getModel("sales/order")->loadByIncrementId($order_id); //load order by order id 

                    $order->setStatus("return_rejected ");

                    $order->save();



                    Mage::getSingleton('customer/session')->addSuccess(Mage::helper('returnorder')->__('Return was successfully rejected.'));

                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

                    return;
                } catch (Exception $e) {

                    Mage::getSingleton('customer/session')->addError($e->getMessage());

                    Mage::getSingleton('customer/session')->setReturnData($this->getRequest()->getPost());

                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

                    return;
                }
            }

            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('marketplace/marketplaceaccount/myorderhistory/'));

            return;
        } else {

            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account'));
        }
    }

}
