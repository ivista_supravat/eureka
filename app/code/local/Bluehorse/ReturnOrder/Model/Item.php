<?php
/* @copyright   Copyright (c) 2015 BlueHorse */ 
class Bluehorse_ReturnOrder_Model_Item extends Varien_Object
{
  public function getValues($id)
    {
	 $col=Mage::getModel('returnorder/return')->load($id);
     $order_id = $col->getOrderId(); //use your own order id 
	 $return_item=explode(',',$col->getReturnItem());
     $order = Mage::getModel("sales/order")->loadByIncrementId($order_id); //load order by order id 
	 $items = $order->getAllVisibleItems();
        $arr = array();
                foreach( $items as $item){
					  if(in_array($item->getItemId(),$return_item)){
                        $arr[] = array('label' => $item->getName(), 'value' => $item->getItemId());
					  }
                 }
        return $arr;
	}

}