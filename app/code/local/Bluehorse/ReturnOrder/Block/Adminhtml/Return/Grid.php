<?php
/* @copyright   Copyright (c) 2015 BlueHorse */ 
class Bluehorse_ReturnOrder_Block_Adminhtml_Return_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("returnGrid");
				$this->setDefaultSort("return_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("returnorder/return")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("return_id", array(
				"header" => Mage::helper("returnorder")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "return_id",
				));
                
				$this->addColumn("order_id", array(
				"header" => Mage::helper("returnorder")->__("Order ID"),
				"index" => "order_id",
				));
				$this->addColumn("name", array(
				"header" => Mage::helper("returnorder")->__("Name"),
				"index" => "name",
				));
				$this->addColumn("email", array(
				"header" => Mage::helper("returnorder")->__("Email ID"),
				"index" => "email",
				));
				$this->addColumn("phone", array(
				"header" => Mage::helper("returnorder")->__("Phone No."),
				"index" => "phone",
				));
						$this->addColumn('status', array(
						'header' => Mage::helper('returnorder')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Bluehorse_ReturnOrder_Block_Adminhtml_Return_Grid::getOptionArray6(),				
						));
						
					$this->addColumn('request_date', array(
						'header'    => Mage::helper('returnorder')->__('Request Date'),
						'index'     => 'request_date',
						'type'      => 'datetime',
					));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('return_id');
			$this->getMassactionBlock()->setFormFieldName('return_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_return', array(
					 'label'=> Mage::helper('returnorder')->__('Remove Return'),
					 'url'  => $this->getUrl('*/adminhtml_return/massRemove'),
					 'confirm' => Mage::helper('returnorder')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray6()
		{
            $data_array=array(); 
			$data_array[0]='Return Requested';
			$data_array[1]='Return Rejected';
			$data_array[2]='Return Initiated';
			$data_array[3]='Return Completed';
            return($data_array);
		}
		static public function getValueArray6()
		{
            $data_array=array();
			foreach(Bluehorse_ReturnOrder_Block_Adminhtml_Return_Grid::getOptionArray6() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}