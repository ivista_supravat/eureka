<?php
/* @copyright   Copyright (c) 2015 BlueHorse */ 	
class Bluehorse_ReturnOrder_Block_Adminhtml_Return_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "return_id";
				$this->_blockGroup = "returnorder";
				$this->_controller = "adminhtml_return";
				//$this->_updateButton("save", "label", Mage::helper("return")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("returnorder")->__("Delete Item"));
				$this->_removeButton('save');

/*				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("return")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);
*/


				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("return_data") && Mage::registry("return_data")->getId() ){

				    return Mage::helper("returnorder")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("return_data")->getId()));

				} 
				else{

				     return Mage::helper("returnorder")->__("Add Item");

				}
		}
}