<?php
/* @copyright   Copyright (c) 2015 BlueHorse */ 
class Bluehorse_ReturnOrder_Block_Adminhtml_Return_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{
                $orderid=$this->getRequest()->getParam('id');
				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("returnorder_form", array("legend"=>Mage::helper("returnorder")->__("Item information")));
				
						$fieldset->addField("order_id", "text", array(
						"label" => Mage::helper("returnorder")->__("Order ID"),
						"name" => "order_id",
						));
					    $fieldset->addField('returnorder_item', 'multiselect', array(
					    'name' => 'returnorder_item[]',
					    'label' => Mage::helper('returnorder')->__('Return Item(s)'),
					    'title' => Mage::helper('returnorder')->__('Return Item(s)'),
					    'required' => true,
					    'values' => Mage::getSingleton('returnorder/item')->getValues($orderid),
			             ));
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("returnorder")->__("Name"),
						"name" => "name",
						));
					
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("returnorder")->__("Email ID"),
						"name" => "email",
						));
					
						$fieldset->addField("phone", "text", array(
						"label" => Mage::helper("returnorder")->__("Phone No."),
						"name" => "phone",
						));
					
						$fieldset->addField("reason", "textarea", array(
						"label" => Mage::helper("returnorder")->__("Reason"),
						"name" => "reason",
						));
									
						$fieldset->addField('image', 'image', array(
						'label' => Mage::helper('returnorder')->__('Image'),
						'name' => 'image',
						'note' => '(*.jpg, *.png, *.gif)',
						));				
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('returnorder')->__('Status'),
						'values'   => Bluehorse_ReturnOrder_Block_Adminhtml_Return_Grid::getValueArray6(),
						'name' => 'status',
						));
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('request_date', 'date', array(
						'label'        => Mage::helper('returnorder')->__('Request Date'),
						'name'         => 'request_date',
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));

				if (Mage::getSingleton("adminhtml/session")->getReturnData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getReturnData());
					Mage::getSingleton("adminhtml/session")->setReturnData(null);
				} 
				elseif(Mage::registry("return_data")) {
				    $form->setValues(Mage::registry("return_data")->getData());
				}
				return parent::_prepareForm();
		}
}
