<?php
/* @copyright   Copyright (c) 2015 BlueHorse */ 
class Bluehorse_ReturnOrder_Block_Adminhtml_Return extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_return";
	$this->_blockGroup = "returnorder";
	$this->_headerText = Mage::helper("returnorder")->__("Return Manager");
	//$this->_addButtonLabel = Mage::helper("return")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');

	
	}

}