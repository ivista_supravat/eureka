<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
        $this->getTable('return'), //table name
        'seller_id',      //column name
        'varchar(50) default NULL'  //
        );
$installer->endSetup(); 
