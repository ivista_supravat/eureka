<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
        $this->getTable('return'), //table name
        'return_item',      //column name
        'varchar(500) default NULL'  //
        );
$installer->endSetup(); 
