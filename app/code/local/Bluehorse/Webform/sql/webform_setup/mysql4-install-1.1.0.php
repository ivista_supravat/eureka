<?php

$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('bh_webform'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
            'identity' => true,
                ), 'Webform ID')
        ->addColumn('formname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
            'default' => '',
                ), 'Form Name')
        ->addColumn('allfields', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
            'default' => '',
                ), 'allfields')
        ->addColumn('mailsent', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0',
                ), 'Status of Sent mail')
        ->addColumn('contactyet', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0',
                ), 'contact yet')
        ->addColumn('adminfeedback', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
            'default' => '',
                ), 'adminfeedback')
        ->addColumn('created_time', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
            'nullable' => true,
            'default' => null,
                ), 'Created Date')
        ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
            'nullable' => true,
            'default' => null,
                ), 'Update Date')
        ->setComment('Webform table');

$installer->getConnection()->createTable($table);
$installer->endSetup();
?>
