<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

class Bluehorse_Webform_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function becomeadelerAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function becomeasalesfranchiseeAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function becomebusinessassociateAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function bookademoAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function packagedrinkingwaterAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function placeabulkenqueryAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

}
