<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

class Bluehorse_Webform_Adminhtml_WebformController extends Mage_Adminhtml_Controller_Action {

    /**
     * Initialize layout.
     *
     * @return Bluehorse_Webform_Adminhtml_ExportwebformController
     */
    protected function _initAction() {
        $this->_title($this->__('Retails Store Management'))
                ->loadLayout()
                ->_setActiveMenu('Webform/Webform');

        return $this;
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('Webform/items');
    }

    public function indexAction() {
        $this->loadLayout();
        $myblock = $this->getLayout()->createBlock('Webform/adminhtml_Webform');
        $this->_addContent($myblock);
        $this->renderLayout();
    }

    public function productAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('Webform.edit.tab.product')
                ->setWareId($this->getRequest()->getPost('id', null));
        $this->renderLayout();
    }

    public function productGridAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('Webform.edit.tab.product')
                ->setWareId($this->getRequest()->getPost('id', null));
        $this->renderLayout();
    }

    public function exportCsvAction() {

        $rows = [];

        $collection = Mage::getModel('Webform/Webform')->getCollection();

        $admin_user_session = Mage::getSingleton('admin/session');
        $adminUsername = $admin_user_session->getUser()->getUsername();
        $role_data = Mage::getModel('admin/user')->load($adminId)->getRole()->getData();
        $collection = Mage::getModel('Webform/Webform')->getCollection();
        if ($role_data['role_id'] == 8) {
            $collection->addFieldToFilter('username', $adminUsername);
        }


        foreach ($collection as $WebformItem) {
            //$rows[$i]['Webform_id'] = $WebformItem->getId();
            $zipcodes = Mage::getModel('Webform/zipcode')->getCollection()
                    ->addFieldToFilter('wid', $WebformItem->getId());
            foreach ($zipcodes as $item) {
                $zip = $item->getData();
                //$rows[$i]['zipcode'] = $zip['zipcode'];

                $products = Mage::getModel('Webform/product')->getCollection()
                        ->addFieldToFilter('zipcode_id', $zip['id']);
                $j = 1;
                $rows[0][0] = 'Webform_id';
                $rows[0][1] = 'username';

                $rows[0][2] = 'associate_zipcode';
                $rows[0][3] = 'is_primary';
                $rows[0][4] = 'delivery_time';

                $rows[0][5] = 'product_sku';
                $rows[0][6] = 'product_qty';

                foreach ($products as $product) {
                    
                    $pro = $product->getData();
                    $rows[$j][] = $WebformItem->getId();
                    $rows[$j][] = $WebformItem->getUsername();

                    $rows[$j][] = $zip['zipcode'];
                    $rows[$j][] = $zip['is_primary'];
                    $rows[$j][] = $zip['delivery_time'];


                    $rows[$j][] = $pro['product_sku'];
                    $rows[$j][] = $pro['product_qty'];
                    $j++;
                }
            }
            $i++;
        }

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=Webform.csv");
        header("Pragma: no-cache");
        header("Expires: 0");


        $this->outputCSV($rows);
        //die;
        //$content = $this->_generateCsv($rows);
        //var_dump($str);
        //$content = $this->getLayout()->createBlock('Webform/adminhtml_Webform_grid')->getCsv();
        //var_dump($content);
        //die;
        //$this->_sendUploadResponse($fileName, $content);
    }

    function outputCSV($data) {
        $outputBuffer = fopen("php://output", 'w');
        foreach ($data as $val) {
            fputcsv($outputBuffer, $val);
        }
        fclose($outputBuffer);
    }

    function _generateCsv($data, $delimiter = ',', $enclosure = '"') {
        $handle = fopen('php://temp', 'r+');
        foreach ($data as $line) {
            fputcsv($handle, $line, $delimiter, $enclosure);
        }
        rewind($handle);
        while (!feof($handle)) {
            $contents .= fread($handle, 8192);
        }
        fclose($handle);
        return $contents;
    }

    public function exportExcelAction() {

        $fileName = 'Webform-' . date('d-m-Y-H-i-s');
        $rows = [];
        $WebformItems = Mage::getModel('Webform/Webform')->getCollection();

        foreach ($WebformItems as $WebformItem) {
            //$rows[$i]['Webform_id'] = $WebformItem->getId();
            $zipcodes = Mage::getModel('Webform/zipcode')->getCollection()
                    ->addFieldToFilter('wid', $WebformItem->getId());
            foreach ($zipcodes as $item) {
                $zip = $item->getData();
                //$rows[$i]['zipcode'] = $zip['zipcode'];

                $products = Mage::getModel('Webform/product')->getCollection()
                        ->addFieldToFilter('zipcode_id', $zip['id']);
                $j = 1;
                $rows[0][0] = 'Webform_id';
                $rows[0][1] = 'associate_zipcode';
                $rows[0][2] = 'associate_product';
                $rows[0][3] = 'associate_product_qty';

                foreach ($products as $product) {
                    $pro = $product->getData();
                    $rows[$j][] = $WebformItem->getId();
                    $rows[$j][] = $zip['zipcode'];
                    $rows[$j][] = $pro['product_sku'];
                    $rows[$j][] = $pro['product_qty'];
                    $j++;
                }
            }
            $i++;
        }

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}.xls");
        header("Pragma: no-cache");
        header("Expires: 0");


        $this->outputCSV($rows);
        //$fileName = 'Webform-' . date('d-m-Y-H-i-s') . '.xls';
        // $content = $this->getLayout()->createBlock('Webform/adminhtml_Webform_grid')->getExcelFile();
        //$this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export csv and xml
     *
     */
    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

}
