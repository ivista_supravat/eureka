<?php

class Bluehorse_Sendy_Model_Observer {

    public function __contruct() {
        
    }

   public function handleSubscriber(Varien_Event_Observer $observer) {
 $routeName = Mage::app()->getRequest()->getRouteName(); 
    if($routeName == 'newsletter' ) {
        //-------------------------- You need to set these --------------------------//
	$your_installation_url = Mage::getStoreConfig('sendysms/enablesendy/installationurl'); //Your Sendy installation (without the trailing slash)
        $newsletetrlist = Mage::getStoreConfig('sendysms/enablesendy/subscriberlistid');
	$list = $newsletetrlist; //Can be retrieved from "View all lists" page
	$success_url = Mage::getBaseUrl(); //URL user will be redirected to if successfully subscribed
	$fail_url = Mage::getBaseUrl(); //URL user will be redirected to if subscribing fails
	//---------------------------------------------------------------------------//

	//POST variables
	$name = 'Guest';
	$email = $_POST['email'];
	$boolean = 'true';
	
	//Check fields
	if($name=='' || $email=='')
	{
		echo 'Please fill in all fields.';
		exit;
	}
	
	//Subscribe
	$postdata = http_build_query(
	    array(
	    'name' => $name,
	    'email' => $email,
	    'list' => $list,
	    'boolean' => 'true'
	    )
	);
	$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
	$context  = stream_context_create($opts);
	$result = file_get_contents($your_installation_url.'/subscribe', false, $context);
	if($result)
		header("Location: $success_url");
	else
		header("Location: $fail_url");
        
    }
    
   }
}