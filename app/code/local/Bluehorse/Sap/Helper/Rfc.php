<?php

class Bluehorse_Sap_Helper_Rfc extends Mage_Core_Helper_Abstract {

    public function getLogon() {
        $conn = array(
            "ASHOST" => Mage::getSingleton('sap/config')->getSaprfcAshost(),
            "SYSNR" => Mage::getSingleton('sap/config')->getSaprfcSysnr(),
            "CLIENT" => Mage::getSingleton('sap/config')->getSaprfcClient(),
            "USER" => Mage::getSingleton('sap/config')->getSaprfcUser(),
            "PASSWD" => Mage::getSingleton('sap/config')->getSaprfcPasswd(),
            "LANG" => "EN"
        );
        return $conn;
    }

}
