<?php


class Bluehorse_Sap_Helper_Data extends Mage_Core_Helper_Abstract {

    public function PaymentType($code) {

        $response = '';
        if (!empty($code)) {
            $response = '';
            switch ($code) {
                case 'free':
                    $response = 'FREE';
                    break;
                case 'cashondelivery':
                    $response = 'C';
                    break;
                case 'paypal_billing_agreement':
                    $response = 'FF';
                    break;
                case 'payubiz':
                    $response = 'FF';
                    break;
                default:
                    $response = 'c';
            }
        }
        return $response;
    }

    public function getRegionCode() {
        
    }
    
    
    public function getLoginUrl() {
		
		return Mage::getUrl('customer-service/amc/login');
		
	}

}
