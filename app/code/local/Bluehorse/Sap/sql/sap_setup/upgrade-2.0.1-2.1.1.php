<?php

$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()
        ->newTable($installer->getTable('sap/emaillog'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'email')
        ->addColumn('hash', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'hash')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'start date')
        ->setComment('saprfc email logs');
$installer->getConnection()->createTable($table);
$installer->endSetup();

