<?php

$installer = $this;
$installer->startSetup();


$installer->getConnection()
->addColumn($installer->getTable('sales/order'),'sold_to_party', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_BIGINT,
    'nullable'  => false,
    'length'    => 100,
    'after'     => 'increment_id',
    'comment'   => 'Sold customer id'
    ));
    
$installer->getConnection()->addColumn($installer->getTable('sales/order'),'ship_to_party', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_BIGINT,
    'nullable'  => false,
    'length'    => 100,
    'after'     => 'increment_id',
    'comment'   => 'Ship id'
    ));
    
$installer->getConnection()->addColumn($installer->getTable('sales/order'),'sap_id', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => false,
    'length'    => 255,
    'after'     => 'increment_id',
    'comment'   => 'sap order id'
    ));  
 
$installer->endSetup();

?>
