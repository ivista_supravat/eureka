<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'name', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'Full Name Customer'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'landline', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => "Customer's Landline number"
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'email', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'Email Address'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'mobile', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'Mobile no'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'address', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'Customer address'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'pincode', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'Pincode'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'service_prod_name', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'amc name'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'service_prod_price', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'amc original price'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'contract_duration', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'Contract Number'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'grand_total', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'Grand Total'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'service_prod_price', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'amc original price'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'service_prod_price', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'amc original price'
       ]
);
$installer->getConnection()->addColumn(
        $installer->getTable('sap/webamc'), 'service_prod_price', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => false,
        'length' => 255,
        'after' => 'customer_id',
        'comment' => 'amc original price'
       ]
);

$installer->endSetup();

