<?php

$installer = $this;

$installer->startSetup();


$table = $installer->getConnection()
        ->newTable($installer->getTable('sap/webamc'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('customer_code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'I_CUSTOMER')
        ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'magento customer id')
        
        ->addColumn('component', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'component')
        ->addColumn('quasarid', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'quasarid')
        ->addColumn('main_prod', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'main_prod')
        ->addColumn('service_prod', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'service_prod')
        ->addColumn('exec_partner', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'exec_partner')
        ->addColumn('discount', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'discount')
        ->addColumn('e_contract', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'e_contract')
        ->addColumn('e_proc_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'e_proc_type')
        ->addColumn('e_flag', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'e_flag')
        ->addColumn('e_iccrno', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'e_iccrno')
        ->addColumn('start_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                ), 'start date')
        ->addColumn('start_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                ), 'start date')
        ->addColumn('start_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'start date')
        
        
        ->setComment('Articles table');
$installer->getConnection()->createTable($table);
$installer->endSetup();

