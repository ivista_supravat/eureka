<?php

class Bluehorse_Sap_Block_Amc extends Mage_Core_Block_Template {

    public function getFormAction() {

        return $this->getUrl('customer-service/amc/details');
    }

    public function getOptionFormUrl() {

        return $this->getUrl('customer-service/amc/options');
    }

    public function getPaymentFormUrl() {
        return $this->getUrl('customer-service/amc/payment');
    }

    public function getMakePaymentFormUrl() {
        return $this->getUrl('customer-service/amc/makepayment');
    }

    public function getAmcComponent() {
        return Mage::getSingleton('customer/session')->getData('customer_comp');
    }

    public function getAMCOptions() {

        return Mage::getSingleton('sap/amc')->getOptoins();
    }

    public function getLoginPostUrl() {
        return $this->getUrl('customer-service/amc/loginPost');
    }

    public function getDetailsUrl() {
        return $this->getUrl('customer-service/amc/details');
    }

    public function getWebleadFormUrl() {
        return Mage::getUrl('customer-service/amc/webleadPost');
    }

    public function getCustomerAMCHistory() {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $customerId = $customer->getId();            
            $amcObject = Mage::getModel('sap/webamc')->getCollection();
            $amcObject->addFieldToFilter('customer_id', $customerId);
        }
        return $amcObject;
    }

}
