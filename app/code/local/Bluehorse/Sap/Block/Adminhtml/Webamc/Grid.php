<?php

class Bluehorse_Sap_Block_Adminhtml_Webamc_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {

        parent::__construct();
        $this->setId('webamcGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('sap/webamc')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('id', array(
            'header' => Mage::helper('wms')->__('ID #'),
            'align' => 'left',
            'index' => 'id',
        ));

        $this->addColumn('customer_code', array(
            'header' => Mage::helper('wms')->__('Customer code #'),
            'align' => 'left',
            'index' => 'customer_code',
        ));


        $this->addColumn('component', array(
            'header' => Mage::helper('wms')->__('Component #'),
            'align' => 'left',
            'index' => 'component',
        ));

        $this->addColumn('quasarid', array(
            'header' => Mage::helper('wms')->__('Quasarid #'),
            'align' => 'left',
            'index' => 'quasarid',
        ));
        $this->addColumn('main_prod', array(
            'header' => Mage::helper('wms')->__('Main Product #'),
            'align' => 'left',
            'index' => 'main_prod',
        ));
        $this->addColumn('service_prod', array(
            'header' => Mage::helper('wms')->__('Service Product #'),
            'align' => 'left',
            'index' => 'service_prod',
        ));
        $this->addColumn('exec_partner', array(
            'header' => Mage::helper('wms')->__('Exec Partner #'),
            'align' => 'left',
            'index' => 'exec_partner',
        ));
        $this->addColumn('e_contract', array(
            'header' => Mage::helper('wms')->__('Contract #'),
            'align' => 'left',
            'index' => 'e_contract',
        ));
    }

}
