<?php 

class Bluehorse_Sap_Block_Adminhtml_Webamc extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {

        $this->_controller = 'adminhtml_webamc';
        $this->_blockGroup = 'sap';
        $this->_headerText = Mage::helper('sap')->__('eAMC Purchased');
        $this->_addButtonLabel = Mage::helper('sap')->__('Add New');
        parent::__construct();
        $this->_removeButton('add');
    }

}
