<?php

class Bluehorse_Sap_Block_Service extends Mage_Core_Block_Template {

    public function getLoginFormUrl() {

        return $this->getUrl('customer-service/request/loginPost');
    }

    public function getLogCustomer() {

        $CUST = Mage::getSingleton('customer/session')->getData('rfc_customer');
        $ADDRESS = $CUST['HOUSENO'] . ',' . $CUST['STREET1'] . ',' . $CUST['STREET2'] . ',' . $CUST['STREET4'] . ',' . $CUST['STREET4'];
        $regioon_name = Mage::getModel('directory/region')->loadByCode($CUST['REGION'], $CUST['COUNTRY']);
        $object = new Varien_Object();
        $object->setCustomerCode($CUST['CUSTOMER']);
        $object->setFirstName($CUST['FIRSTNAME']);
        $object->setLastName($CUST['LASTNAME']);
        $object->setLandLine($CUST['LANDLINE']);
        $object->setEmail($CUST['EMAIL']);
        $object->setMobile($CUST['MOBILE']);
        $object->setHouseNo($CUST['HOUSENO']);
        $object->setAddress($ADDRESS);
        $object->setStreet1($CUST['STREET1']);
        $object->setStreet2($CUST['STREET2']);
        $object->setStreet3($CUST['STREET3']);
        $object->setStreet4($CUST['STREET4']);
        $object->setCity($CUST['CITY']);
        $object->setPincode($CUST['PINCODE']);
        $object->setCountry('India'); //
        $object->setRegion($regioon_name->getName());
        $object->setSearchterm1($CUST['SEARCHTERM1']);
        $object->setSearchterm2($CUST['SEARCHTERM2']);
        $object->setServicebp($CUST['SERVICEBP']);
        $object->setFlag($CUST['FLAG']);
        $object->setLang($CUST['LANG']);
        $object->setProductCategory($CUST['PRODUCT_CATEGORY']);


        return $object;
    }

    public function getLogCustomerComp() {

        $components = Mage::getSingleton('customer/session')->getData('rfc_customer_comp');
        $collection = new Varien_Data_Collection();

        foreach ($components as $row) {

            $obj = new Varien_Object();
            $obj->setCustomerCode($row['CUSTID']);
            $obj->setComponentId($row['COMPONENT_ID']);
            $obj->setIBase($row['IBASE']);
            $obj->setProductId($row['PRODUCT_ID']);
            $obj->setProductDescription($row['PRODUCT_DESCRIPT']);
            $obj->setContractNumber($row['CONTRACT_NUMBER']);
            $obj->setContractStatus($row['CONTRACT_STATUS']);
            $obj->setValidFrom($row['VALID_FROM']);
            $obj->setValidTo($row['VALID_TO']);
            $obj->setFlag($row['FLAG']);
            $obj->setLocatedAt($row['LOCATED_AT']);
            $obj->setSerialnumber($row['SERIAL_NUMBER']);
            $collection->addItem($obj);
            unset($obj);
        }
        return $collection;
    }

    public function currentComponent() {
        $compid = $this->getRequest()->getParam('compid');

        $components = Mage::getSingleton('customer/session')->getData('rfc_customer_comp');

        foreach ($components as $key => $value) {
            $valided = array_search($compid, $value, true);
            if (!empty($valided)) {
                $item = $value;
                break;
            }
        }

        $obj = new Varien_Object();
        $obj->setCustomerCode($item['CUSTID']);
        $obj->setComponentId($item['COMPONENT_ID']);
        $obj->setIBase($item['IBASE']);
        $obj->setProductId($item['PRODUCT_ID']);
        $obj->setProductDescription($item['PRODUCT_DESCRIPT']);
        $obj->setContractNumber($item['CONTRACT_NUMBER']);
        $obj->setContractStatus($item['CONTRACT_STATUS']);
        $obj->setValidFrom($item['VALID_FROM']);
        $obj->setValidTo($item['VALID_TO']);
        $obj->setFlag($item['FLAG']);
        $obj->setLocatedAt($item['LOCATED_AT']);
        $obj->setSerialnumber($item['SERIAL_NUMBER']);

        return $obj;
    }

    

    public function updateProfileUrl() {
        return $this->getUrl('customer-service/request/profilePost');
    }

    public function getLogServiceRequestFormUrl() {
        return $this->getUrl('customer-service/request/ServiceRequesPost');
    }

    public function getNonDbSrFormUrl() {
        return $this->getUrl('customer-service/request/nondbPost');
    }

}
