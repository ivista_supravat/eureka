<?php

ini_set("display_errors", "1");
error_reporting(E_ALL & ~E_NOTICE);

class Bluehorse_Sap_AmcController extends Mage_Core_Controller_Front_Action {

    protected function _construct() {
        parent::_construct();
        if (extension_loaded("saprfc") == false) {
            $error = Mage::helper('sap')->__('PHP extension "saprfc" must be loaded.');
            Mage::throwException($error);
            return;
        }
    }

    public function preDispatch() {
        parent::preDispatch();

        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('sap')->getLoginUrl();

        $openActions = array(
            'login',
            'amclead',
            'webleadPost',
            'download'
        );
        $pattern = '/^(' . implode('|', $openActions) . ')/i';

        if (!preg_match($pattern, $action)) {
            if (!$this->_getSession()->authenticate($this)) {
                //$this->setFlag('', 'no-dispatch', true);
                return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::helper('sap')->getLoginUrl());
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }

    protected function _getSession() {
        return Mage::getSingleton('customer/session');
    }

    public function indexAction() {

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('customer-service/amc/details');
            return;
        }
        $this->_redirect('customer-service/amc/login');
    }

    public function loginAction() {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('customer-service/amc/details');
            return;
        }

        $this->loadLayout();
        $this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
        $myBlock = $this->getLayout()->createBlock('sap/amc');
        $myBlock->setTemplate('bluehorse_sap/amc/login.phtml');
        $this->getLayout()->getBlock('content')->insert($myBlock);
        $this->renderLayout();
    }

    public function loginPostAction() {

        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*/');
            return;
        }

        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session = $this->_getSession();



        // If customer wants to login by email
        $post = $this->getRequest()->getPost();

        if ($this->getRequest()->isPost() && !empty($post['email'])) {

            if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                Mage::getSingleton('core/session')->addError(Mage::helper('sap')->__('You have entered an invalid email address'));
                return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/login"));
            }

            // Check valid customer 
            $sparfc = Mage::getModel('sap/saprfc');
            $validCustomer = $sparfc->loginAMC($post['email']);

//            if ($validCustomer === false) {
//                Mage::getSingleton('core/session')->addSuccess(Mage::helper('sap')->__('Confirmation link sent to your email address.'));
//                return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/login"));
//            } else {
            $model = Mage::getModel('sap/emaillog');

            $post = $this->getRequest()->getPost();

            $post['hash'] = md5(rand(0, 1000));
            $post['name'] = Mage::helper('sap')->__('Customer');
            $post['confirmationlink'] = Mage::getUrl('customer-service/amc/amc_account_details', array('_query' => '_rdr=' . $post['hash']));

            $error = false;

            if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                $error = true;
            }
            if (!Zend_Validate::is(trim($post['hash']), 'NotEmpty')) {
                $error = true;
            }

            if (!Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
                $error = true;
            }
            if (!Zend_Validate::is(trim($post['confirmationlink']), 'NotEmpty')) {
                $error = true;
            }

            if ($error) {
                throw new Exception();
            }
            Mage::log($post['confirmationlink'], null, 'saprfc.log');
            try {
                $model->setEmail($post['email']);
                $model->setHash($post['hash']);
                $model->save();
            } catch (Exception $ex) {
                Mage::getSingleton('core/session')->addError($ex->getMessage());
            }
            //}
            // Now send email
            Mage::getSingleton('core/session')->addSuccess(Mage::helper('sap')->__('Confirmation link sent to your email address.'));
            return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/login"));
        }

        // If customer wants to login by mobile
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('mobile')) {
            $mobile_no = $this->getRequest()->getPost('mobile');

            $sparfc = Mage::getModel('sap/saprfc');
            $validCustomer = $sparfc->loginAMC($mobile_no);
            if ($validCustomer === false) {
                return $this->_redirectUrl(Mage::getUrl('customer-service/amc/login', array('_secure' => true, 'DB' => 1)));
                //return $this->_redirectUrl(Mage::getUrl('customer-service/amc/amclead'));
            } else {
                $validCustomer = $sparfc->getCustmoerComponenet($mobile_no);
                Mage::getSingleton('core/session')->addSuccess('You have been successfully logged in.');
                Mage::getSingleton('sap/amc')->setComponent($validCustomer);
                Mage::getSingleton('sap/session')->setData('customer_comp', $validCustomer);
                return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/details"));
            }
        }
    }

    public function detailsAction() {

        // $sparfc = Mage::getModel('sap/saprfc');
        //$validCustomer = $sparfc->getCustmoerComponenet($mobileNo);


        /* if (!empty($validCustomer)) {
          Mage::getSingleton('sap/amc')->setComponent($validCustomer);
          Mage::getSingleton('sap/session')->setData('customer_comp', $validCustomer);
          } */

        $mobile_no = '';
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $mobile_no = $customerData->getMobileNo();
        }

        if (empty($mobile_no)) {
            Mage::getSingleton('core/session')->addError('Mobile number is mandatory');
            return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/login"));
        }

        $sparfc = Mage::getModel('sap/saprfc');
        $cust_comp = $sparfc->getCustmoerComponenet($mobile_no);
        if (empty($cust_comp)) {
            var_dump("will come a amc web lead");
            die;
        }
        Mage::getSingleton('customer/session')->setData('customer_comp', $cust_comp);
        $this->loadLayout();
        $this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
        $myBlock = $this->getLayout()->createBlock('sap/amc');
        $myBlock->setTemplate('bluehorse_sap/amc/details.phtml');
        $this->getLayout()->getBlock('content')->insert($myBlock);
        $this->renderLayout();
    }

    public function optionsAction() {

        $mobile_no = '';
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $mobile_no = $customerData->getMobileNo();
        }

        if (empty($mobile_no)) {
            Mage::getSingleton('core/session')->addError('Mobile number is mandatory');
            return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/login"));
        }
        $data = $this->getRequest()->getPost();

        if (empty($data)) {
            Mage::getSingleton('core/session')->addError('Please select your component');
            return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/details"));
        }

        $sparfc = Mage::getModel('sap/saprfc');
        $options = $sparfc->getOptions($data);
        Mage::getSingleton('sap/session')->setData('I_COMPONENTID', $this->getRequest()->getParam('I_COMPONENTID'));
        Mage::getSingleton('sap/session')->setData('options', $options);
        Mage::getSingleton('sap/amc')->setOptoins($options);

        $this->loadLayout()
                ->_initLayoutMessages('core/session')
                ->_initLayoutMessages('core/session')
                ->getLayout()->getBlock('head')->setTitle($this->__('eBuy Annual Maintenance Contract (AMC) Details'));
        $this->renderLayout();
    }

    public function paymentAction() {
        $data = $this->getRequest()->getPost();
        if (empty($data)) {
            return $this->_redirectReferer();
        }
        $options = Mage::getSingleton('sap/session')->getData('options');
        foreach ($options as $key => $value) {
            $valided = array_search($data['AMC_PROD'], $value, true);
            if (!empty($valided)) {
                $item = $value;
                break;
            }
        }
        Mage::getSingleton('sap/session')->setData('selected_item', $item);
        $this->loadLayout()
                ->_initLayoutMessages('core/session')
                ->_initLayoutMessages('core/session')
                ->getLayout()->getBlock('head')->setTitle($this->__('eBuy Annual Maintenance Contract (AMC) Details'));
        $this->renderLayout();
    }

    public function makepaymentAction() {

        $data = $this->getRequest()->getPost();

        $sparfc = Mage::getSingleton('sap/saprfc')->createAMC($data);
        //Mage::getSingleton('core/session')->setContractNum
        //$data = Mage::getModel('sap/payUbiz')->captureFields($data);
        $item = Mage::getSingleton('sap/session')->getData('selected_item');
        $customer = Mage::getSingleton('sap/session')->getData('customer_comp');
        $cust = $customer['customer'];
        $I_COMPONENTID = Mage::getSingleton('sap/session')->getData('I_COMPONENTID');

        if (!empty($data)) {
            $data = array(
                'key' => 'gtKFFx',
                'txnid' => uniqid('animesh_'),
                'amount' => $item['PRICE'] - $item['DISCOUNT'],
                'firstname' => $cust['FIRSTNAME'],
                'email' => 'test@payu.in',
                'phone' => $cust['MOBILE'],
                'productinfo' => $sparfc,
                'surl' => 'http://127.0.0.1/eurekaforbes/index.php/customer-service/redirect/success/',
                'furl' => 'http://127.0.0.1/eurekaforbes/index.php/customer-service/redirect/failure'
            );
            $f = Mage::getModel('sap/payUbiz')->payUpage($data);
            var_dump($f);
            die;
            //$this->_redirect('customer-service/amc/success');
        }
        $this->loadLayout();
        $this->renderLayout();

        Mage::getSingleton('sap/session')->setData('pg', $this->getRequest()->getParam('payment_type'));
        Mage::getModel('sap/payUbiz')->FormFields();


        $this->loadLayout()
                ->_initLayoutMessages('core/session')
                ->_initLayoutMessages('core/session')
                ->getLayout()->getBlock('head')->setTitle($this->__('eBuy Annual Maintenance Contract (AMC) Details'));
        $this->renderLayout();
    }

    public function testAction() {
        $data = $this->getRequest()->getPost();
        if (!empty($data)) {
            $data = array(
                'key' => 'gtKFFx',
                'txnid' => uniqid('animesh_'),
                'amount' => rand(0, 100),
                'firstname' => 'Test',
                'email' => 'test@payu.in',
                'phone' => '98765433210',
                'productinfo' => 'Product Info',
                'surl' => 'http://127.0.0.1/eurekaforbes/index.php/customer-service/redirect/success/',
                'furl' => 'http://127.0.0.1/eurekaforbes/index.php/customer-service/redirect/failure'
            );
            $f = Mage::getModel('sap/payUbiz')->payUpage($data);
            var_dump($f);
            die;
            //$this->_redirect('customer-service/amc/success');
        }
        $this->loadLayout();
        $this->renderLayout();
    }

    public function failureAction() {
        $this->loadLayout()
                ->_initLayoutMessages('core/session')
                ->_initLayoutMessages('core/session')
                ->getLayout()->getBlock('head')->setTitle($this->__('Payment Failured'));
        $this->renderLayout();
    }

    public function successAction() {

        $contacrNumber = Mage::getSingleton('sap/session')->getData('e_contract');
        $thml = Mage::helper('sap')->__("Your request for AMC has been submitted successfully.<br/> Your contract No. is:  $contacrNumber </b>. Kindly note this number for future correspondence.");


        $this->loadLayout();
        $this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
        $myBlock = $this->getLayout()->createBlock('sap/amc');
        $myBlock->setTemplate('bluehorse_sap/amc/payment/success.phtml');
        $myBlock->setResponse($thml);
        $this->getLayout()->getBlock('content')->insert($myBlock);
        $this->renderLayout();
    }

    /*
     * Download customer amc purchase pdf
     * 
     * 
     */

    public function DownloadAction() {

        $tcpdf = new TCPDF_TCPDF();

        $html = '<h1> hello world  dddd </h1>';
        /* $myBlock = $this->getLayout()->createBlock('core/template')
          ->setTemplate('bluehorse_sap/invoice.phtml');

          $html = $this->getResponse()->setBody($myBlock->toHtml());
         */
        $tcpdf->setPrintHeader(false);
        $tcpdf->setPrintFooter(false);
        $tcpdf->AddPage();

        $tcpdf->writeHTML($html, true, false, true, false, '');

        $tcpdf->lastPage();

        $tcpdf->Output('ICCR_2364' . time() . '.pdf', 'I');
    }

    public function thankyouAction() {
        $Username = 'Keshab Karmakar';
        $useremail = 'keshab.karmakar@bluehorse.in';
        $emailTemp = Mage::getModel('core/email_template')->load(4);
        $emailTempVariables = array();
        $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
        $adminEmail = $admin_storemail ? $admin_storemail : Mage::getStoreConfig('trans_email/ident_general/email');
        $adminUsername = 'Admin';
        $emailTempVariables['customername'] = 'Keshab Karmakar';
        $emailTempVariables['content'] = ' Thank you! for purchasing this amc';
        $processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
        $emailTemp->setSenderName($adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail, $Username, $emailTempVariables);
        $message = 'Thank you! We have received your service request and your service ticket number is xxxxxx.';
        $mobile = '918553007539';
        $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
        $this->loadLayout();
        $this->renderLayout();
    }

    public function myamclistAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function mylogserviceslistAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function buyamcinformAction() {
        $Username = 'Keshab Karmakar';
        $useremail = 'keshab.karmakar@bluehorse.in';
        $emailTemp = Mage::getModel('core/email_template')->load(4);
        $emailTempVariables = array();
        $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
        $adminEmail = $admin_storemail ? $admin_storemail : Mage::getStoreConfig('trans_email/ident_general/email');
        $adminUsername = 'Admin';
        $emailTempVariables['customername'] = 'Keshab Karmakar';
        $emailTempVariables['content'] = ' your have 30 days left to purchase AMC for your product';
        $processedTemplate = $emailTemp->getProcessedTemplate(
                $emailTempVariables);
        $emailTemp->setSenderName(
                $adminUsername);
        $emailTemp->setSenderEmail($adminEmail);
        $emailTemp->send($useremail, $Username, $emailTempVariables);
        $message = 'Thank you! We have received your service request and your service ticket number is xxxxxx.';
        $mobile = '918553007539';
        $sms_status = Mage::getSingleton('ezoomerce/client')->call(array('dest' => $mobile, 'msg' => $message));
        Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());
    }

    public function listAction() {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('customer-service/amc/');
            return;
        }
        $this->loadLayout();
        $this->renderLayout();
    }

    public function amcleadAction() {
        $this->loadLayout()
                ->_initLayoutMessages('core/session')
                ->_initLayoutMessages('core/session')
                ->getLayout()->getBlock('head')->setTitle($this->__('eBuy Annual Maintenance Contract (AMC) Web Lead'));
        $this->renderLayout();
    }

    public function webleadPostAction() {

        if ($this->getRequest()->isPost()) {
            $sparfc = Mage::getModel('sap/saprfc');
            $response = $sparfc->createWebAmcLead($this->getRequest()->getParams());
        }
        $thml = Mage::helper('sap')->__("Thank you for sharing your details with us.<br/> Your request reference no <b> $response </b>. we shall  get back to you shortly.");

        $this->loadLayout();
        $this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
        $myBlock = $this->getLayout()->createBlock('sap/amc');
        $myBlock->setTemplate('bluehorse_sap/thankyou.phtml');
        $myBlock->setResponse($thml);
        $this->getLayout()->getBlock('content')->insert($myBlock);
        $this->renderLayout();

        //  Your request reference no 123456. we shall  get back to you shortly
        //Mage::getSingleton('core/session')->addSuccess('Thank you for sharing your details with us.');
        //return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/details"));
    }

    public function amc_account_detailsAction() {

        $_rdr = $this->getRequest()->getParam('_rdr');

        if (empty($_rdr)) {
            Mage::getSingleton('core/session')->addError('Your confirmation link has expired. please try again');
            return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/login"));
        }

        if (!empty($_rdr)) {
            $email = Mage::getModel('sap/emaillog')->load($_rdr, 'hash')->getEmail();

            // Call to saprfc function module to login
            $sparfc = Mage::getModel('sap/saprfc');
            $validCustomer = $sparfc->loginAMCEmail($email);
            if ($validCustomer === false) {
                return $this->_redirectUrl(Mage::getUrl('customer-service/amc/login', array('_secure' => true, 'DB' => 1)));
                //return $this->_redirectUrl(Mage::getUrl('customer-service/amc/amclead'));
            } else {
                $validCustomer = $sparfc->getCustmoerComponenet($mobile_no);
                Mage::getSingleton('core/session')->addSuccess('You have been successfully logged in.');
                Mage::getSingleton('sap/amc')->setComponent($validCustomer);
                Mage::getSingleton('sap/session')->setData('customer_comp', $validCustomer);
                return Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("customer-service/amc/details"));
            }
            var_dump($email);
            die;
        }
    }

}

/* http://abakalidis.blogspot.in/2009/12/how-to-set-up-saprfc-extension-for-php.html */
?>
     





