<?php

class Bluehorse_Sap_DownloadController extends Mage_Core_Controller_Front_Action {

    public function html2pdfAction() {

        
        $tcpdf = new TCPDF_TCPDF();

        // $html = '<h1> hello world  dddd </h1>';
        $block = $this->getLayout()->createBlock('core/template')
                ->setTemplate('bluehorse_sap/invoice.phtml');

        $html = $this->getResponse()->setBody($block->toHtml());

        $tcpdf->setPrintHeader(false);
        $tcpdf->setPrintFooter(false);
        $tcpdf->AddPage();

        $tcpdf->writeHTML($html, true, false, true, false, '');

        $tcpdf->lastPage();

        $tcpdf->Output('ICCR_' . time() . '.pdf', 'I');
    }

    public function indexAction() {
        
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('contract_no')) {
            Mage::getSingleton('sap/session')->setData('e_contract', $this->getRequest()->getPost('contract_no'));
        }
        
        $tcpdf = new TCPDF_TCPDF();

        $block = $this->getLayout()->createBlock('sap/service')
                ->setTemplate('bluehorse_sap/invoice.phtml');

        $html = $this->getResponse()->setBody($block->toHtml());


        $tcpdf->setPrintHeader(false);
        $tcpdf->setPrintFooter(false);
        $tcpdf->AddPage();

        $tcpdf->writeHTML($html, true, false, true, false, '');

        $tcpdf->lastPage();

        $tcpdf->Output('ICCR_' . time() . '.pdf', 'I');
    }

    public function printPdfAction() {
        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $page->setFont($font, 8);

        //add a logo
        $image = Mage::getBaseDir('media') . '/logo-eureka.jpg';
        if (is_file($image)) {
            $image1 = Zend_Pdf_Image::imageWithPath($image);

            $x = 30;
            $y = 750;
            $page->drawImage($image1, $x, $y, $x + 180, $y + 60);
        }

        $image2 = Mage::getBaseDir('media') . '/logo-shproji.jpg';
        if (is_file($image2)) {
            $image2 = Zend_Pdf_Image::imageWithPath($image2);

            $x = 450;
            $y = 750;
            $page->drawImage($image2, $x, $y, $x + 120, $y + 50);
        }

        $x = 25;
        $y = 720;
        $page->setLineWidth(0.5);
        $page->drawLine($x, $y, $x + 545, $y + 0);

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD), 16);
        $titre = "INVOICE CUM CONTRACT RECEIPT";
        $page->drawText($titre, $page->getWidth() - 420, $page->getHeight() - 150, "UTF-8");

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES), 14);
        $titre = "for comprehensive service contract";
        $page->drawText($titre, $page->getWidth() - 420, $page->getHeight() - 170, "UTF-8");

        //Date and time
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES), 10);
        $txt = "Date : " . date('d-m-Y');
        $page->drawText($txt, $page->getWidth() - 570, $page->getHeight() - 210, "UTF-8");

        //Date and time
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES), 10);
        $txt = "Contract Receipt No : 984634334522";
        $page->drawText($txt, $page->getWidth() - 175, $page->getHeight() - 210, "UTF-8");

        // Draw line 
        $x = 25;
        $y = 625;
        $page->setLineWidth(1);
        $page->drawLine($x, $y, $x + 545, $y + 0);

        //Date and time
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES), 14);
        $txt = "Customer's Personal Details";
        $page->drawText($txt, $page->getWidth() - 570, $page->getHeight() - 250, "UTF-8");

        // Draw Table
        //$page->drawRectangle(25, $page->getHeight() - 260, $page->getWidth() - 25, $page->getHeight() - 280, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        // $page->drawText('Column1', $x + 1, $this->y + 50, 'UTF-8');
        $x = 25;
        $y = $page->getHeight() - 260;
        //for ($i = 0; $i > 5; $i++) {
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $page->getHeight() - 260, $page->getWidth() - 25, $page->getHeight() - 280, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(25, $page->getHeight() - 280, $page->getWidth() - 25, $page->getHeight() - 300, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(25, $page->getHeight() - 300, $page->getWidth() - 25, $page->getHeight() - 320, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(25, $page->getHeight() - 320, $page->getWidth() - 25, $page->getHeight() - 340, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawRectangle(25, $page->getHeight() - 340, $page->getWidth() - 25, $page->getHeight() - 360, Zend_Pdf_Page::SHAPE_DRAW_STROKE);


//            $page->drawLine($x + 30, $y + 12, $x + 30, $y - 8);
//            $page->drawLine($x + 150, $y + 12, $x + 150, $y - 8);
//            $page->drawLine($x + 490, $y + 12, $x + 490, $y - 8);
//            $page->drawLine($x + 30, $y + 12, $x + 30, $y - 8);
//            $page->drawLine($x + 150, $y + 12, $x + 150, $y - 8);
//            $page->drawLine($x + 490, $y + 12, $x + 490, $y - 8);
        // row1
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES), 8);

        $page->drawText("Name:", $page->getWidth() - 560, $page->getHeight() - 275, "UTF-8");
        $page->drawText("Supravat Mondal", $page->getWidth() - 500, $page->getHeight() - 275, "UTF-8");

        $page->drawText("Phone : ", $page->getWidth() - 250, $page->getHeight() - 275, "UTF-8");
        $page->drawText("9564705218", $page->getWidth() - 180, $page->getHeight() - 275, "UTF-8");


        // row2
        $page->drawText("Address:", $page->getWidth() - 560, $page->getHeight() - 295, "UTF-8");
        $page->drawText("Jnanbharathi Campus, Bengaluru", $page->getWidth() - 500, $page->getHeight() - 295, "UTF-8");
        $page->drawText("Mobile : ", $page->getWidth() - 250, $page->getHeight() - 295, "UTF-8");
        $page->drawText("9564705218", $page->getWidth() - 180, $page->getHeight() - 295, "UTF-8");


        // row3
        $page->drawText("Pin:", $page->getWidth() - 560, $page->getHeight() - 315, "UTF-8");
        $page->drawText("560052", $page->getWidth() - 500, $page->getHeight() - 315, "UTF-8");
        $page->drawText("Component No : ", $page->getWidth() - 250, $page->getHeight() - 315, "UTF-8");
        $page->drawText("9564705218", $page->getWidth() - 180, $page->getHeight() - 315, "UTF-8");


        // row4
        $page->drawText("Email:", $page->getWidth() - 560, $page->getHeight() - 335, "UTF-8");
        $page->drawText("supravat.mondal@ivistasolutions.com", $page->getWidth() - 500, $page->getHeight() - 335, "UTF-8");

        // row5
        $page->drawText("Unit SL. No:", $page->getWidth() - 560, $page->getHeight() - 355, "UTF-8");
        $page->drawText("123456789012", $page->getWidth() - 500, $page->getHeight() - 355, "UTF-8");


        //Draw table header row’s
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD), 12);
        $page->drawText("Product", $page->getWidth() - 540, $page->getHeight() - 400, "UTF-8");
        $page->drawText("Model", $page->getWidth() - 400, $page->getHeight() - 400, "UTF-8");
        $page->drawText("Quantity", $page->getWidth() - 300, $page->getHeight() - 400, "UTF-8");
        $page->drawText("Basic Rate", $page->getWidth() - 200, $page->getHeight() - 400, "UTF-8");
        $page->drawText("Amount", $page->getWidth() - 100, $page->getHeight() - 400, "UTF-8");


        $page->drawRectangle(25, $page->getHeight() - 500, $page->getWidth() - 25, $page->getHeight() - 420, Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8);
        $page->drawText("GWPDDRMRRUOOOOO", $page->getWidth() - 560, $page->getHeight() - 460, "UTF-8");
        $page->drawText("DR. AQUAGUARD MAGNA HD RO", $page->getWidth() - 440, $page->getHeight() - 460, "UTF-8");
        $page->drawText(1, $page->getWidth() - 260, $page->getHeight() - 460, "UTF-8");
        $page->drawText("Rs. 100000.00", $page->getWidth() - 200, $page->getHeight() - 460, "UTF-8");
        $page->drawText("Rs. 100000.00", $page->getWidth() - 100, $page->getHeight() - 460, "UTF-8");


        // note : 
        $note = 'Note: The above rate is inclusive of service tax';
        $page->drawText($note, 25, $page->getHeight() - 550, "UTF-8");

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD), 12);
        $page->drawText("Total:", $page->getWidth() - 200, $page->getHeight() - 550, "UTF-8");
        $page->drawText("Discount:", $page->getWidth() - 200, $page->getHeight() - 570, "UTF-8");
        $page->drawText("Grand Total:", $page->getWidth() - 200, $page->getHeight() - 590, "UTF-8");

        $style = new Zend_Pdf_Style();
        $style->setFillColor(new Zend_Pdf_Color_HTML('#708090'));
        $style->setFont(Zend_Pdf_font::fontWithName(Zend_pdf_font::FONT_TIMES_BOLD), 10);
        $page->setStyle($style);
        $page->drawText("Rs. 100000.00", $page->getWidth() - 100, $page->getHeight() - 550, "UTF-8");
        $page->drawText("Rs. 99.00", $page->getWidth() - 100, $page->getHeight() - 570, "UTF-8");
        $page->drawText("Rs. 99001.00", $page->getWidth() - 100, $page->getHeight() - 590, "UTF-8");


        $x = 25;
        $y = 720;
        $page->setLineWidth(0.5);
        $page->drawLine(25, $page->getHeight() - 610, $page->getWidth() - 25, $page->getHeight() - 610);


        $pdf->pages[] = $page;

        //generate pdf
        $content = $pdf->render();

        $fileName = date('d-m-Y-His') . '.pdf';
        //send it to the browser to download
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /* public function prindPdfAction() {
      $pdf = new Zend_Pdf();
      $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
      $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
      $page->setFont($font, 12);


      $page->drawText('Customer:', 20, 30, 'UTF-8');

      //add text
      $page->setFont($font, 16);
      $titre = "Your AMC Purchase eRecipt ";
      $page->drawText($titre, 155, $page->getHeight() - 85, "UTF-8");

      //add pages to main document
      $pdf->pages[] = $page;

      //generate pdf
      $content = $pdf->render();

      $fileName = rand(11, 999) . ' . pdf  ';
      //send it to the browser to download
      $this->_prepareDownloadResponse($fileName, $content);
      } */
}
