<?php

ini_set("display_errors", "1");
error_reporting(E_ALL & ~E_NOTICE);

class Bluehorse_Sap_RedirectController extends Mage_Core_Controller_Front_Action {

    public function cancelAction() {
        echo 'Cancel amc';
        $response = $this->getRequest()->getPost();
        var_dump($response);
        die;
        $this->_redirect('customer-service/amc/cancel');
    }

    public function failureAction() {
        $response = $this->getRequest()->getPost();
        Mage::getModel('sap/payUbiz')->getResponseOperation($response);
        $this->_redirect('customer-service/amc/failure');
    }

    public function successAction() {
        $response = $this->getRequest()->getPost();
        Mage::getModel('sap/payUbiz')->getResponseOperation($response);
        $this->_redirect('customer-service/amc/success');
    }

}
