<?php
class Bluehorse_Sap_Adminhtml_SapController extends Mage_Adminhtml_Controller_Action {
	
	public function indexAction() {
		
	}
	public function updateAction() {
		
		$order_id = $this->getRequest()->getParam('order_id');
		$sap_id = $this->getRequest()->getParam('sap_id');
		
		$order  = Mage::getModel('sales/order')->load($order_id);
		
		$order->setSapId($sap_id);
		$order->save();
		Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Sap Code updated"));
		$this->_redirectReferer();
	}
	
}
