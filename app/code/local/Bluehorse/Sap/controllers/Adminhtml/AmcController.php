<?php
class Bluehorse_Sap_Adminhtml_AmcController extends Mage_Adminhtml_Controller_Action {
	
	
	protected function _initAction() {
        $this->_title($this->__('eAMC Management'))
                ->loadLayout();
		$this->_setActiveMenu('customer/amcpurches');
        return $this;
    }
	public function indexAction() {
        $this->loadLayout();
        $myblock = $this->getLayout()->createBlock('sap/adminhtml_webamc');
        $this->_addContent($myblock);
        $this->renderLayout();
    }
}
