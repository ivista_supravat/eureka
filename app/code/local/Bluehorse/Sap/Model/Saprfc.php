<?php

class Bluehorse_Sap_Model_Saprfc {

    public function loginAMC($mobileNo) {

        $SAPRFC = new SAPRFC_SAP();

        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $row = array();
        $sap = new SAPConnection();

        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            throw new Exception('Could not connect to server');
        }

        $fce = $sap->NewFunction("Z_WEB_MOBILENO_GET_CUST_NEW");

        if ($fce == false) {
            Mage::log("Function module response false", null, 'saprfc.log');
            return false;
        }

        $fce->I_MOBILENO = $mobileNo;
        $fce->I_EMAIL = '';
        $fce->Call();


        if ($fce->GetStatus() == SAPRFC_OK) {
            $fce->ET_CUST_DET->Reset();

            while ($fce->ET_CUST_DET->Next()) {
                $row[] = $fce->ET_CUST_DET->row;
                //$customer = Mage::getModel('sap/customer')->createNewCustomer($row);
                //Mage::log($row, null, 'saprfc.log');
            }
            $customer = $row[0];

            Mage::log('=======' . $mobileNo . '========== ', null, 'saprfc.log');
            if (empty($row)) {
                Mage::log('Customer is not found (Non-DB): ' . count($row), null, 'saprfc.log');
                return false;
            }
            if (count($row) > 1) {
                Mage::log('Multiple Customer has found (Non-DB): ' . count($row), null, 'saprfc.log');
                return false;
            }

            Mage::getModel('sap/customer')->createNewCustomer($customer);
            return $customer;
        } else {
            $sap->Close();
            return false;
        }
    }

    public function loginAMCByEmail($email) {

        $SAPRFC = new SAPRFC_SAP();

        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $row = array();
        $sap = new SAPConnection();

        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            throw new Exception('Could not connect to server');
        }

        $fce = $sap->NewFunction("Z_WEB_MOBILENO_GET_CUST_NEW");

        if ($fce == false) {
            Mage::log("Function module response false", null, 'saprfc.log');
            return false;
        }

        $fce->I_MOBILENO = '';
        $fce->I_EMAIL = $email;
        $fce->Call();


        if ($fce->GetStatus() == SAPRFC_OK) {
            $fce->ET_CUST_DET->Reset();

            while ($fce->ET_CUST_DET->Next()) {
                $row[] = $fce->ET_CUST_DET->row;
                //$customer = Mage::getModel('sap/customer')->createNewCustomer($row);
                //Mage::log($row, null, 'saprfc.log');
            }
            $customer = $row[0];

            Mage::log('=======' . $email . '========== ', null, 'saprfc.log');
            if (empty($row)) {
                Mage::log('Customer is not found (Non-DB): ' . count($row), null, 'saprfc.log');
                return false;
            }
            if (count($row) > 1) {
                Mage::log('Multiple Customer has found (Non-DB): ' . count($row), null, 'saprfc.log');
                return false;
            }
            return $customer;
        } else {
            $sap->Close();
            return false;
        }
    }

    public function loginAMCEmail($email) {
        $SAPRFC = new SAPRFC_SAP();

        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $row = array();
        $sap = new SAPConnection();

        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            throw new Exception('Could not connect to server');
        }

        $fce = $sap->NewFunction("Z_WEB_MOBILENO_GET_CUST_NEW");

        if ($fce == false) {
            Mage::log("Function module response false", null, 'saprfc.log');
            return false;
        }

        $fce->I_MOBILENO = '';
        $fce->I_EMAIL = $email;
        $fce->Call();


        if ($fce->GetStatus() == SAPRFC_OK) {
            $fce->ET_CUST_DET->Reset();

            while ($fce->ET_CUST_DET->Next()) {
                $row[] = $fce->ET_CUST_DET->row;
                //$customer = Mage::getModel('sap/customer')->createNewCustomer($row);
                //Mage::log($row, null, 'saprfc.log');
            }
            $customer = $row[0];

            Mage::log('======= ' . $email . ' ========== ', null, 'saprfc.log');
            if (empty($row)) {
                Mage::log('Customer is not found (Non-DB): ' . count($row), null, 'saprfc.log');
                return false;
            }
            if (count($row) > 1) {
                Mage::log('Multiple Customer has found (Non-DB): ' . count($row), null, 'saprfc.log');
                return false;
            }

            Mage::getModel('sap/customer')->createNewCustomer($customer);
            return $customer;
        } else {
            $sap->Close();
            return false;
        }
    }

    public function getCustomerByMobile($mobileNo) {

        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $row = array();
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            throw new Exception('Could not connect to server');
        }

        $fce = $sap->NewFunction("Z_WEB_MOBILENO_GET_CUST_NEW");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            $sap->PrintStatus();
            throw new Exception($sap->PrintStatus());
        }

        $fce->I_MOBILENO = $mobileNo;
        $fce->I_EMAIL = '';
        $fce->Call();


        if ($fce->GetStatus() == SAPRFC_OK) {
            $fce->ET_CUST_DET->Reset();

            while ($fce->ET_CUST_DET->Next()) {
                $row[] = $fce->ET_CUST_DET->row;
                //$customer = Mage::getModel('sap/customer')->createNewCustomer($row);
                //Mage::log($row, null, 'saprfc.log');
            }
            $customer = $row[0];
            $next_action = '';
            Mage::log('=======' . $mobileNo . '========== ', null, 'saprfc.log');
            if (empty($row)) {
                Mage::log('Customer is not found (Non-DB): ' . count($row), null, 'saprfc.log');
                return false;
            }
            if (count($row) > 1) {
                Mage::log('Multiple Customer has found (Non-DB): ' . count($row), null, 'saprfc.log');
                return false;
            }
            if (count($row) == 1) {
                $record = $row;
                $row = $row[0];

                if (empty($row['SERVICEBP'])) {
                    Mage::log('SERVICEBP is empty', null, 'saprfc.log');
                    Mage::log($row, null, 'saprfc.log');
                    return false;
                }
                Mage::log('Only one customer has found (Non-DB): ' . count($record), null, 'saprfc.log');
                $comp = $this->getComponentByMobile($mobileNo);
                Mage::log('Number of component of this customer: ' . count($comp), null, 'saprfc.log');

                if (count($comp) === 0) {
                    Mage::log('No component found for the customer: ', null, 'saprfc.log');
                    return false;
                }
                if (count($comp) > 1) {
                    Mage::log('More then one component fount for the customer: ' . count($comp), null, 'saprfc.log');
                    return false;
                }
                Mage::log($row, null, 'saprfc.log');
            }
            Mage::getModel('sap/customer')->createNewCustomer($customer);
            return $customer;
        } else {
            Mage::log($fce->PrintStatus(), null, 'saprfc.log');
            throw new Exception($fce->PrintStatus());
            $sap->Close();
            return false;
        }
    }

    public function getComponentByMobile($mobileNo) {
        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $component = array();
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            throw new Exception('Could not connect to server');
        }

        $fce = $sap->NewFunction("Z_WEB_GET_CUST_COMP_DETAILS");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            $sap->PrintStatus();
            throw new Exception($sap->PrintStatus());
        }

        $fce->I_MOBILENO = $mobileNo;
        $fce->I_EMAIL = '';
        $fce->Call();


        if ($fce->GetStatus() == SAPRFC_OK) {
            $fce->E_COMP->Reset();
            while ($fce->E_COMP->Next()) {
                $component[] = $fce->E_COMP->row;
            }
            Mage::getSingleton('sap/amc')->setComponent($component);
            //Mage::log($component, null, 'saprfc.log');
            return $component;
        } else {
            Mage::log($fce->PrintStatus(), null, 'saprfc.log');
            throw new Exception($fce->PrintStatus());
            $sap->Close();
        }
    }

    /**
     * 
     * Z_WEB_GET_CUST_COMP_DETAILS
     */
    public function getCustmoerComponenet($mobileNo) {
        Mage::log("IMPORT :" . $mobileNo, null, 'saprfc.log');
        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        //$component = array();
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            throw new Exception('Could not connect to server');
        }

        $fce = $sap->NewFunction("Z_WEB_GET_CUST_COMP_DETAILS");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            $sap->PrintStatus();
            throw new Exception($sap->PrintStatus());
        }
        Mage::log("IMPORT :" . $mobileNo, null, 'saprfc.log');
        $fce->I_MOBILENO = $mobileNo;
        $fce->I_EMAIL = '';
        $fce->Call();


        if ($fce->GetStatus() == SAPRFC_OK) {
            Mage::log("RFC response E_FLAG : " . $fce->E_FLAG, null, 'saprfc.log');
            if ('x' === strtolower($fce->E_FLAG)) {
                return false;
            } else {
                Mage::log("RFC response else ", null, 'saprfc.log');
                $fce->ET_CUST_DET->Reset();
                while ($fce->ET_CUST_DET->Next()) {
                    Mage::log("while customer ", null, 'saprfc.log');
                    $row = $fce->ET_CUST_DET->row;
                    Mage::log("Response Customer info" . $row, null, 'saprfc.log');
                    Mage::log("Get Customer :", null, 'saprfc.log');
                    Mage::log($row, null, 'saprfc.log');
                    Mage::getModel('sap/customer')->createNewCustomer($row);
                }
                $fce->E_COMP->Reset();
                while ($fce->E_COMP->Next()) {
                    $component[] = $fce->E_COMP->row;
                }
                return array('component' => $component, 'customer' => $row);
            }
        }
        Mage::log($fce->PrintStatus(), null, 'saprfc.log');
        $sap->Close();
        return array('component' => null, 'customer' => null);
    }

    /**
     * 
     * Z_WEB_GET_CUST_COMP_DETAILS
     */
    public function getCustmoerComponenetByEmail($email) {
        Mage::log("IMPORT :" . $email, null, 'saprfc.log');
        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        //$component = array();
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            throw new Exception('Could not connect to server');
        }

        $fce = $sap->NewFunction("Z_WEB_GET_CUST_COMP_DETAILS");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            $sap->PrintStatus();
            throw new Exception($sap->PrintStatus());
        }
        Mage::log("IMPORT :" . $email, null, 'saprfc.log');
        $fce->I_MOBILENO = '';
        $fce->I_EMAIL = $email;
        $fce->Call();


        if ($fce->GetStatus() == SAPRFC_OK) {
            Mage::log("RFC response E_FLAG : " . $fce->E_FLAG, null, 'saprfc.log');
            if ('x' === strtolower($fce->E_FLAG)) {
                return false;
            } else {
                Mage::log("RFC response else ", null, 'saprfc.log');
                $fce->ET_CUST_DET->Reset();
                while ($fce->ET_CUST_DET->Next()) {
                    Mage::log("while customer ", null, 'saprfc.log');
                    $row = $fce->ET_CUST_DET->row;
                    Mage::log("Response Customer info" . $row, null, 'saprfc.log');
                    Mage::log("Get Customer :", null, 'saprfc.log');
                    Mage::log($row, null, 'saprfc.log');
                    Mage::getModel('sap/customer')->createNewCustomer($row);
                }
                $fce->E_COMP->Reset();
                while ($fce->E_COMP->Next()) {
                    $component[] = $fce->E_COMP->row;
                }
                return array('component' => $component, 'customer' => $row);
            }
        }
        Mage::log($fce->PrintStatus(), null, 'saprfc.log');
        $sap->Close();
        return array('component' => null, 'customer' => null);
    }

    public function getOptions($data) {
        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $row = array();
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            throw new Exception('Could not connect to server');
        }

        $fce = $sap->NewFunction("ZCRM_AMC_PROD_PRICE");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            $sap->PrintStatus();
            throw new Exception($sap->PrintStatus());
        }
        $fce->I_MPROD = $data['I_MPROD'];
        $fce->I_CUSTOMER = $data['I_CUSTOMER'];
        $fce->I_COMPONENTID = $data['I_COMPONENTID'];
        $fce->Call();

        if ($fce->GetStatus() == SAPRFC_OK) {

            if ('x' === strtolower($fce->E_FLAG)) {
                Mage::log('SAPRFC RETURN : ' . $fce->E_FLAG, null, 'saprfc.log');
                return $row;
            } else {
                $fce->T_AMCPROD_PRICE->Reset();
                while ($fce->T_AMCPROD_PRICE->Next()) {
                    $row[] = $fce->T_AMCPROD_PRICE->row;
                }
            }
        }
        Mage::log($row, null, 'saprfc.log');
        //Mage::log($fce->PrintStatus(), null, 'saprfc.log');
        $sap->Close();
        return $row;
    }

    public function CreateAMC($data) {
        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $item = Mage::getSingleton('sap/session')->getData('selected_item');

        $customer = Mage::getSingleton('sap/session')->getData('customer_comp');
        $cust = $customer['customer'];
        $I_COMPONENTID = Mage::getSingleton('sap/session')->getData('I_COMPONENTID');

        $response['status'] = true;
        $response['message'] = '';
        $data = array(
            'id' => null,
            'customer_code' => $cust['CUSTOMER'],
            'name' => $cust['FIRSTNAME'] . ' ' . $cust['LASTNAME'], //
            'landline' => $cust['LANDLINE'], //
            'email' => $cust['EMAIL'], //
            'mobile' => $cust['MOBILE'], //
            'address' => $cust['HOUSENO'] . ', ' . $cust['STREET1'] . ',' . $cust['STREET2'] . ',' . $cust['STREET4'] . ',' . $cust['CITY'], //
            'pincode' => $cust['PINCODE'], //
            'component' => $I_COMPONENTID,
            'quasarid' => null,
            'main_prod' => $item['MPRODUCT'],
            'service_prod' => $item['AMC_PROD'],
            'service_prod_name' => $item['PROD_DESC'], //
            'service_prod_price' => $item['PRICE'], //
            'contract_duration' => $item['CONT_DURATN'], //
            'start_date' => date('Y-m-d H:i:s'),
            'discount' => $item['DISCOUNT'],
            'grand_total' => number_format($item['PRICE'] - $item['DISCOUNT'], 2), //
            'exec_partner' => $cust['SERVICEBP'],
            'e_contract' => null,
            'e_proc_type' => null,
            'e_start_data' => null,
            'e_end_date' => null,
            'e_flag' => null,
            'e_iccrno' => null,
        );
        Mage::log($data, null, 'saprfc.log');
        $webamc = Mage::getModel('sap/webamc');
        $webamc->setData($data);
        $webamc->save();

        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $row = array();
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            Mage::throwException('Could not connect to server');
        }

        $fce = $sap->NewFunction("ZCRM_WEB_AMC_CREATE");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server not response');
        }


        $p['I_CUSTOMER'] = $cust['CUSTOMER'];
        $p['I_COMPONENT'] = Mage::getSingleton('sap/session')->getData('I_COMPONENTID');
        $p['I_QUASARID'] = $webamc->getId();
        $p['I_MAIN_PROD'] = $item['MPRODUCT'];
        $p['I_SERVICE_PROD'] = $item['AMC_PROD'];
        $p['I_START_DATE'] = $webamc->getStartDate();
        $p['I_EXEC_PARTNER'] = $cust['SERVICEBP'];
        $p['I_DISCOUNT'] = $item['DISCOUNT'];
        Mage::log($p, null, 'saprfc.log');

        $fce->I_CUSTOMER = $cust['CUSTOMER'];
        $fce->I_COMPONENT = Mage::getSingleton('sap/session')->getData('I_COMPONENTID');
        $fce->I_QUASARID = $webamc->getId();
        $fce->I_MAIN_PROD = $item['MPRODUCT'];
        $fce->I_SERVICE_PROD = $item['AMC_PROD'];
        $fce->I_START_DATE = $webamc->getStartDate();
        $fce->I_EXEC_PARTNER = $cust['SERVICEBP'];
        $fce->I_DISCOUNT = $item['DISCOUNT'];
        $fce->Call();

        if ($fce->GetStatus() == SAPRFC_OK) {

            if ('x' === strtolower($fce->E_FLAG)) {

                Mage::log($sap->PrintStatus(), null, 'saprfc.log');
                Mage::throwException('Server response someting is wrong, Please contact');
            }

            $webamcRESPONSE['e_contract'] = $fce->E_CONTRACT;
            $webamcRESPONSE['e_proc_type'] = $fce->E_PROC_TYPE;
            $webamcRESPONSE['e_start_data'] = $fce->E_START_DATE;
            $webamcRESPONSE['e_end_date'] = $fce->E_END_DATE;
            $webamcRESPONSE['e_flag'] = $fce->E_FLAG;
            $webamcRESPONSE['E_ERROR'] = $fce->E_ERROR;
            $webamcRESPONSE['e_iccrno'] = $fce->E_ICCRNO;

            // UPDATE saprfc_webamc table
            $webamc->setEContract($fce->E_CONTRACT);
            $webamc->setEProcType($fce->E_PROC_TYPE);
            $webamc->setEStartDate(date('Y-m-d H:i:s', strtotime($fce->E_START_DATE)));
            $webamc->setEEndDate(date('Y-m-d H:i:s', strtotime($fce->E_END_DATE)));
            $webamc->setEFlag($fce->E_FLAG);
            $webamc->setEIccrno($fce->E_ICCRNO);
            $webamc->save();

            Mage::getSingleton('sap/session')->setData('e_contract', $fce->E_CONTRACT);

            Mage::log("Inserted AMC Code : " . $webamc->getId(), null, 'saprfc.log');
            Mage::log("E_CONTRACT : " . $fce->E_CONTRACT, null, 'saprfc.log');


            return $webamc->getEContract();
        } else {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server status is not OK');
        }
        $sap->Close();
        return $webamc->getId();
    }

    public function amcPayment($data) {
        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $row = array();
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            Mage::throwException('Could not connect to server');
        }

        $fce = $sap->NewFunction("ZEFL_CRM_WEBAMC_PYMT");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server not response');
        }
        Mage::log($data, null, 'saprfc.log');

        $fce->ZWEB_AMC = $data['productinfo'];
        $fce->ZPARTNER = $data['SERVICEBP'];
        $fce->ZCUSTOMER = $data['CUSTOMER'];
        $fce->ZMODEL = $data['MPRODUCT'];
        $fce->ZAMC_PRODUCT = $data['AMC_PROD'];
        $fce->ZAMOUNT = $data['amount'];
        $fce->ZPAYMENT_TYPE = $data['PG_TYPE'];
        $fce->ZWEB_AMC_DATE = date('Y-m-d H:i:s');
        $fce->ZREF_NUMBER = $data['bank_ref_num'];
        $fce->Call();

        if ($fce->GetStatus() == SAPRFC_OK) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::log("Update Success: " . $fce->LV_FLAG, null, 'saprfc.log');
            return true;
        } else {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server status is not OK');
        }
        $sap->Close();
        return $fce->LV_FLAG;
    }

    public function UpdatePersonalDetails($data) {
        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $row = array();
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            Mage::throwException('Could not connect to server');
        }

        $fce = $sap->NewFunction("ZCRM_WEB_AMC_CUST_GA");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server not response');
        }
        Mage::log($data, null, 'saprfc.log');

        $fce->I_CUSTOMER = $data['I_CUSTOMER'];
        $fce->I_TITLE = $data['SERVICEBP'];
        $fce->I_FNAME = $data['lastname'];
        $fce->I_LNAME = $data['firstname'];
        $fce->I_EMAIL = $data['email'];
        $fce->I_LANDLINE = $data['landline'];
        $fce->I_MOBILE = $data['mobile'];
        $fce->I_DOORNO = $data['house_no'];
        $fce->I_ADDR1 = $data['street1'];
        $fce->I_ADDR2 = $data['street3'];
        $fce->I_ADDR2 = $data['bank_ref_num'];
        $fce->I_LANDMARK = $data['state'];
        $fce->I_STATE = $data['pincode'];
        $fce->I_CITY = $data['city'];

        $fce->Call();
        #E_ACTIVITY
        #E_FLAG      
        if ($fce->GetStatus() == SAPRFC_OK) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::log("personal information update Success: " . $fce->E_ACTIVITY, null, 'saprfc.log');



            return true;
        } else {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server status is not OK');
        }
        $sap->Close();
        return $fce->E_FLAG;
    }

    /*
     * 1. Customer not exist
     * 2. Customer exist with multiple record
     * 3. Customer found but component does not exist
     * 4. Customer found with multiple component 
     * 5. Customer found but BP(Business Partner ) resigned
     * 6. Customer found but BP(Business Partner ) is empty
     * 7. Customer found with  multiple BP(Business Partner )
     * 
     */

    public function createWebAmcLead($data) {

        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $response = null;
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            Mage::throwException('Could not connect to server');
        }

        $fce = $sap->NewFunction("ZEFL_WEB_AMC_LEAD");

        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server not response');
        }
        Mage::log($data, null, 'saprfc.log');

        $fce->I_TITLE = $data['I_TITLE'];
        $fce->I_FNAME = $data['I_FNAME'];
        $fce->I_LNAME = $data['I_LNAME'];
        $fce->I_EMAIL = $data['I_EMAIL'];
        $fce->I_LANDLINE = $data['I_LANDLINE'];
        $fce->I_MOBILE = $data['I_MOBILE'];
        $fce->I_DOORNO = $data['I_DOORNO'];
        $fce->I_ADDR1 = $data['I_ADDR1'];
        $fce->I_ADDR2 = $data['I_ADDR2'];
        $fce->I_LANDMARK = $data['I_LANDMARK'];
        $fce->I_STATE = $data['I_STATE'];
        $fce->I_CITY = $data['I_CITY'];
        $fce->I_PINCODE = $data['I_PINCODE'];

        $fce->Call();

        Mage::log("Call function module for web lead :", null, 'saprfc.log');

        if ($fce->GetStatus() == SAPRFC_OK) {
            Mage::log("Response function module for web lead: " . $fce->E_ACTIVITY, null, 'saprfc.log');
            return $fce->E_ACTIVITY;
        } else {
            return false;
            Mage::throwException('Server status is not OK');
        }
        $sap->Close();
        return $fce->E_FLAG;
    }

    public function createNonDbComplaint($data) {
        $name = $data['I_TITLE'] . ' ' . $data['firstname'] . ' ' . $data['lastname'];
        $postData['I_MOBILENO'] = $data['mobile'];
        $postData['STATE'] = $data['I_STATE'];
        $postData['TEXT1'] = [
            'FELD1' => 'Name :' . $name . ', Email: ' . $data['email'],
            'FELD2' => 'Landline :' . $data['landline'] . ', House No : ' . $data['house_no'] . ', Address : ' . $data['street1'] . ',' . $data['street2'] . ', Landmark: ' . $data['street3'],
            'FELD3' => 'State : ' . $data['I_STATE'] . ', City: ' . $data['city'],
            'FELD4' => 'Category :' . $data['category'] . ', Product : ' . $data['product'],
        ];
        $postData['DATE'] = date("Ymd");
        $postData['TIME'] = date('H:i:s');

        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $response = null;
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            Mage::throwException('Could not connect to server');
        }
        $fce = $sap->NewFunction("ZEFL_WEB_COMPLAINT_NONDB");
        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server not response');
        }

        $fce->I_MOBILENO = $postData['I_MOBILENO']; //$data['mobile'];
        $fce->STATE = $postData['STATE']; //$data['I_STATE'];
        $fce->TEXT1 = $postData['TEXT1'];
        $fce->DATE = date('Ymd'); //'20160608'; //$postData['DATE'];
        $fce->TIME = '000000'; //$postData['TIME'];
        //var_dump($fce->TEXT1); die;
        $fce->Call();

        Mage::log($fce, null, 'saprfc.log');

        if ($fce->GetStatus() == SAPRFC_OK) {
            if (empty($fce->COMPLAINT)) {
                Mage::log("Non-DB complaint is not create : ", null, 'saprfc.log');
            } else {
                Mage::log("Non-DB complaint No is : " . $fce->COMPLAINT, null, 'saprfc.log');
            }
            return $fce->COMPLAINT;
        } else {
            return false;
            Mage::throwException('Server status is not OK');
        }
        $sap->Close();
        return $fce->COMPLAINT;
    }

    public function createDbComplaint($data) {
        
        
        //var_dump($data); die;
        require_once(Mage::getBaseDir('lib') . DS . 'saprfc' . DS . 'sap.php');
        $logonInfo = Mage::helper('sap/rfc')->getLogon();
        $response = null;
        $sap = new SAPConnection();
        $sap->Connect($logonInfo);

        if ($sap->GetStatus() == SAPRFC_OK)
            $sap->Open();
        if ($sap->GetStatus() != SAPRFC_OK) {
            Mage::log("Could not connect to server", null, 'saprfc.log');
            Mage::throwException('Could not connect to server');
        }
        $fce = $sap->NewFunction("Z_WEB_CREATE_COMPLAINT");
        if ($fce == false) {
            Mage::log($sap->PrintStatus(), null, 'saprfc.log');
            Mage::throwException('Server not response');
        }
        $array = [
            "GUID" => "",
            "COMPLAINT_NO" => "",
            "BP" => $data['bussiness_patner'],
            "FWD_AGENT" => "",
            "CATEGORY" => "",
            "STATUS" => "",
            "DESCRIPTION" => $data['mobile'],
            "TECHNICIAN" => "",
            "CUSTOMER" => $data['customer_code'],
            "ORDERED_PROD" => $data['product_code'],
            "PRDESCRIPTION" => $data['product_description'],
            "POSTING_DATE" => date('Ymd', strtotime($data['datetime'])),
            "SERV_PROCESS_NO" => "",
            "CONTRACT_NO" => $data['contract_no'],
            "IB_IBASE" => $data['ib_base'],
            "IB_INSTANCE" => $data['ib_instance'],
            "EFL_REASON" => $data['EFL_REASON'],
            "SERV_START_DATE" => "",
            "REQUESTED_DATE" => date('Ymd'),
            "REQUESTED_TIME" => date('his'),
            "DATE2" => "",
            "PLANT" => "",
            "EXTERNAL_REF" => "",
            "ME_ACTION" => "",
            "TEXT1" => "",
            "REMARKS" => $data['remarks']
        ];
        Mage::log($array, null, 'saprfc.log');
        $fce->IS_LIST_DATA = $array;

        $fce->Call();

        Mage::log($fce->IS_LIST_DATA, null, 'saprfc.log');

        if ($fce->GetStatus() == SAPRFC_OK) {

            Mage::log("Response function module Z_WEB_CREATE_COMPLAINT : " . $fce->ET_COMPLAINT, null, 'saprfc.log');
            Mage::log($fceET_RETURN_MSG . $fce->ET_COMPLAINT, null, 'saprfc.log');
            
            return $fce->ET_COMPLAINT;
        } else {
            return false;
            Mage::throwException('Server status is not OK');
        }
        $sap->Close();
        return $fce->COMPLAINT;
    }

    public function getCusromerByEmail($email = '') {

        /* $SAPRFC = new SAPRFC_SAP();
          $logonInfo = Mage::helper('sap/rfc')->getLogon();
          $sap = new SAPConnection();

          $sap->Connect($logonInfo);

          if ($sap->GetStatus() == SAPRFC_OK)
          $sap->Open();
          if ($sap->GetStatus() != SAPRFC_OK) {
          Mage::log("Could not connect to server", null, 'saprfc.log');
          Mage::throwException('Could not connect to server');
          }
          $fce = $sap->NewFunction("Z_WEB_CREATE_COMPLAINT");
          if ($fce == false) {
          Mage::log($sap->PrintStatus(), null, 'saprfc.log');
          Mage::throwException('Server not response');
          }


          $fce->IS_LIST_DATA = $array;

          $fce->Call();

          Mage::log($fce->IS_LIST_DATA, null, 'saprfc.log');

          if ($fce->GetStatus() == SAPRFC_OK) {
          Mage::log("Response function module Z_WEB_CREATE_COMPLAINT : " . $fce->ET_COMPLAINT, null, 'saprfc.log');

          return $fce->ET_COMPLAINT;
          } else {
          return false;
          Mage::throwException('Server status is not OK');
          }
          $sap->Close();
          return $fce->COMPLAINT; */
    }

}
