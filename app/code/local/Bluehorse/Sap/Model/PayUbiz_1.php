<?php

class Bluehorse_Sap_Model_PayUbiz {

    protected $_merchent_key = "JBZaLc";
    protected $_salt = "eCwWELxi";
    private $url = 'https://test.payu.in/';
    private $salt;
    private $params = array();

    const SUCCESS = 1;
    const FAILURE = 0;

    public function getPayUbizUrl() {

        $url = 'https://test.payu.in/';

        return( $url );
    }

    public function call($params) {
        $data = array('key' => 'gtKFFx', 'txnid' => uniqid('animesh_'), 'amount' => rand(0, 100),
            'firstname' => 'Test', 'email' => 'test@payu.in', 'phone' => '98765433210',
            'productinfo' => 'Product Info', 'surl' => 'payment_success', 'furl' => 'payment_failure');


        $host = (isset($_SERVER['https']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        if (isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']))
            $params['surl'] = $host;
        if (isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']))
            $params['furl'] = $host;

        $result = $this->pay($params, $this->_salt);
    }

    function pay($params, $salt) {
        if (!is_array($params))
            throw new Exception('Pay params is empty');

        if (empty($salt))
            throw new Exception('Salt is empty');

        $result = $this->paymentpay($params);
        unset($payment);

        return $result;
    }

    public function paymentpay($params = null) {
        if (is_array($params))
            foreach ($params as $key => $value)
                $this->params[$key] = $value;

        $error = $this->check_params();

        
        if ($error === true) {
            $this->params['hash'] = $this->get_hash($this->params, $this->salt);
            
            $result = $this->curl_call($this->url . '_payment?type=merchant_txn', http_build_query($this->params));
            
            $transaction_id = ($result['curl_status'] === 1) ? $result['result'] : null;

            if (empty($transaction_id))
                return array(
                    'status' => 0,
                    'data' => $result['error']);

            return array(
                'status' => 1,
                'data' => $this->url . '_payment_options?mihpayid=' . $transaction_id);
        } else {
            return array('status' => 0, 'data' => $error);
        }
    }

    private function check_params() {
        if (empty($this->params['key']))
            return $this->error('key');
        if (empty($this->params['txnid']))
            return $this->error('txnid');
        if (empty($this->params['amount']))
            return $this->error('amount');
        if (empty($this->params['firstname']))
            return $this->error('firstname');
        if (empty($this->params['email']))
            return $this->error('email');
        if (empty($this->params['phone']))
            return $this->error('phone');
        if (empty($this->params['productinfo']))
            return $this->error('productinfo');
        if (empty($this->params['surl']))
            return $this->error('surl');
        if (empty($this->params['furl']))
            return $this->error('furl');

        return true;
    }

    public static function get_hash($params, $salt) {
        $posted = array();

        if (!empty($params))
            foreach ($params as $key => $value)
                $posted[$key] = htmlentities($value, ENT_QUOTES);

        $hash_sequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

        $hash_vars_seq = explode('|', $hash_sequence);
        $hash_string = null;

        foreach ($hash_vars_seq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }

        $hash_string .= $salt;
        return strtolower(hash('sha512', $hash_string));
    }

    public static function curl_call($url, $data) {
        $ch = curl_init();
        var_dump($url); die;
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36',
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0));

        $o = curl_exec($ch);

        if (curl_errno($ch)) {
            $c_error = curl_error($ch);

            if (empty($c_error))
                $c_error = 'Server Error';

            return array('curl_status' => 0, 'error' => $c_error);
        }

        $o = trim($o);
        return array('curl_status' => 1, 'result' => $o);
    }

}
