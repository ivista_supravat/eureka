<?php

class Bluehorse_Sap_Model_Info {

    public function getData($_order) {

        //date_default_timezone_set('Asia/Kolkata');

        $order = Mage::getModel('sales/order')->load($_order);

        $orderItenAddress = $order->getBillingAddress();

        $customer = Mage::getModel('customer/customer')->load($order->getData('customer_id'));
        $address = Mage::getModel('customer/address')->load($customer->getDefaultBilling());

        $street = $address->getStreet();

        $items = $order->getAllVisibleItems();
        $itemDetails = [];
        $itm = [];
        foreach ($items as $item):


            $itm['Material'] = $item->getSku();
            $itm['Quantity'] = round($item->getQtyOrdered());
            $itm['FocFlag'] = '';
            $itm['BuyBackInd'] = '';
            $itm['EflMatInd'] = '';
            $itm['CouponDiscount'] = $this->getCouponDiscount();
            $itm['Discount2'] = $this->getDiscount2();
            $itm['Discount3'] = $this->getDiscount3();
            $itm['Discount4'] = $this->getDiscount4();

            $itemDetails[] = $itm;
            unset($itm);
        endforeach;


        $transaction = Mage::getModel('payubiz/paymentdetails');
        $transaction->load($order->getIncrementId(), 'order_id');

        //Zend_Debug::dump($transaction->getData());

        $CustomerInfo = [
            'CustomerCode' => '8000083714', //$customer->getId(),
            'DistributionChannel' => '09',
            'Title' => $customer->getPrefix(),
            'Name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
            'HouseNumber' => 'bis',
            'Street' => (isset($street[0])) ? $street[0] : 'NA',
            'Street2' => (isset($street[1])) ? $street[1] : 'NA',
            'Street3' => (isset($street[2])) ? $street[2] : 'NA',
            'CountryCode' => $address->getCountryId(),
            'City' => $address->getCity(),
            'Region' => Mage::getModel('directory/region')->load($address->getRegionId())->getCode(),
            'PostalCode' => $address->getPostcode(),
            'PaymentType' => $this->getCcBank($order->getBaseTotalDue()),
            'EmployeeCode' => '9053797',
            'AuthorisnNo' => '',
            'TransactionId' => $transaction->getTxnid(),
        ];

        $cust = array();
        for ($i = 0; $i < 2; $i++) :


            $cust[$i] = [
                'CustomerInfo' => $CustomerInfo,
                'CustomerContactInfo' => [
                    '0' => [
                        'Contact' => $customer->getEmail(),
                        'ContactType' => 'E'
                    ],
                    '1' => [
                        'Contact' => $address->getTelephone(),
                        'ContactType' => 'M'
                    ],
                ]
            ];
        endfor;

        $data = [
            'CollectionInfo' => [
                'Customer' => '8000083714', //$customer->getId(),
                'CustCreditAmt' => round($transaction->getAmount()),
                'AuthorisnNo' => '',
                'CcBank' => $this->getCcBank($order->getBaseTotalDue()),
            ],
            'CustomerInfo' => $cust,
            'SalesOrderInfo' => [
                'SoldToParty' => '8000083714', //$customer->getId(),
                'ShipToParty' => '8000083714', //$customer->getId(),
                'EmployeeCode' => '',
                'TransactionId' => $transaction->getTxnid(),
                'TransactionDate' => date('Y-m-d', strtotime($transaction->getCreatedTime())),
                'TransactionTime' => date('His', strtotime(Mage::getSingleton('core/date')->gmtDate())),
                'PaymentType' => $this->getCcBank($order->getBaseTotalDue()),
                'ItemDetails' => $itemDetails,
                'EuroFriend' => '',
                'BuyBackInd' => ''
            ],
            'Source' => 'W'
        ];
        //Mage::log($data, null, 'sap_input_data.log');
        return $data;
    }

    public function getCcBank($val) {
        return ((!$val) ? 'CC' : 'CC' );
    }

    public function getAuthorisnNo($txnid) {
        if ($txnid)
            return substr($txnid, -6);
        return NULL;
    }

    public function getCouponDiscount() {
        return [
            'DiscountType' => '',
            'Amount' => '',
            'Description' => '',
            'CouponCode' => '',
            'CouponDesc' => '',
            'CouponRate' => ''
        ];
    }

    public function getDiscount2() {
        return [
            'DiscountType' => '',
            'Amount' => 0,
            'Description' => '',
            'CouponCode' => '',
            'CouponDesc' => '',
            'CouponRate' => ''
        ];
    }

    public function getDiscount3() {
        return [
            'DiscountType' => '',
            'Amount' => 0,
            'Description' => '',
            'CouponCode' => '',
            'CouponDesc' => '',
            'CouponRate' => ''
        ];
    }

    public function getDiscount4() {
        return [
            'DiscountType' => '',
            'Amount' => 0,
            'Description' => '',
            'CouponCode' => '',
            'CouponDesc' => '',
            'CouponRate' => ''
        ];
    }

}
