<?php

class Bluehorse_Sap_Model_Misc extends Varien_Object {

    const SUCCESS = 1;
    const FAILURE = 0;

    public static function get_hash($params, $salt) {
        $posted = array();

        if (!empty($params))
            foreach ($params as $key => $value)
                $posted[$key] = htmlentities($value, ENT_QUOTES);

        $hash_sequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

        $hash_vars_seq = explode('|', $hash_sequence);
        $hash_string = null;

        foreach ($hash_vars_seq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }

        $hash_string .= $salt;
        return strtolower(hash('sha512', $hash_string));
    }

    public static function reverse_hash($params, $salt, $status) {
        $posted = array();
        $hash_string = null;

        if (!empty($params))
            foreach ($params as $key => $value)
                $posted[$key] = htmlentities($value, ENT_QUOTES);

        $additional_hash_sequence = 'base_merchantid|base_payuid|miles|additional_charges';
        $hash_vars_seq = explode('|', $additional_hash_sequence);

        foreach ($hash_vars_seq as $hash_var)
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] . '|' : '';

        $hash_sequence = "udf10|udf9|udf8|udf7|udf6|udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key";
        $hash_vars_seq = explode('|', $hash_sequence);
        $hash_string .= $salt . '|' . $status;

        foreach ($hash_vars_seq as $hash_var) {
            $hash_string .= '|';
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
        }

        return strtolower(hash('sha512', $hash_string));
    }

    public static function curl_call($url, $data) {

        try {
//            var_dump($url);
//            var_dump($data);
//            die;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36');

            $o = curl_exec($ch);

            if (curl_errno($ch)) {
                //print curl_error($ch);
            }
            //var_dump($o);
            //echo '<pre>';
            //print_r(curl_getinfo($ch));
            //die("------CURL_______");

            $o = trim($o);

            return array('curl_status' => Bluehorse_Sap_Model_Misc::SUCCESS, 'result' => $o);
        } catch (Exception $ex) {
            Mage::throwException($ex->getMessage());
        }
    }

    public static function show_page($result) {
        if ($result['status'] === Bluehorse_Sap_Model_Misc::SUCCESS)
            header('Location:' . $result['data']);
        else
            Mage::throwException("PayUbiz can not return payment url");
        // throw new Exception($result['data']);
    }

    public static function show_reponse($result) {
        var_dump($result['data']); die;
        if ($result['status'] === Bluehorse_Sap_Model_Misc::SUCCESS)
            $result['data']();
        else
            return $result['data'];
    }

}
