<?php

class Bluehorse_Sap_Model_Xmlformater {

    public function arrayToXml($array) {

        if (!is_array($array)) {
            return false;
        }

        // creating object of SimpleXMLElement
        $xml_data = new SimpleXMLElement('<ZEcTransactionCreate></ZEcTransactionCreate>');

        // function call to convert array to xml
        $this->array_to_xml($array, $xml_data);

        //DOMDocument to format code output
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml_data->asXML());

        return $dom->saveXML();
    }

// function defination to convert array to xml
    public function array_to_xml($data, &$xml_data) {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (is_numeric($key)) {
                    $key = 'item'; // . $key; //dealing with <0/>..<n/> issues
                }
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }

}
