<?php

class Bluehorse_Sap_Model_Customer {

    public function createNewCustomer($cust) {

        if (isset($cust['MOBILE']) && !empty($cust['MOBILE'])) {

            $customer = Mage::getModel('customer/customer')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('mobile_no', array('eq' => $cust['MOBILE']))
                    ->load()
                    ->getFirstItem();
        } elseif (isset($cust['EMAIL']) && !empty($cust['EMAIL'])) {

            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
            $customer->loadByEmail($cust['EMAIL']);
        }

        try {
            $randomPassword = 'password123';
            $name = $cust['FIRSTNAME'];
            $temp = explode(" ", $name);
            $firstname = $temp[0];
            $lastname = $temp[1];
            $email = '';

            Mage::log("Magento found a customer " . $customer->getId(), null, 'saprfc.log');



            if (null == $customer->getId()) {

                $customer->setId(null)
                        ->setGroupId(4)
                        ->setSkipConfirmationIfEmail($customer->getEmail())
                        ->setFirstname($firstname)
                        ->setLastname($lastname)
                        ->setMobileNo($cust['MOBILE'])
                        ->setEmail($cust['EMAIL'])
                        ->setPassword($randomPassword)
                        ->setPasswordConfirmation($randomPassword);
                $errors = array();
                $validationCustomer = $customer->validate();

                if (is_array($validationCustomer)) {
                    $errors = array_merge($validationCustomer, $errors);
                }
                $validationResult = count($errors) == 0;
                if (true === $validationResult) {

                    $customer->save();
                    $autoregister_array["customerSaved"] = true;
                    $customer->sendNewAccountEmail();
                    Mage::log("New customer created", null, 'saprfc.log');
                    $this->createAddress($cust, $customer->getId());
                }
            } elseif (null === $customer->getEmail()) {
                
            } else {

                $customer->setId($customer->getId())
                        ->setFirstname($firstname)
                        ->setLastname($lastname)
                        ->setEmail($customer->getEmail());

                $errors = array();
                $validationCustomer = $customer->validate();

                if (is_array($validationCustomer)) {
                    $errors = array_merge($validationCustomer, $errors);
                }
                $validationResult = count($errors) == 0;
                if (true === $validationResult) {

                    $customer->save();
                    $autoregister_array["customerSaved"] = true;
                    //$customer->sendNewAccountEmail();
                    //$this->_getSession()->setCustomer($customer)->addSuccess($this->__('The account information has been saved.'));
                    Mage::log("Update customer", null, 'saprfc.log');
                }
            }
        } catch (Exception $ex) {
            Zend_Debug::dump($ex->getMessage());
        }
        $this->loginCustomerById($customer->getId());
        return true;
    }

    function loginCustomerById($customerId) {


        $customer = Mage::getModel('customer/customer')
                ->load($customerId);
        return Mage::getSingleton('customer/session')->loginById($customer->getId());
        throw new Exception('Login failed');
    }

    public function create($cust) {

        $autoregister_array["customerSaved"] = false;
        $email = '';
        // Checking requested customer information empty 
        if (count($cust) < 1) {
            return false;
        }
        if (isset($cust['EMAIL']) && !empty($cust['EMAIL'])) {
            return Mage::getSingleton('core/session')->addError('Customer email does not exist');
        }
        // Checking customer exist
        if (isset($cust['EMAIL'])) {
            $email = (empty($cust['EMAIL'])) ? $this->generateRandomString(6) : $cust['EMAIL'];
            $customer = Mage::getModel('customer/customer')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('mobile_no', array('eq' => $cust['MOBILE']))
                    ->load()
                    ->getFirstItem();

            //$customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($customer->getEmail());

            if (empty($customer->getEmail())) {

                $randomPassword = 'password123';
                $name = $cust['FIRSTNAME'];
                $temp = explode(" ", $name);
                $firstname = $temp[0];
                $lastname = $temp[1];
                $customer->setId(null)
                        ->setSkipConfirmationIfEmail($email)
                        ->setPrefix('Sir')
                        ->setFirstname($firstname)
                        ->setLastname($lastname)
                        ->setMobileNo($cust['MOBILE'])
                        ->setEmail($email)
                        ->setPassword($randomPassword)
                        ->setPasswordConfirmation($randomPassword);
                $errors = array();
                $validationCustomer = $customer->validate();

                if (is_array($validationCustomer)) {
                    $errors = array_merge($validationCustomer, $errors);
                }
                $validationResult = count($errors) == 0;
                if (true === $validationResult) {
                    $customer->save();
                    $autoregister_array["customerSaved"] = true;
                    $customer->sendNewAccountEmail();


                    $this->createAddress($cust, $customer->getId());
                } else {
                    Mage::log($errors);
                }
            } else {

                $randomPassword = 'password123';
                $name = $cust['FIRSTNAME'];
                $temp = explode(" ", $name);
                $firstname = $temp[0];
                $lastname = $temp[1];
                $customer->setId($customer->getId())
                        ->setSkipConfirmationIfEmail($email)
                        ->setPrefix('Sir')
                        ->setFirstname($firstname)
                        ->setLastname($lastname)
                        ->setMobileNo($cust['MOBILE'])
                        ->setEmail($email)
                        ->setPassword($randomPassword)
                        ->setPasswordConfirmation($randomPassword);
                $errors = array();
                $validationCustomer = $customer->validate();

                if (is_array($validationCustomer)) {
                    $errors = array_merge($validationCustomer, $errors);
                }
                $validationResult = count($errors) == 0;
                if (true === $validationResult) {
                    $customer->save();
                    $autoregister_array["customerSaved"] = true;
                    $customer->sendNewAccountEmail();
                } else {
                    Mage::log($errors);
                }
            }
            $this->loginById($customer->getId());
            return;
        }
    }

    public function createAddress($_customer, $cid) {

        $regionModel = Mage::getModel('directory/region')->loadByCode($_customer['REGION'], $_customer['COUNTRY']);
        $regionId = $regionModel->getId();
        $name = $_customer['FIRSTNAME'];
        $temp = explode(" ", $name);
        $firstname = $temp[0];
        $lastname = $temp[1];
        $_custom_address = array(
            'firstname' => $firstname,
            'lastname' => $lastname,
            'street' => array($_customer['STREET1'], $_customer ['STREET2'] . '-' . $_customer ['STREET3'] . '-' . $_customer['STREET4']),
            'city' => $_customer['CITY'],
            'region_id' => $regionId,
            'company' => '',
            'postcode' => $_customer['PINCODE'],
            'country_id' => $_customer['COUNTRY'],
            'telephone' => $_customer['MOBILE'],
            'fax' => '',
            'email' => $_customer['EMAIL'],
            'house_no' => $_customer['HOUSENO'],
            'landmark' => ''
        );

        $customAddress = Mage::getModel('customer/address');
        $customAddress->setData($_custom_address)
                ->setCustomerId($cid)
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');
        // this is the most important part->setIsDefaultBilling(1)  // set as default for billing->setIsDefaultShipping(1) // set as default for shipping->setSaveInAddressBook(1);
        try {
            $customAddress->save();
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }

        return;
    }

    /*
     * Update loggod customer's personal information & default address information
     * @params array
     * @return boolean
     */
    /*
      public function updateInfo($data) {

      if (is_array($data) && !empty($data)) {


      $customerData = Mage::getSingleton('customer/session')->getCustomer();
      $customer_Id = $customerData->getId();

      $customer = Mage::getModel('customer/customer');
      $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
      $customer->load($customer_Id);

      // update customer personal information
      if ($customer->getId()) {
      $customer->setFirstname($data['lastname']);
      $customer->setLastname($data['lastname']);
      $customer->setEmail($data['email']);
      $customer->setMobileNo($data['mobile']);
      $customer->save();
      }

      // update customer's  default billing & shipping address

      $customer->getDefaultBilling();
      $customer->getDefaultShipping();

      $addressData = array(
      'postcode' => 123456,
      'country' => 'US',
      'firstname' => 'supravat',
      'lastname' => 'mondal',
      'telephone' => '9564705218',
      'region_id' => '516',
      );

      if ($customer->getDefaultBilling()) {
      $address = Mage::getModel('customer/address')->load($customer->getDefaultBilling());
      echo "BEFORE UPDATE";
      var_dump($address->getData());

      $address->setCustomerId($address->getCustomer()->getId());
      foreach ($addressData as $addressCode => $addressValue) {
      if (isset($addressData[$addressCode])) {
      $address->setData($addressCode, $addressData[$addressValue]);
      }
      }
      try {
      $address->setId($customer->getDefaultShipping());
      $address->save();
      } catch (Mage_Core_Exception $e) {
      echo $e->getMessage();
      }
      }
      echo "AFTER UPDATE";
      $address = Mage::getModel('customer/address')->load($customer->getDefaultBilling());
      var_dump($address->getData());
      die('Update is successful;');
      Zend_debug::dump($customer->getData());
      Zend_debug::dump($data);
      die("customer information ");
      }

      return false;
      }
     * 
     */
}
