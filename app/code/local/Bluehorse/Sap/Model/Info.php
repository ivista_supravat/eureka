<?php

class Bluehorse_Sap_Model_Info {

    public function getData($_order) {

        //date_default_timezone_set('Asia/Kolkata');

        $order = Mage::getModel('sales/order')->load($_order);

        $orderItenAddress = $order->getBillingAddress();
//Zend_Debug::dump($orderItenAddress->getData()); 
        $payment = $order->getPayment();

        $paymentType = $payment->getMethodInstance()->getCode();

        $customer = Mage::getModel('customer/customer')->load($order->getData('customer_id'));
        $address = Mage::getModel('customer/address')->load($customer->getDefaultBilling());
        //Zend_Debug::dump($order->getData('customer_id')); die;
        // Zend_Debug::dump($address); die;
        $street = $orderItenAddress->getStreet();
        $items = $order->getAllVisibleItems();

        $itemDetails = [];

        $itm = [];
        foreach ($items as $item):

            $duscountArray = explode(",", $item->getAppliedRuleIds());

            $first_rule = (isset($duscountArray[0])) ? $duscountArray[0] : 0;
            $second_rule = (isset($duscountArray[1])) ? $duscountArray[1] : 0;
            $third_rule = (isset($duscountArray[2])) ? $duscountArray[2] : 0;
            $four_rule = (isset($duscountArray[3])) ? $duscountArray[3] : 0;

            $itm['Material'] = $item->getSku();
            $itm['Quantity'] = round($item->getQtyOrdered());
            $itm['FocFlag'] = '';
            $itm['BuyBackInd'] = '';
            $itm['EflMatInd'] = '';
            $itm['CouponDiscount'] = $this->getCouponDiscount($first_rule);
            $itm['Discount2'] = $this->getCouponDiscount($second_rule);
            $itm['Discount3'] = $this->getCouponDiscount($third_rule);
            $itm['Discount4'] = $this->getCouponDiscount($four_rule);


            $itemDetails['item'] = $itm;
            unset($itm);
        endforeach;

        $transaction = Mage::getModel('payubiz/paymentdetails');
        $transaction->load($order->getIncrementId(), 'order_id');
        $data = [
            'CollectionInfo' => array(
                'Customer' => $order->getSoldToParty(), //$customer->getId(),
                'CustCreditAmt' => (float) number_format((float) $transaction->getAmount(), 2, '.', ''),
                'AuthorisnNo' => '',
                'CcBank' => $this->getCcBank($order->getBaseTotalDue()),
            ),
            'CustomerInfo' => array(
                'item' => array(
                    array(
                        'CustomerInfo' => array(
                            'CustomerCode' => $order->getSoldToParty(),
                            'DistributionChannel' => '09',
                            'Title' => (null == $orderItenAddress->getPrefix()) ? 'Mr' : $orderItenAddress->getPrefix(),
                            'Name' => $orderItenAddress->getFirstname() . ' ' . $orderItenAddress->getLastname(),
                            'HouseNumber' => ($orderItenAddress->getHouseNo()) ? $orderItenAddress->getLandmark() : 'NA',
                            'Street' => (isset($street[0])) ? $street[0] : 'NA',
                            'Street2' => (isset($street[1])) ? $street[1] : 'NA',
                            'Street3' => ($orderItenAddress->getLandmark()) ? $orderItenAddress->getLandmark() : 'NA',
                            'CountryCode' => $orderItenAddress->getCountryId(),
                            'City' => $orderItenAddress->getCity(),
                            'Region' => Mage::getModel('directory/region')->load($orderItenAddress->getRegionId())->getCode(),
                            'PostalCode' => $orderItenAddress->getPostcode(),
                            'PaymentType' => $this->getPaymentType($paymentType),
                            'EmployeeCode' => '',
                            'AuthorisnNo' => '',
                            'TransactionId' => $transaction->getTxnid()
                        ),
                        'CustomerContactInfo' => array(
                            'item' => array(
                                array(
                                    'Contact' => $customer->getEmail(),
                                    'ContactType' => 'E'
                                ),
                                array(
                                    'Contact' => $order->getBillingAddress()->getTelephone(),
                                    'ContactType' => 'M'
                                ),
                            ),
                        ),
                    ),
                    array(
                        'CustomerInfo' => array(
                            'CustomerCode' => $order->getSoldToParty(),
                            'DistributionChannel' => '09',
                            'Title' => (null == $orderItenAddress->getPrefix()) ? 'Mr' : $orderItenAddress->getPrefix(),
                            'Name' => $orderItenAddress->getFirstname() . ' ' . $orderItenAddress->getLastname(),
                            'HouseNumber' => ($orderItenAddress->getHouseNo()) ? $orderItenAddress->getLandmark() : 'NA',
                            'Street' => (isset($street[0])) ? $street[0] : 'NA',
                            'Street2' => (isset($street[1])) ? $street[1] : 'NA',
                            'Street3' => ($orderItenAddress->getLandmark()) ? $orderItenAddress->getLandmark() : 'NA',
                            'CountryCode' => $orderItenAddress->getCountryId(),
                            'City' => $orderItenAddress->getCity(),
                            'Region' => Mage::getModel('directory/region')->load($orderItenAddress->getRegionId())->getCode(),
                            'PostalCode' => $orderItenAddress->getPostcode(),
                            'PaymentType' => $this->getPaymentType($paymentType),
                            'EmployeeCode' => '',
                            'AuthorisnNo' => '',
                            'TransactionId' => $transaction->getTxnid()
                        ),
                        'CustomerContactInfo' => array(
                            'item' => array(
                                array(
                                    'Contact' => $customer->getEmail(),
                                    'ContactType' => 'E'
                                ),
                                array(
                                    'Contact' => $order->getBillingAddress()->getTelephone(),
                                    'ContactType' => 'M'
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            'SalesOrderInfo' => [
                'SoldToParty' => $order->getSoldToParty(), 
                'ShipToParty' => $order->getShipToParty(),
                'EmployeeCode' => '',
                'TransactionId' => $transaction->getTxnid(),
                'TransactionDate' => date('Y-m-d', strtotime($transaction->getCreatedTime())),
                'TransactionTime' => date('His', strtotime(Mage::getSingleton('core/date')->gmtDate())),
                'PaymentType' => $this->getPaymentType($paymentType),
                'ItemDetails' => $itemDetails,
                'EuroFriend' => '',
                'BuyBackInd' => ''
            ],
            'Source' => 'W'
        ];
        //Mage::log($data, null, 'sap_input_data.log');
        return $data;
    }

    public function getCcBank($val) {
        return (($val) ? 'CC' : 'FF' );
    }

    public function getAuthorisnNo($txnid) {
        if ($txnid)
            return substr($txnid, -6);
        return NULL;
    }

    public function getCouponDiscount($id) {

        if (0 < $id) {
            $oRule = Mage::getModel('salesrule/rule')->load($id);

            return [
                'DiscountType' => 'C',
                'Amount' => $oRule->getDiscountAmount(),
                'Description' => (!empty($oRule->getDescription())) ? $oRule->getDescription() : '',
                'CouponCode' => (!empty($oRule->getCouponCode())) ? $oRule->getCouponCode() : '',
                'CouponDesc' => (!empty($oRule->getName())) ? $oRule->getName() : '',
                'CouponRate' => (!empty($oRule->getDiscountAmount())) ? $oRule->getDiscountAmount() : '',
            ];
        }
        return [
            'DiscountType' => '',
            'Amount' => 0,
            'Description' => '',
            'CouponCode' => '',
            'CouponDesc' => '',
            'CouponRate' => ''
        ];
    }

    public function getPaymentType($code) {

        if ('cashondelivery' == $code) {
            return "C";
        } else {
            return "F";
        }
        return "C";
    }

    public function getNextCustomerCode($id = 0) {
        if (0 < $id)
            return $id;
        return 8000083716 + 1;
    }

}
