<?php

class Bluehorse_Sap_Model_Observer {

    public function __contruct() {
        
    }

    /*
     * Customer save before process checkout button click
     */

    public function autoRegisterBilling($evt) {

        if (!Mage::getSingleton('sap/config')->isEnabled()) {
            return;
        }
        if (!Mage::helper('customer')->isLoggedIn()) {
            $data = $evt->getEvent()->getControllerAction()->getRequest()->getPost('billing', array());
            $_custom_address = $data;


            $customer = Mage::getModel("customer/customer");
            $email = $data['email'];
            $websiteId = Mage::app()->getWebsite()->getId();
            $store = Mage::app()->getStore();
            $pwd = $data['customer_password'];
            $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);
 Mage::log($data, null, 'SOAP.log');
            if (!$customer->getId()) {
                //Code begins here for new customer registration
                $customer->website_id = $websiteId;
                $customer->setStore($store);
                $customer->firstname = $data['firstname'];
                $customer->lastname = $data['lastname'];
                $customer->mobile_no = $_customer['telephone'];
                $customer->setEmail($email);
                $customer->setPassword($pwd);
                $customer->sendNewAccountEmail('confirmed');
                $customer->save();

                $customAddress = Mage::getModel('customer/address');
                //$customAddress = new Mage_Customer_Model_Address();
                $customAddress->setData($_custom_address)
                        ->setCustomerId($customer->getId())
                        ->setIsDefaultBilling('1')
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');
                $customAddress->save();
            }
            // to login that customer.
            Mage::getSingleton('customer/session')->loginById($customer->getId());
        }
    }

    public function registerGuestUser($observer) {

        $order = $observer->getEvent()->getOrder();
        $autoregister_array["customerSaved"] = false;
        $email = $order->getCustomerEmail();
        $firstname = $order->getCustomerFirstname();
        $lastname = $order->getCustomerLastname();

        $customer = Mage::getModel('customer/customer');

        $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);


        if (!$customer->getId()) {

            $randomPassword = 'password123';
            $customer->setId(null)
                    ->setSkipConfirmationIfEmail($email)
                    ->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setEmail($email)
                    ->setMobileNo('9153515478')
                    ->setPassword($randomPassword)
                    ->setPasswordConfirmation($randomPassword);

            $errors = array();
            $validationCustomer = $customer->validate();

            if (is_array($validationCustomer)) {
                $errors = array_merge($validationCustomer, $errors);
            }

            $validationResult = count($errors) == 0;

            if (true === $validationResult) {

                $customer->save();
                $autoregister_array["customerSaved"] = true;
                $customer->sendNewAccountEmail();
            } else {
                Mage::log($errors);
            }
            $billingaddress = $order->getBillingAddress()->getData();
            $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);
            $this->saveBilling($billingaddress, $customer->getId());
        }
        Mage::getSingleton('core/session')->setCustomerCode($customer->getId());
    }

    public function saveBilling($_customer, $cid) {
        // set billing address
        //var_dump($_customer); die;
        $_custom_address = array(
            'firstname' => $_customer['firstname'],
            'lastname' => $_customer['lastname'],
            'street' => $_customer['street'],
            'city' => $_customer['city'],
            'region_id' => $_customer['region_id'],
            'company' => $_customer['company'],
            'postcode' => $_customer['postcode'],
            'country_id' => $_customer['country_id'],
            'telephone' => $_customer['telephone'],
            'fax' => $_customer['fax'],
            'email' => $_customer['email'],
            'house_no' => $_customer['house_no'],
            'landmark' => $_customer['landmark']
        );

        $customAddress = Mage::getModel('customer/address');
        $customAddress->setData($_custom_address)
                ->setCustomerId($cid) // this is the most important part
                ->setIsDefaultBilling('1')  // set as default for billing
                ->setIsDefaultShipping('1') // set as default for shipping
                ->setSaveInAddressBook('1');
        try {
            $customAddress->save();
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }

        return;
    }

    public function postDataTosServer(Varien_Event_Observer $observer) {
        $orderIds = $observer->getData('order_ids');

        foreach ($orderIds as $_orderId) {
            Mage::log("ORDER ID : " . $_orderId, null, 'SOAP.log');
            $order = Mage::getModel('sales/order')->load($_orderId);
            $customer = Mage::getModel('customer/customer')->load(Mage::getSingleton('core/session')->getCustomerCode());

            $order->setCustomerId($customer->getId());
            $order->setCustomerFirstname($customer->getFirstname());
            $order->setCustomerLastname($customer->getLastname());
            $order->setCustomerEmail($customer->getEmail());
            $order->setCustomerGroupId(1);
            $order->setSustomerIsGuest(0);
            $preOrder = $_orderId - 1;
            Mage::log("PRE-ORDER ID : " . $preOrder, null, 'SOAP.log');
            $previousOrder = Mage::getModel('sales/order')->load($preOrder);
            Mage::log("PRE-CUST-CODE : " . $previousOrder->getSoldToParty(), null, 'SOAP.log');
            $partyCode = $previousOrder->getSoldToParty() + 1;
            Mage::log("POST-CUST-CODE : " . $partyCode, null, 'SOAP.log');
            // generate Customer Code
            $order->setSoldToParty($partyCode);
            $order->setShipToParty($partyCode);
            try {
                $order->save();
                //$updated_order = Mage::getModel('sales/order')->load($_orderId);
                //Zend_Debug::dump($order);
                //die;
                $this->exportRecentOrder($_orderId);
            } catch (Exception $e) {
                Mage::log(Mage::helper('sap')->__('EXPORT DATA ERROR: ' . $e->getMessage()), null, 'SOAP.log');
            }
        }
    }

    public function exportRecentOrder($_order) {
        if (!Mage::getSingleton('sap/config')->isStatus()) {
            return;
        }

        try {
            $importParams = Mage::getSingleton('sap/info')->getData($_order);

            Mage::log($importParams, null, 'SOAP.log');
            ini_set('default_socket_timeout', 90000);
            $client = new SoapClient(Mage::getSingleton('sap/config')->getSoapUrl(), [
                'Client' => Mage::getSingleton('sap/config')->getSoapClient(),
                'UserName' => Mage::getSingleton('sap/config')->getSoapUsername(),
                'Password' => Mage::getSingleton('sap/config')->getSoapPassword(),
                'exceptions' => true,
                'trace' => true,
                'cache_wsdl' => WSDL_CACHE_BOTH,
                'keep_alive' => false,
                'connection_timeout' => 90000
            ]);
            $response = $client->ZEcTransactionCreate($importParams);
            $responseToArray = json_decode(json_encode($response), true);
            
            if (isset($responseToArray["Response"]["Document"]) && !empty($responseToArray["Response"]["Document"])) {
                $order = Mage::getModel('sales/order')->load($_order);
                $order->setSapId($responseToArray["Response"]["Document"]);
                $order->setStatus('pending_payment');
                $order->save();
                Mage::log("Success", null, 'SOAP.log');
            }

            Mage::log($responseToArray["Response"]["Document"], null, 'SOAP.log');
            return;
        } catch (SOAPFault $ex) {
            Mage::log(Mage::helper('sap')->__('SOAP ERROR' . $ex->getMessage()), null, 'SOAP.log');
            Mage::getSingleton('core/session')->addError(Mage::helper('sap')->__('SOAP ERROR: ' . $ex->getMessage()));
            return;
        }
    }

}
