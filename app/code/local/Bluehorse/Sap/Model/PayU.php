<?php

class Bluehorse_Sap_Model_PayU {

    protected $_merchent_key = "JBZaLc";
    protected $_salt = "GQs7yium";

    public function getPayUbizUrl() {

        $url = 'https://test.payu.in/_payment';

        return( $url );
    }

    public function FormFields() {

        // Variable initialization

        $customerInfo = Mage::getSingleton('sap/session')->getData('customer_comp');
        $ProductInfo = Mage::getSingleton('sap/session')->getData('selected_item');

        $customerInfo = $customerInfo['customer'];

        $transaction_mode = 'test';
        $merchant_key = 'gtKFFx';
        $salt = 'eCwWELxi';
        $payment_gateway = Mage::getSingleton('sap/session')->getData('pg');

        $bankcode = ''; //$this->getConfigData('bankcode');
        $txnid = rand(1111111111, 9999999999);

        $description = $ProductInfo['PROD_DESC'];
        $countryName = Mage::getModel('directory/country')->load('IN')->getName();

        $customer = Mage::getModel('customer/customer')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('mobile_no', array('eq' => $customerInfo['MOBILE']))
                ->load()
                ->getFirstItem();

        $calculatedAmount_INR = round($this->getTotalAmount($ProductInfo), 2);

        // Construct data for the form
        $data = array(
            // Merchant details
            'key' => $merchant_key,
            'txnid' => $txnid,
            'amount' => $calculatedAmount_INR,
            'productinfo' => $description,
            // Buyer details
            'firstname' => $customerInfo['FIRSTNAME'],
            'Lastname' => ($customerInfo['LASTNAME']) ? $customerInfo['LASTNAME'] : "Not Set",
            'City' => $customerInfo['CITY'],
            'State' => $customerInfo['REGION'],
            'Country' => $countryName,
            'Zipcode' => $customerInfo['PINCODE'],
            'email' => ($customerInfo['EMAIL']) ? $customerInfo['EMAIL'] : 'supravat.mondal@ivistasolutions.com',
            'phone' => $customerInfo['MOBILE'],
            'surl' => $this->getSuccessUrl(),
            'furl' => $this->getfailureUrl(),
            'curl' => $this->getCancelUrl(),
        );
        $data['pg'] = $payment_gateway;
        $data['bankcode'] = $bankcode;

        $data['Hash'] = strtolower(hash('sha512', $merchant_key . '|' . $txnid . '|' . $data['amount'] . '|' .
                        $data['productinfo'] . '|' . $data['firstname'] . '|' . $data['email'] . '|||||||||||' . $salt));
        
        if (PB_DEBUG) {

            Mage::log('payubiz' . print_r($data), null, 'payubiz.log');
        }
        return( $data );
    }

    public function getTotalAmount($item) {

        return $item['PRICE'] - $item['DISCOUNT'];
    }

    public function getSuccessUrl() {

        return Mage::getUrl('payubiz/redirect/success', array('_secure' => true));
    }

    public function getCancelUrl() {
        return Mage::getUrl('payubiz/redirect/cancel', array('_secure' => true));
    }

    public function getfailureUrl() {

        return Mage::getUrl('payubiz/redirect/failure', array('_secure' => true));
    }

}
