<?php

class Bluehorse_Sap_Model_Mysql4_Emaillog extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init("sap/emaillog", "id");
    }
}
