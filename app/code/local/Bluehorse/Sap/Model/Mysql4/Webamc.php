<?php

class Bluehorse_Sap_Model_Mysql4_Webamc extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init("sap/webamc", "id");
    }

    protected function _beforeSave($object) {

        $cid = 'Guest';
        $collection = Mage::getModel('sap/webamc')
                ->getCollection()
                ->setOrder('quasarid', 'DESC')
                ->setPageSize(1);
        $webamc = $collection->getLastItem();
        $quasarid = $webamc->getQuasarid() + 1;

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $cid = $customerData->getId();
        }
        $object->setData('quasarid', $quasarid);
        $object->setData('customer_id', $cid);

        return parent::_beforeSave();
    }

}
