<?php

class Bluehorse_Sap_Model_Mysql4_Webamc_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('sap/webamc');
    }

    public function count() { //inherited from Varien_Data_Collection
        $this->load();
        return count($this->_items);
    }

}
