<?php

class Bluehorse_Sap_Model_PayUbiz extends Varien_Object {

    const KEY = 'gtKFFx';
    CONST SALT = 'eCwWELxi';

    /**
     * Returns the pay page url or the merchant js file.
     * 
     * @param unknown $params        	
     * @param unknown $salt        	
     * @throws Exception
     * @return Ambigous <multitype:number string , multitype:number Ambigous <boolean, string> >
     */
    function pay($params, $salt) {
        if (!is_array($params))
            throw new Exception('Pay params is empty');

        if (empty($salt))
            throw new Exception('Salt is empty');

        $payment = new Bluehorse_Sap_Model_Payment($salt);
        $result = $payment->pay($params);
        unset($payment);

        return $result;
    }

    /**
     * Displays the pay page.
     * 
     * @param unknown $params        	
     * @param unknown $salt        	
     * @throws Exception
     */
    function payUpage($params, $salt = 'eCwWELxi') {
        if (count($_POST) && isset($_POST['mihpayid']) && !empty($_POST['mihpayid'])) {
            $_POST['surl'] = $params['surl'];
            $_POST['furl'] = $params['furl'];

            $result = $this->response($_POST, $salt);
            Bluehorse_Sap_Model_Misc::show_reponse($result);
        } else {
            $host = (isset($_SERVER['https']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

            if (isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']))
                $params['surl'] = $this->getSuccessUrl();
            if (isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']))
                $params['furl'] = $this->getfailureUrl();

            $result = $this->pay($params, $salt);

            Bluehorse_Sap_Model_Misc::show_page($result);
        }
    }

    /**
     * Returns the response object.
     * 
     * @param unknown $params        	
     * @param unknown $salt        	
     * @throws Exception
     * @return number
     */
    function response($params, $salt) {

        if (!is_array($params))
            throw new Exception('PayU response params is empty');

        if (empty($salt))
            throw new Exception('Salt is empty');

        if (empty($params['status']))
            throw new Exception('Status is empty');

        $response = new Bluehorse_Sap_Model_Response($salt);
        $result = $response->get_response($_POST);
        Mage::getSingleton('sap/session')->setData('payment_information', $_POST);
        unset($response);

        return $result;
    }

    public function getResponseOperation($response) {

        if (isset($response['status'])) {

            $webamc = Mage::getModel('sap/webamc')->load($response['productinfo'], 'e_contract');
            $item = Mage::getSingleton('sap/session')->getData('selected_item');
            $CUSTOMER = Mage::getSingleton('sap/session')->getData('customer_comp');
            $CUST = $CUSTOMER['customer'];

            $paymentInfo['ZWEB_AMC'] = $response['productinfo'];
            $paymentInfo['ZPARTNER'] = $CUST['SERVICEBP'];
            $paymentInfo['ZCUSTOMER'] = $CUST['CUSTOMER'];
            $paymentInfo['ZMODEL'] = $item['MPRODUCT'];
            $paymentInfo['ZAMC_PRODUCT'] = $item['AMC_PROD'];
            $paymentInfo['ZAMOUNT'] = $response['amount'];
            $paymentInfo['ZPAYMENT_TYPE'] = $response['PG_TYPE'];
            $paymentInfo['ZWEB_AMC_DATE'] = date('Y-m-d H:i:s');
            $paymentInfo['ZREF_NUMBER'] = $response['bank_ref_num'];

            $f = Mage::getSingleton('sap/saprfc')->amcPayment($paymentInfo);
            return $f;
        }
    }

    public function getSuccessUrl() {

        return Mage::getUrl('customer-service/redirect/success', array('_secure' => true));
    }

    public function getCancelUrl() {
        return Mage::getUrl('customer-service/redirect/cancel', array('_secure' => true));
    }

    public function getfailureUrl() {

        return Mage::getUrl('customer-service/redirect/failure', array('_secure' => true));
    }

    public function captureFields($data) {

        $item = Mage::getSingleton('sap/session')->getData('selected_item');
        $customer = Mage::getSingleton('sap/session')->getData('customer');

        $data = array(
            'key' => self::KEY,
            'txnid' => uniqid(),
            'amount' => $item['PRICE'],
            'firstname' => $customer['FIRSTNAME'],
            'email' => (empty($customer['EMAIL'])) ? 'supravat.mondal@ivistasolutions.com' : $customer['EMAIL'],
            'phone' => $customer['MOBILE'],
            'productinfo' => $item['PRICE'],
            'surl' => $this->getSuccessUrl(),
            'furl' => $this->getfailureUrl()
        );

        return $data;
    }

}
