<?php

class Bluehorse_Sap_Model_Config {

    const XML_PATH_ENABLED = 'customer/guestregister/enabled';
    const XML_PATH_STATUS = 'sap/general/status';
    const XML_PATH_SOAP_CLIENT = 'sap/general/api_client';
    const XML_PATH_SOAP_URL = 'sap/general/service_url';
    const XML_PATH_SOAP_USERNAME = 'sap/general/api_user';
    const XML_PATH_SOAP_PASSWORD = 'sap/general/api_key';
    /* SAP RFC Configarations */
    const XML_PATH_SOAPRFC_ASHOST = 'rfc/generalseoption/ashost';
    const XML_PATH_SOAPRFC_SYSNR = 'rfc/generalseoption/sysnr';
    const XML_PATH_SOAPRFC_CLIENT = 'rfc/generalseoption/client';
    const XML_PATH_SOAPRFC_USER = 'rfc/generalseoption/rfcusername';
    const XML_PATH_SOAPRFC_PASSWD = 'rfc/generalseoption/rfcpassword';

    public function isEnabled($storeId = null) {
        if (Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $storeId)) {
            return true;
        }
        return false;
    }

    public function isStatus($storeId = null) {
        if (Mage::getStoreConfigFlag(self::XML_PATH_STATUS, $storeId)) {
            return Mage::getStoreConfigFlag(self::XML_PATH_STATUS, $storeId);
        }
        return false;
    }

    public function getSoapClient() {
        return Mage::getStoreConfig(self::XML_PATH_SOAP_CLIENT);
    }

    public function getSoapUrl() {
        return Mage::getStoreConfig(self::XML_PATH_SOAP_URL);
    }

    public function getSoapUsername() {
        return Mage::getStoreConfig(self::XML_PATH_SOAP_USERNAME);
    }

    public function getSoapPassword() {
        return Mage::getStoreConfig(self::XML_PATH_SOAP_PASSWORD);
    }

    public function getSaprfcAshost() {
        return Mage::getStoreConfig(self::XML_PATH_SOAPRFC_ASHOST);
    }

    public function getSaprfcSysnr() {
        return Mage::getStoreConfig(self::XML_PATH_SOAPRFC_SYSNR);
    }

    public function getSaprfcClient() {
        return Mage::getStoreConfig(self::XML_PATH_SOAPRFC_CLIENT);
    }

    public function getSaprfcUser() {
        return Mage::getStoreConfig(self::XML_PATH_SOAPRFC_USER);
    }

    public function getSaprfcPasswd() {
        return Mage::getStoreConfig(self::XML_PATH_SOAPRFC_PASSWD);
    }

}
