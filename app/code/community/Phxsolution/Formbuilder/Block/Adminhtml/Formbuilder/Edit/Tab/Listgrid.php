<?php
/*
/**
* Phxsolution Formbuilder
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so you can be sent a copy immediately.
*
* Original code copyright (c) 2008 Irubin Consulting Inc. DBA Varien
*
* @category   adminhtml block
* @package    Phxsolution_Formbuilder
* @author     Murad Ali
* @contact    contact@phxsolution.com
* @site       www.phxsolution.com
* @copyright  Copyright (c) 2014 Phxsolution Formbuilder
* @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/
?>
<?php
class Phxsolution_Formbuilder_Block_Adminhtml_Formbuilder_Edit_Tab_Listgrid extends Mage_Adminhtml_Block_Widget_Grid
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function _construct()
    {
        parent::_construct();
        $this->setId('expert_advice_grid');
        $this->setDefaultSort('sort_order');
        $this->setDefaultDir('ASC');
        //enable ajax grid
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('formbuilder/records')->getRecordsCollection();
        $resource = Mage::getSingleton('core/resource');
        $collection->getSelect()->join(array('lk'=>$resource->getTableName('formbuilder_forms')),'lk.forms_index = main_table.forms_index',array('lk.title'));
        //echo $collection->getSelect()->__toString();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $this->addColumn('records_index', array(
            'header' => Mage::helper('formbuilder')->__('ID'),
            'width' =>  '100px',
            'align' => 'left',
            'index' => 'records_index',
        ));
        $this->addColumn('forms_index', array(
            'header' => Mage::helper('formbuilder')->__('Form Name'),
            'align' => 'left',
            'width' =>  '200px',
            'index' => 'title',
            /*'renderer'  => 'formbuilder/adminhtml_formbuilder_renderer_fieldtitle'*/
        ));
        
        $this->addColumn('customer', array(
            'header' => Mage::helper('formbuilder')->__('Customer Details'),
            'width' =>  '200px',
            'align' => 'left',
            'index' => 'customer',
        ));
        $this->addColumn('value', array(
            'header' => Mage::helper('formbuilder')->__('Content'),
            'align' => 'left',
            'index' => 'value',
        ));
        return parent::_prepareColumns();
    }
    //this method is reuired if you want ajax grid
    public function getGridUrl()
    {
        return $this->getUrl('*/*/fieldsgrid', array('_current' => true));
    }
    public function canShowTab()
    {
        return true;
    }
    public function isHidden()
    {
        return false;
    }
    public function getTabLabel()
    {
        return $this->__('Fields List');
    }
    public function getTabTitle()
    {
        return $this->__('Fields List');
    }
}