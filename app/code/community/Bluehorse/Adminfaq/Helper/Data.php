<?php

class Bluehorse_Adminfaq_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getAllCategoriesArray($optionList = false) {
        $categoriesArray = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSort('name', 'asc')
                ->addFieldToFilter('is_active', array('eq' => '1'))
                ->addAttributeToFilter('level','2')
                ->load()
                ->toArray();

        if (!$optionList) {
            return $categoriesArray;
        }
        $categories[] = array(
                    'value' => 'ebuy',
                    'label' => 'Ebuy',
                );
        foreach ($categoriesArray as $categoryId => $category) {
            if (isset($category['name'])) {
                $categories[] = array(
                    'value' => $categoryId,
                    'label' => Mage::helper('adminfaq')->__($category['name'])
                );
            }
        }
        $categories[] = array(
                    'value' => 'euroviva',
                    'label' => 'EuroViva',
                );
        return $categories;
    }

}
