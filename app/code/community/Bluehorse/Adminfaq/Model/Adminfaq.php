<?php

class Bluehorse_Adminfaq_Model_Adminfaq extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('adminfaq/adminfaq');
    }
}