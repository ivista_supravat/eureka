<?php

class Bluehorse_Adminfaq_Model_Mysql4_Adminfaq_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('adminfaq/adminfaq');
    }
}