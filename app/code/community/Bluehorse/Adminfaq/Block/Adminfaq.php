<?php
class Bluehorse_Adminfaq_Block_Adminfaq extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getAdminfaq()     
     { 
        if (!$this->hasData('adminfaq')) {
            $this->setData('adminfaq', Mage::registry('adminfaq'));
        }
        return $this->getData('adminfaq');
        
    }
}