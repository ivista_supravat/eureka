<?php

class Bluehorse_Adminfaq_Block_Adminhtml_Adminfaq_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('adminfaq_form', array('legend'=>Mage::helper('adminfaq')->__('FAQ information')));
     
	$fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('adminfaq')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

	 $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('adminfaq')->__('Status'),
          'name'      => 'status', 
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('adminfaq')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('adminfaq')->__('Disabled'),
              ),
          ),
      )); 
	  
	  
	   $fieldset->addField('type', 'select', array(
          'label'     => Mage::helper('adminfaq')->__('Question Type'),
          'name'      => 'type', 
		  'required'  => true,
          'values'    => Mage::helper('adminfaq')->getAllCategoriesArray(true),
      )); 
	  
	  
	  $fieldset->addField('questions', 'text', array(
          'label'     => Mage::helper('adminfaq')->__('Question'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'questions',
      ));

      $fieldset->addField('answers', 'editor', array(
          'name'      => 'answers',
          'label'     => Mage::helper('adminfaq')->__('Answers'),
          'title'     => Mage::helper('adminfaq')->__('Answers'),
          'style'     => 'width:700px; height:300px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));


      if ( Mage::getSingleton('adminhtml/session')->getAdminfaqData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getAdminfaqData());
          Mage::getSingleton('adminhtml/session')->setAdminfaqData(null);
      } elseif ( Mage::registry('adminfaq_data') ) {
          $form->setValues(Mage::registry('adminfaq_data')->getData());
      }
      return parent::_prepareForm();
  }
}