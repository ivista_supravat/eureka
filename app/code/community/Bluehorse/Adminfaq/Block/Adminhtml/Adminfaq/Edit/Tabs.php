<?php

class Bluehorse_Adminfaq_Block_Adminhtml_Adminfaq_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('adminfaq_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('adminfaq')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('adminfaq')->__('FAQ Information'),
          'title'     => Mage::helper('adminfaq')->__('FAQ Information'),
          'content'   => $this->getLayout()->createBlock('adminfaq/adminhtml_adminfaq_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}