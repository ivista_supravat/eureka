<?php

class Bluehorse_Adminfaq_Block_Adminhtml_Adminfaq_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('adminfaqGrid');
      $this->setDefaultSort('id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {

      $collection = Mage::getModel('adminfaq/adminfaq')->getCollection();

      $this->setCollection($collection);
	
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('id', array(
          'header'    => Mage::helper('adminfaq')->__('ID'),
          'align'     =>'right',
          'width'     => '10px',
          'index'     => 'id',
      ));

      $this->addColumn('name', array(
          'header'    => Mage::helper('adminfaq')->__('Title'),
		  'width'     => '120px',
          'align'     =>'left',
          'index'     => 'name',
      ));

	 $this->addColumn('questions', array(
          'header'    => Mage::helper('adminfaq')->__('Questions'),
		  'width'     => '540px',
          'align'     =>'left',
          'index'     => 'questions',
      ));
	  	  
	 $this->addColumn('status', array(
          'header'    => Mage::helper('adminfaq')->__('Status'),
          'align'     => 'left',
          'width'     => '20px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
		$this->addExportType('*/*/exportCsv', Mage::helper('adminfaq')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('adminfaq')->__('XML'));
	  
      return parent::_prepareColumns();
  }


  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}