<?php

class Bluehorse_Adminfaq_Block_Adminhtml_Adminfaq_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'adminfaq';
        $this->_controller = 'adminhtml_adminfaq';
        

        $this->_formScripts[] = "
           function toggleEditor() {
                if (tinyMCE.getInstanceById('adminfaq_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'adminfaq_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'adminfaq_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('adminfaq_data') && Mage::registry('adminfaq_data')->getId() ) {
            return Mage::helper('adminfaq')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('adminfaq_data')->getTitle()));
        } else {
            return Mage::helper('adminfaq')->__('Add Item');
        }
    }
}