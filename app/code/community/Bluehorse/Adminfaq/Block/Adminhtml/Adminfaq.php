<?php
class Bluehorse_Adminfaq_Block_Adminhtml_Adminfaq extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_adminfaq';
    $this->_blockGroup = 'adminfaq';
    $this->_headerText = Mage::helper('adminfaq')->__('FAQ Manager');
    $this->_addButtonLabel = Mage::helper('adminfaq')->__('Add Question');
    parent::__construct();
  }
}