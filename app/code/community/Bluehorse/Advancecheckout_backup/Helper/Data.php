<?php
class Bluehorse_Advancecheckout_Helper_Data extends Mage_Core_Helper_Abstract 
{
    const XML_HIDE_SHIPPING_PATH = 'advancecheckout/general/method_status';
    const XML_DEFAULT_SHIPPING_PATH = 'advancecheckout/general/method_code';
    const XML_STATUS_PATH = 'advancecheckout/general/checkout_status';
    
    public function getHideShipping()
    {
		if(!$this->getIsEnabled()) {
			 return false;
		}
        if (!Mage::getStoreConfigFlag(self::XML_DEFAULT_SHIPPING_PATH)){
            return false;
        }
        if (!$this->getDefaultShippingMethod()){
            return false;
        }
        return true;
    }
    public function getDefaultShippingMethod()
    {
        return Mage::getStoreConfig(self::XML_DEFAULT_SHIPPING_PATH);
    }
    
    public function getIsEnabled()
    {
        return Mage::getStoreConfig(self::XML_STATUS_PATH);

    }
}
