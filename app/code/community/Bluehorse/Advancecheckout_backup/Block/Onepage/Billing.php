<?php

class Bluehorse_Advancecheckout_Block_Onepage_Billing extends Mage_Checkout_Block_Onepage_Billing
{

    protected function _construct()
    {
		$step = (Mage::helper('advancecheckout')->getIsEnabled())? 'Shipping Information': 'Billing Information'; 
		
        $this->getCheckout()->setStepData('billing', array(
            'label'     => $step,
            'is_show'   => $this->isShow()
        ));

        if ($this->isCustomerLoggedIn()) {
            $this->getCheckout()->setStepData('billing', 'allow', true);
        }
        Mage_Checkout_Block_Onepage_Abstract::_construct();
    }

}

