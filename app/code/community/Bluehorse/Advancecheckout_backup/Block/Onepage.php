<?php

class Bluehorse_Advancecheckout_Block_Onepage extends Mage_Checkout_Block_Onepage {

    public function getSteps() {
        $steps = array();

        if (!$this->isCustomerLoggedIn()) {
            //$steps['login'] = $this->getCheckout()->getStepData('login');
        }

        $stepCodes = array('billing', 'payment', 'review');

        foreach ($stepCodes as $step) {
            $steps[$step] = $this->getCheckout()->getStepData($step);
        }
        return $steps;
    }

    public function getActiveStep() {
        //return $this->isCustomerLoggedIn() ? 'billing' : 'login';
        return $this->isCustomerLoggedIn() ? 'billing' : 'billing';
    }

    /* Remove Shipping and shipping method from checkout */

    protected function _getStepCodes() {
        /* if (!Mage::helper('advancecheckout')->getHideShipping()){
          return parent::_getStepCodes();
          } */
        return array_diff(parent::_getStepCodes(), array('shipping_method', 'shipping'));
    }

}
