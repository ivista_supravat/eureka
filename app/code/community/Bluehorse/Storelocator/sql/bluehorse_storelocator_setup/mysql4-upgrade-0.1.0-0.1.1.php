<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
        $this->getTable('bluehorse_storelocator/storelocator'), //table name
        'office_type',      //column name
        'varchar(20) default NULL'  //
        );
$installer->endSetup(); 
