<?php
/**
 * Fontend index controller
 * 
 * @category    Bluehorse
 * @package     Bluehorse_Storelocator
 * @author      Supravat Mondal
 * 
 */
class Bluehorse_Storelocator_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Pre dispatch action that allows to redirect to no route page in case of disabled extension through admin panel
     */
    public function preDispatch()
    {
        parent::preDispatch();
        
        if (!Mage::helper('bluehorse_storelocator')->isEnabled()) {
            $this->setFlag('', 'no-dispatch', true);
            $this->_redirect('noRoute');
        }        
    }
    
    function indexAction()
    {
        $this->loadLayout();
        
        $listBlock = $this->getLayout()->getBlock('stores.list');
        if ($listBlock) {
            $currentPage = abs(intval($this->getRequest()->getParam('p')));
            if ($currentPage < 1) {
                $currentPage = 1;
            }
            $listBlock->setCurrentPage($currentPage);
        }
        
        $this->renderLayout();
    }
    
    /**
     * Stores view action
     */
    public function viewAction()
    {
        $storelocatorId = $this->getRequest()->getParam('id');
        if (!$storelocatorId) {
            return $this->_forward('noRoute');
        }
        
        /** @var $model Bluehorse_Storelocator_Model_Storelocator */
        $model = Mage::getModel('bluehorse_storelocator/storelocator')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($storelocatorId);

        if (!$model->getId()) {
            return $this->_forward('noRoute');
        }
        Mage::register('store_view', $model);
        $this->loadLayout();
        $this->renderLayout();
    }
    
     /**
     * Stores search action
     */
    public function searchAction() 
    {
        //search form validation
        $post = $this->getRequest()->getQuery();
        if ( !empty($post) ) {
            try {
                $error = false;
                if(isset($post['country'])) {
                    if (Zend_Validate::is(trim($post['country']) , 'NotEmpty')) {
                        $error = true;
                    }
                }

                if(isset($post['state'])) {
                    if (Zend_Validate::is(trim($post['state']) , 'NotEmpty')) {
                        $error = true;
                    }
                }

                if(isset($post['city'])) {
                    if (Zend_Validate::is(trim($post['city']), 'NotEmpty')) {
                        $error = true;
                    }
                }
                if (!$error) {
                    throw new Exception('Please enter Country or State or City');
                }
            } catch (Exception $e) {

                Mage::getSingleton('core/session')->addError($e->getMessage());
                Mage::getSingleton('core/session')->setSearchFormData($post);
                $this->_redirect('*/*/*');
                return;
            }
        }

        Mage::getSingleton('core/session')->setSearchFormData(false);

        $this->loadLayout();

        $listBlock = $this->getLayout()->getBlock('stores.list');
        if ($listBlock) {
            $currentPage = abs(intval($this->getRequest()->getParam('p')));
            if ($currentPage < 1) {
                $currentPage = 1;
            }
            $listBlock->setCurrentPage($currentPage);
        }

        $this->renderLayout();
    }
    
    public function cityAction(){
        $result = array();
        if (!$this->getRequest()->isXmlHttpRequest()) {
             $this->_redirect('*/*/');
            return;
        }
        $state = $this->getRequest()->getPost('state');
        $collection = Mage::getModel('bluehorse_storelocator/storelocator')->getCollection()->addStoreFilter(Mage::app()->getStore()->getId())->addStatusFilter()->addStateFilter($state);
        
		$collection->getSelect()->group('city');
		$_countries = array();
                $result['html'] = "<option value= 'All' >-- All City --</option>";
                $result['allcity'] = '';
                $result['citylat'] = '';
                $result['citylng'] = '';
                $result['citylat'] = '';
                $result['namelist'] = '';
		foreach($collection as $item) {
                    $result['html'] .= "<option value= '".$item->getCity()."' >".$item->getCity()."</option>";
                    $result['allcity'] = $a;
                    $result['citylat'] = $item->getLatitude();
                    $result['citylng'] = $item->getLongitude();
                    $result['fulladdress'] = $item->getStreetAddress().','.$item->getCity().','.$item->getState().','.$item->getZipcode();
                    $result['namelist'] = $item->getName();
                }
        
	$result['namelist'] = '';
   
        $this->getResponse()->clearHeaders();
        $this->getResponse()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    public function areaAction(){
        $result = array();
        if (!$this->getRequest()->isXmlHttpRequest()) {
             $this->_redirect('*/*/');
            return;
        }
        $city = $this->getRequest()->getPost('city');
        $collection = Mage::getModel('bluehorse_storelocator/storelocator')->getCollection()->addStoreFilter(Mage::app()->getStore()->getId())->addStatusFilter()->addCityFilter($city);
        
		//$collection->getSelect()->group('city');
		$_countries = array();
                $result['html'] = "<option value= 'All' >-- All City --</option>";
                $result['allarea'] = '';
                $result['arealat'] = '';
                $result['arealng'] = '';
                $result['fulladdress'] = '';
                $result['namelist'] = '';
		foreach($collection as $item) {
                    $result['html'] .= "<option value= '".$item->getCity()."' >".$item->getCity()."</option>";
                    $result['allarea'] = '';
                    $result['arealat'] = $item->getLatitude();
                    $result['arealng'] = $item->getLongitude();
                    $result['fulladdress'] = $item->getStreetAddress().','.$item->getCity().','.$item->getState().','.$item->getZipcode();
                    $result['namelist'] = $item->getName();
                }
        
	$result['namelist'] = '';
   
        $this->getResponse()->clearHeaders();
        $this->getResponse()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
}
