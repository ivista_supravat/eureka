<?php

class PayUbiz_PayUbiz_Model_Mysql4_Transactioninfo_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init("payubiz/transactioninfo");
    }

}
