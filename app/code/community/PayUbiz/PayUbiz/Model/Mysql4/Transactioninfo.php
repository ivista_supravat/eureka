<?php

class PayUbiz_PayUbiz_Model_Mysql4_Transactioninfo extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('payubiz/transactioninfo', "id");
    }
}