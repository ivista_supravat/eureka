

function Ajaxloggin(url, params, method, button) {
    //button.disabled=false;
    new Ajax.Request(url, {
        method: method,
        parameters: params,
        onLoading: function () {
            $('ajaxlogin-please-wait').show();
        },
        onSuccess: function (transport) {
            button.disabled = false;
            $('ajaxlogin-please-wait').hide();
            if (200 == transport.status) {
                var response = transport.responseText.evalJSON( );

                /* get Succuess page */
                $('loginmessage').show();
                if (response.success == 1) {


                    if ($('messageflag').hasClassName('error-msg')) {
                        $('messageflag').removeClassName('error-msg');
                    }
                    $('messageflag').addClassName('success-msg');
                    $('message-contain').update(response.message);
                    window.location.href = '';

                } else {


                    if ($('messageflag').hasClassName('success-msg')) {
                        $('messageflag').removeClassName('success-msg');
                    }
                    $('messageflag').addClassName('error-msg');
                    $('message-contain').update(response.error);
                }

            } else {
                alert('Something went wrong...');
            }

        },
        onFailure: function () {
            button.disabled = false;
            alert('Something went wrong...');
        }
    });

}

if (!window.ajaxLogin)
    var ajaxLogin = new Object();

ajaxLogin.Methods = {
    init: function (options) {
        Object.extend(this, this.options || {});
    },
    updateAccountLoginLink: function () {
        var accountlinks = $$('a');
        accountlinks.each(function (accountlink) {
            if (accountlink.href.indexOf('customer/account/login/') > -1) {
                //if(accountlink.href.match(/\/customer\/account\/login\//)){
                $(accountlink).writeAttribute('onclick', 'ajaxLogin.opencolorBox($(this)); return false;');
                $(accountlink).writeAttribute('data-ajaxob', 'getexajax');
                $(accountlink).writeAttribute('href', '#inline_content');
            }
        });

    },
    opencolorBox: function (currentvar) {
        jQuery(currentvar).colorbox({
            inline: true, width: "80%",
            title: 'Login & Register',
            /*					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
             onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
             onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); }, */
            onCleanup: function () {
                $('ajax-Forgotpassword-form').hide();
                $('ajax-register-form').hide();
                $('ajax-login-form').show();
                ajaxLogin.hideAllMesageDiv();
            },
            /*					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }*/
        });
    },
    forgotpasswordPost: function (fAction, fparams, fmethod, fbutton) {
        new Ajax.Request(fAction, {
            method: 'GET',
            parameters: fparams,
            onLoading: function () {
                $('ajaxforgot-please-wait').show();
            },
            onSuccess: function (transport) {
                fbutton.disabled = false;
                $('ajaxforgot-please-wait').hide();
                if (200 == transport.status) {
                    var response = transport.responseText.evalJSON( );
                    $('forgotmessage').show();
                    if (response.success == 1) {
                        if ($('messageflag-forgotpassword').hasClassName('error-msg')) {
                            $('messageflag-forgotpassword').removeClassName('error-msg');
                        }
                        $('messageflag-forgotpassword').addClassName('success-msg');
                        $('message-contain-forgotpassword').update(response.message);
                        //window.location.href='';

                    } else {


                        if ($('messageflag-forgotpassword').hasClassName('success-msg')) {
                            $('messageflag-forgotpassword').removeClassName('success-msg');
                        }
                        $('messageflag-forgotpassword').addClassName('error-msg');
                        $('message-contain-forgotpassword').update(response.error);
                    }

                } else {
                    alert('Something went wrong...');
                }

            },
            onFailure: function () {
                fbutton.disabled = false;
                alert('Something went wrong...');
            }
        });

    },
    RegusterPost: function (fAction, fparams, fmethod, fbutton) {
        new Ajax.Request(fAction, {
            method: fmethod,
            parameters: fparams,
            onLoading: function () {
                $('ajaxregister-please-wait').show();
            },
            onSuccess: function (transport) {
                fbutton.disabled = false;
                $('ajaxregister-please-wait').hide();
                if (200 == transport.status) {
                    var response = transport.responseText.evalJSON( );
                    $('regsitermessage').show();
                    if (response.success == 1) {
                        if ($('messageflag-login').hasClassName('error-msg')) {
                            $('messageflag-login').removeClassName('error-msg');
                        }
                        $('messageflag-login').addClassName('success-msg');
                        $('message-contain').update(response.message);
                        window.location.href = '';

                    } else {
                        if ($('messageflag-login').hasClassName('success-msg')) {
                            $('message-contain-login').removeClassName('success-msg');
                        }
                        $('messageflag').addClassName('error-msg');
                        $('message-contain-login').update(response.error);
                    }

                } else {
                    alert('Something went wrong...');
                }

            },
            onFailure: function () {
                fbutton.disabled = false;
                alert('Something went wrong...');
            }
        });

    },
    activeAjagFGP: function () {
        ajaxLogin.hideAllMesageDiv();
        $('ajax-login-form').hide();
        $('ajax-register-form').hide();
        $('ajax-Forgotpassword-form').show();
    },
    activeAjaxLogin: function () {
        ajaxLogin.hideAllMesageDiv();
        $('ajax-Forgotpassword-form').hide();
        $('ajax-register-form').hide();
        $('ajax-login-form').show();
    },
    activeAjaxRegister: function () {
        ajaxLogin.hideAllMesageDiv();
        $('ajax-login-form').hide();
        $('ajax-Forgotpassword-form').hide();
        $('ajax-register-form').show();
    },
    hideAllMesageDiv: function () {
        $('regsitermessage').hide();
        $('forgotmessage').hide();
        $('loginmessage').hide();
    }
};

Object.extend(ajaxLogin, ajaxLogin.Methods);