<?php

# Create New admin User programmatically.
require_once('app/Mage.php'); // Relative path to Mage.php
umask(0);
Mage::app();

$existsSku = Mage::helper('warehouse/validation')->isValidPincode('587155');

Zend_Debug::dump($existsSku);
die;
$dta = Mage::getSingleton('sap/info')->getData(177);
echo json_encode($dta);
die;

$id = '100000158';
$transaction = Mage::getModel('payubiz/paymentdetails');
$transaction->load($id, 'order_id');

$newstring = substr('', -7);

$date = strtotime('2016-04-12 18:47:11');
Zend_Debug::dump($date);
$transaction->setCreatedTime('2016-04-12 18:47:11');
$transaction->save();

//Zend_Debug::dump($transaction->getCreatedTime());
//$address = Mage::getModel('customer/address');

try {

    $order = Mage::getModel('sales/order')->load(168);

    $customerId = $order->getCustomerId();

    $customer = Mage::getModel('customer/customer')->load($customerId);
    //var_dump($customer->getName());
    $defaultAddress = Mage::getModel('customer/address')->load($customer->getDefaultBilling());


    $street = $defaultAddress->getStreet();


    // product information 

    $items = $order->getAllVisibleItems();
    $itemDetails = array();
    foreach ($items as $item):
        $itemDetails['items'][] = $item->getData();
    endforeach;

    //Zend_Debug::dump($itemDetails);


    $CollectionInfo = [
        'Customer' => $customer->getId(),
        'CustCreditAmt' => $order->getGrandTotal(),
        'AuthorisnNo' => ($order->getBaseTotalDue()),
        'CcBank' => '',
    ];
    $CustomerInfo = [
        'CustomerCode' => $customer->getId(),
        'DistributionChannel' => '',
        'Title' => 'NA',
        'Name' => $customer->getName(),
        'HouseNumber' => '',
        'Street' => (isset($street[0])) ? $street[0] : 'NA',
        'Street2' => (isset($street[1])) ? $street[1] : 'NA',
        'Street3' => 'NA',
        'CountryCode' => $defaultAddress->getCountryId(),
        'City' => $defaultAddress->getCity(),
        'Region' => Mage::getModel('directory/region')->load($defaultAddress->getRegionId())->getCode(),
        'PostalCode' => $defaultAddress->getPostcode(),
        'PaymentType' => Mage::helper('sap')->PaymentType($order->getPayment()->getMethodInstance()->getCode()),
        'EmployeeCode' => 'NA',
        'AuthorisnNo' => '',
        'TransactionId' => '',
    ];

    $data = [
        'CollectionInfo' => $CollectionInfo,
        'CustomerInfo' => [
            'item' => [
                'CustomerInfo' => $CustomerInfo,
                'CustomerContactInfo' => [
                    'item' => [
                        'Contact' => $customer->getEmail(),
                        'ContactType' => 'E'
                    ]
                ],
            ],
        ],
        'SalesOrderInfo' => [
            'SoldToParty' => $customer->getId(),
            'ShipToParty' => $customer->getId(),
            'EmployeeCode' => '',
            'TransactionId' => '',
            'TransactionDate' => '',
            'TransactionTime' => '',
            'PaymentType' => Mage::helper('sap')->PaymentType($order->getPayment()->getMethodInstance()->getCode()),
            'ItemDetails' => $itemDetails,
            'EuroFriend' => '',
            'BuyBackInd' => ''
        ],
        'Source' => 'W'
    ];

    Zend_Debug::dump($data['SalesOrderInfo']);
} catch (Exception $ex) {
    //Zend_Debug::dump($ex->getMessage());
}
?>
 